*come from diabetes, spring 2009;
%MACRO ANALYZE(METHOD,NCL);
   	PROC CLUSTER DATA=SET2 OUT=TREE METHOD=&METHOD P=35 ccc STANDARD;
   	VAR X1-X5;
   	ID patient;
   	TITLE "Stanford Diabetes Data";
   	TITLE2;
   	%LET K=1;
   	%LET N=%SCAN(&NCL,&K);
   	%DO %WHILE(&N NE);

/* Compute the tree and create a file with cluster labels */

PROC TREE DATA=TREE NOPRINT OUT=OUT NCL=&N;
  	COPY PATIENT x1-x5;
   	TITLE2 "COMPUTE &N CLUSTERS USING METHOD=&METHOD";
PROC SORT DATA=OUT; BY CLUSTER;

/* Delete the following line to avoid printing the 
   data with a column of cluster labels            */

proc print data=out; 
run;                                   

/*  Compute canonical discriminants and display clusters */

PROC CANDISC data=out simple OUT=CAN ncan=5 prefix=can;
     CLASS cluster;
     VAR x1-x5;
run;

PROC PLOT;
     PLOT CAN2*CAN1=CLUSTER/HPOS=56;
     PLOT CAN3*CAN1=CLUSTER/HPOS=56;
     TITLE2 "PLOT OF &N CLUSTERS FROM METHOD=&METHOD";
RUN;

%LET K=%EVAL(&K+1);
%LET N=%SCAN(&NCL,&K);
%END;
%MEND;


/*  Run the macro for several methods        */

%ANALYZE(WARDS, 7 10)
%ANALYZE(centroid, 7)
%ANALYZE(complete, 7)
%ANALYZE(average, 7)
%ANALYZE(single, 7)
