data allscore;
    retain flag 0;
    infile "D:\Adu's Programs\Study\score_copy.txt";
    input score @@;
    if score=. then
       flag=flag+1;
    else
       output allscore;
run;

proc print data=allscore;
run;

%macro getdata;
    %local i;
	%local Str;
	%let str=s;
    data
         %do i=1 %to 3;
            ID&i&str
         %end;
    ;
         set allscore;
         select(flag);
            %do i=1 %to 3;
                when(&i) output ID&i&str;
            %end;
            otherwise;
         end;
    run;
%mend getdata;

%getdata;      

proc print data=id1s;
run;

ods listing file="c:\research\test.txt";

%macro write;
    %local i;
    %do i=1 %to 9207;
       ods listing select "tests for normality";
       proc univariate data=ID&i normal;
       var score;
       run;
    %end;
%mend write;

%write;
data allscore;
    retain flag 0;
    infile "D:\Adu's Programs\Study\score_copy.txt";
    input score @@;
    if score=. then
       flag=flag+1;
    else
       output allscore;
run;

proc print data=allscore;
run;

%macro getdata;
    %local i;
	%local Str;
	%let str=s;
    data
         %do i=1 %to 3;
            ID&i&str
         %end;
    ;
         set allscore;
         select(flag);
            %do i=1 %to 3;
                when(&i) output ID&i&str;
            %end;
            otherwise;
         end;
    run;
%mend getdata;

%getdata;      

proc print data=id1s;
run;

ods listing file="c:\research\test.txt";

%macro write;
    %local i;
    %do i=1 %to 9207;
       ods listing select "tests for normality";
       proc univariate data=ID&i normal;
       var score;
       run;
    %end;
%mend write;

%write;
