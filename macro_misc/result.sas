data mydata;
input score @@;
cards;
 580
 580
 580
 580
 583
 583
 583
 583
 583
 587
 587
 587
 587
 587
 587
 587
 587
 587
 587
 587
 587
 587
 590
 590
 590
 590
 590
 590
 590
 590
 590
 590
 590
 595
 595
 595
 595
 595
;
run;
proc print;
run;
ods listing file="c:\research\test.txt";
ods listing select "tests for normality";
proc univariate data=mydata normal;
run;
