%let contrcoef=;
%let temp=conmat[i];
%let contrcoef=&contrcoef &temp;
%put &contrcoef;
%let contrcoef=&contrcoef &temp;
%put &contrcoef;
%let contrcoef=&contrcoef &temp;
%put &contrcoef;

/*
%let contrcoef=&contrcoef 3;
%let contrcoef=&contrcoef 4;
%put &contrcoef;
*/


proc iml;
	conmat=j(1,2*3,0);
	%do i=1 to 6;
		%let temp=conmat[i];
		%put &temp;
		%let contrcoef=&contrcoef &temp;
		%put &contrcoef;
	%end;
quit;

%put &contrcoef;
