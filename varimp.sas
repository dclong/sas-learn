/*
@description This macro determines the importance of each predictor variable 
using the permutation method.

@param _InDataTrain Two-level name of input dataset on which _InScoreCode will run.

@param _InTarget Name of the target variable in _InDataTrain.

@param _InVarList Two-level name of a SAS dataset 
containing the list of variables used in _InScoreCode.  
This dataset should contain a single column named "Variable".

@param Two-level name of a SAS dataset for output results.
This dataset will have two variables: "Variable" (Character with Length 32) 
and "Importance" (Numeric with Length 8).    
The column "Variable" is identical to the column "Variable" in _InVarList,
and the column "Importance" contains corresponding importances of these variables.

@param _InScoreCode The full path and name of the scoring code.  

@param _InScoreName The name of the score created by _InScoreCode.

@param _Work Library where temporary datasets will be written to.  
The default value is Work.

@param _CleanUp Yes/No flag indicating whether to delete temporary datasets at end of processing.
The default value is No.

@note This macro generates two temporary datasets "pdata" and "score" 
in the specified library "_Work".
"pdata" is the original data "_InDataTrain" with the column 
corresponding to the last variable in "_InVarList" permuted, 
and "score" is the predicted score on the permuted data "pdata".

@example

* library where the input dataset is;
libname paul "C:\Users\ZKYR9JM\Documents\projects\paul\data";
* library where temporary datasets will be written to;
libname test "C:\Users\ZKYR9JM\Documents\projects\paul\test";
* create a macro variable of the input data set for convenience;
%let _InDataTrain = paul.score_data_1304_seg1;
* create a macro variable of the score code for convenience;
%let _InScoreCode = C:\Users\ZKYR9JM\Documents\projects\paul\code\myscorecode.sas;
* read in the variable list;
data myvars;
	Infile "C:\Users\ZKYR9JM\Documents\projects\paul\data\varlist.txt" truncover;
	Input Variable $32.;
Run;
* run %VarImp without clean-up;
%VarImp(&_InDataTrain., newperf, myvars, vi, &_InscoreCode., P_newperf, _Work=test);
* run %VarImp and clean up temporary datasets after execution;
%VarImp(&_InDataTrain., newperf, myvars, vi, &_InscoreCode., P_newperf, _Work=test, _CleanUp=Yes);

*/
%macro VarImp(_InDataTrain, _InTarget, _InVarList, _OutVarList, _InScoreCode, _InScoreName, _Work = Work, _CleanUp = No);
	/* predict original socres without permutation */
	data &_Work..score;
  		set &_InDataTrain.;
  		%include "&_InScoreCode.";
	run;
	/* calcualte original log likelihood, LL0 */
	proc sql noprint;
		select 
			sum(&_InTarget. * log(&_InScoreName.) + (1 - &_InTarget.) * log(1 - &_InScoreName.)) into :LL0 
		from 
			&_Work..score;
	quit;
	/* create macro variables from _InVarList */
	proc sql noprint;
		select count(*) into :n from &_InVarList.;
		select Variable into :x1-:x%left(&n.) from &_InVarList.;
	run;
	%do i = 1 %to &n.;
		/* permute data */
		data &_Work..pdata;
			set &_InDataTrain. (keep=&&x&i.);
			&&x&i.._sorting_key = ranuni(-1);
		run;
		proc sort data=&_Work..pdata;
			by &&x&i.._sorting_key;
		run;
		data &_Work..pdata;
			set &_InDataTrain.;
			set &_Work..pdata (keep=&&x&i.);
		run;
		/* predict socres */
		data &_Work..score;
  			set &_Work..pdata;
  			%include "&_InScoreCode.";
		run;
		/* calcualte original log likelihood, LL0 */
		proc sql noprint;
			select 
				sum(&_InTarget. * log(&_InScoreName.) + (1 - &_InTarget.) * log(1 - &_InScoreName.)) into :LL&i. 
			from 
				&_Work..score;
		quit;
		/* importance */
		%let LL&i. = %sysevalf((&LL0. - &&LL&i.) / &LL0.);
	%end;
	/* read macro variables (importance) into a dataset */
	data &_OutVarList.;
		set &_InVarList.;
		Importance = symget(compress("LL" || _n_));
	run;
	/* cleanup */
	%if &_CleanUp = Yes %then %do;
		proc datasets lib = &_Work nolist;
			delete score pdata;
		run;
	%end;
%mend;
