
```MS SQL Server
If Object_id('tempdb.dbo.#temp0') is not null drop table #temp0
```
```SAS Proc SQL
drop table table_name;
```
notice that a warning message will be printed to the log if the table does not exist.

```MS SQL Server
insert into table
select 1, "blabla"
;
```
```SAS Proc SQL
insert into table
set ...
```
or
```SAS Proc SQL
insert into table
values (v1, v2, v3)
```
Notice that MS SQL Sever can use select without the from clause. 
@todo: Check you can also do this in teradata sql and oracle sql. 


create table as
select ...
<=>
select ...
into table
in most versions of SQL.

In MS SQL Server, you use

insert into table select ... (insert into an exist table) — sas has the same syntax
select ... into ... table (insert into a new table, i.e., create/overwrite a table)

SAS does not support the latter syntax (the reason is that it is reserved for select columns into macro variables) but you use following equivalent code.
create table table_name as
select ...

insert into can be used with set, values and select clause. 

proc sql;
   insert into sql.newcountries
	  set name='Bangladesh',
		  capital='Dhaka',
		  population=126391060
	  set name='Japan',
		  capital='Tokyo',
		  population=126352003;

proc sql;
   insert into sql.newcountries
	  values ('Pakistan', 'Islamabad', 123060000, ., ' ', .)
	  values ('Nigeria', 'Lagos', 99062000, ., ' ', .); 

proc sql;
   insert into a
	  select var1, var2
	  from a
	  where var1 > 0;


MS SQL Server support (update by join). 

UPDATE
	Table
SET
	Table.col1 = other_table.col1,
	Table.col2 = other_table.col2
FROM
	Table
INNER JOIN
	other_table
ON
	Table.id = other_table.id

row_number() over(partition by ... order ...)
SAS proc sql does not support this, but you can sort according to the partion columns, create a sequential id on sorted observation and ...

create index index_name on table_name (column_1, column_2)
3 different ways of creating index in sas?

loops in ms sql server can be implemented by sas macros 

SAS Proc SQL does not support update by join . 
proc sql;
title 'UNITEDSTATES';
update sql.unitedstates as u
   set population=(select population from sql.newpop as n
			where u.name=n.state)
		where u.name in (select state from sql.newpop);
select Name format=$17., Capital format=$15.,
	   Population, Area, Continent format=$13., Statehood format=date9.
   from sql.unitedstates;

This is not efficient. Another (more efficient) way is to first join the tables and then use new columns (created via join) to update your target columns.

MS SQL Server implicitly cast data when necessary while SAS Proc SQL does not implicitly cast data (even if a data step does this).


it seems to me that MS SQL Server support the syntax
select Impute = case when w.YYYY_MM is NULL then 1 else 0 end ...
<=>
select case when w.YYYY_MM is NULL then 1 else 0 end as impute
SAS Proc SQL support the latter syntax but not the first one. 
Actually most versions of SQL support the latter one instead of the first one.

ms sql use [] to quote columns containing white spaces, teradata use '', sas does not support column names containg white spaces. 
column names in sas must be valid sas names.

ms sql support many different ways of creating column alias 
http://sqlblog.com/blogs/aaron_bertrand/archive/2012/01/23/bad-habits-to-kick-using-as-instead-of-for-column-aliases.aspx
