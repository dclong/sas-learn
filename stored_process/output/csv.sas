%global _ODSDEST;
*ProcessBody;

data _null_;
    rc = stpsrv_header('Content-type','application/vnd.ms-excel ');
    rc = stpsrv_header('Content-disposition','attachment; filename=temp.csv');
run;

%let _odsdest=csv;

%STPBEGIN;

proc print data=sashelp.class;
run;

%STPEND;
