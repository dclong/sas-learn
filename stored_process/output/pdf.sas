%global _ODSDEST;
*ProcessBody;

data _null_;
    rc = stpsrv_header('Content-type','application/pdf');
    rc = stpsrv_header('Content-disposition','attachment; filename=temp.pdf');
run;

%let _odsdest=pdf;

%STPBEGIN;

proc print data=sashelp.class;
run;

%STPEND;
