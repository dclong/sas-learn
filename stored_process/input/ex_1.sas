*ProcessBody;

%STPBEGIN;

proc print data=sashelp.class;
    where age = &age;
    title1 "&age-Year-Old Students";
run;

%STPEND;
