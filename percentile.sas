
/*
@author Ben Chuanlong Du
@description Calcualte percentiles of a specified numerical variable in a data set. 
@param data A SAS data set.
@param var The numerical variable (in the data set) whose percentiles are to be calcualted.
@param output The output data set.
@param prefix The prefix to percentiles as column names in the output data set.
@param pctArr The array containing percentiles. 
@write 1 row record with percentiles as columns into output. 
@example{
%array(pct, values=50 60 70 80 90 99.99 100);
%percentile(data=n3d.b_tran_cr_pty_1405, var=tot_amt, output=p, pctArr=pct);
}
*/
%macro percentile(data=, var=, output=, prefix=pct, pctArr=, pctList=);
    proc univariate data=&data. noprint;
        var &var.;
        output out=&output. pctlpts=%do_over(&pctArr, phrase=?) pctlpre=&prefix_;
    run;
%mend;

