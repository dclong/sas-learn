


/*
if else by default, 1 statement, has to use do ... end block if you want to run multiple ...
*/
proc iml;
	if 2 > 1 then do;
		print "good";
		print "bad";
	end;
quit;


proc iml;
	if 2 < 1 then
		print "good"; 
		print "bad";
	
quit;
***********************************************************************************;
/*
modules don't have to be in order
*/
proc iml;
start Mod5(a,b);
   c = a+b;
   d = a-b;
   run Mod6(c,d);
   print "In Mod5:" c d;
finish;

start Mod6(x,y);
   x = x#y;
finish;

	run Mod5({1 2}, {3 4});
quit;

/*
difference, can use negative lag
*/
proc iml;
	x = {1 3 8 9 2};
	dx = x[-1];
	print dx;
quit;

proc iml;
	x = {1, 3, 4, 7, 9};
	dif = dif(x, -2);
	print dif;
quit;

proc iml;
	x = {1 2 3, 4 5 6};
	y = {1 -1, 2 -2};
	z = x # y;
	print z;
quit;

proc iml;
	x = {1 2 3, 4 5 6};
	y = {1 2 1};
	z = x # y;
	print z;
quit;

/**
m:n is a row vector.
The vector way always return a column vector.
If you want to keep the original matrix structure, 
you have to use matrix subscripts.
*/
proc iml;
	print(1:3);
	x1 = {1 2 3 4};
	x2 = {1, 2, 3, 4};
	print(x1[1:3]);
	print(x2[1:3]);
	print(x1[1, 1:3]);
quit;

/**
row and column index trick
*/
proc iml;
	x = {1 2, 3 4};
	y = loc(col(x) >= row(x));
	print y;
quit;


/**
cannot use default argument
*/
proc iml;
	start f(x=1,y=2);
		return(x);
	finish;
quit;

/**
version information
*/
PROC SETINIT;
quit;

PROC PRODUCT_STATUS;
quit;

/**
you can use a very long name for modules in the iml procedure,
but I don't the upper limit
*/
proc iml;
	start upper_right_triangular_matrix(n, k);
		return(j(n, n, 1));
	finish;
	x = upper_right_triangular_matrix(3, 2);
	print x;
quit;

proc iml;
/* 
@param x a matrix.
@return a matrix m such that m[i, j] = i 
*/
start row(x);  
   return(repeat(t(1:nrow(x)), 1, ncol(x)));
finish;

/* 
@param x a matrix.
@return a matrix m such that m[i, j] = j 
*/
start col(x);  
   return(repeat(1:ncol(x), nrow(x)));
finish;


	x = {1 2 3 4, 1 2 3 4, 1 2 3 4, 1 2 3 4};
	y = j(4, 4, 0);
	y[loc(row(x) <= col(x) & col(x) <= row(x) + 2)] = 1;
	r = row(x);
	c = col(y);
	print r;
	print c;
	i = (r <= c & c <= r + 2);
	print i;
	print x;
	print y;

	
quit;



proc iml;
/* 
@param x a matrix.
@return a matrix m such that m[i, j] = i 
*/
start row(x);  
   return(repeat(t(1:nrow(x)), 1, ncol(x)));
finish;

/* 
@param x a matrix.
@return a matrix m such that m[i, j] = j 
*/
start col(x);  
   return(repeat(1:ncol(x), nrow(x)));
finish;


	x = {1 2 3 4, 1 2 3 4, 1 2 3 4, 1 2 3 4};
	
	r = row(x);
	c = col(x);
	print r;
	print c;
	i = (r <= c & c <= r + 2);
	print i;

quit;


proc iml;
	start dif(x);
		n = nrow(x);
		return(x[2:n] - x[1:(n-1)]);
	finish;
	x = {1, 3, 5, 9};
	y = dif(x);
	print(y);
quit;

proc iml;
	x = {1 2 3, 40 50 60};
	y = dif(x);
	print y;
quit;

proc iml;
	x = 7:2;
	print x;
quit;


proc iml;
ERROR: (execution) Invalid subscript or subscript out of range.
Indeces can only be positive. 1-based ...
	x = 1:3;
	print (x[4]);
quit;


proc iml;
	x = {1, -4, 9, -7};
	y = max(x, 0);
	print y;
quit;
How can do vectorized max/min of 2 vectors?

/**
Choose keeps the original matrix sturcture, i.e., dimensions.
*/
proc iml;
	x = {1, -4, 9, -7};
	y = choose(x>0, x, 0);
	print y;
quit;

proc iml;
	x = {1 -4 9 -7};
	y = choose(x>0, x, 0);
	print y;
quit;

/**
Column and row sums
*/
proc iml;
	x = {1 -4, 9 -7};
	y = x[+, ];
	print y;
quit;

proc iml;
	x = {1 -4, 9 -7};
	y = x[, +];
	print y;
quit;

/**
parameters of a module must be data, i.e., matrices.
cannot pass another module as parameter.
If you do want to do that, 
you have to use macros.
*/
proc iml;
	start f(x);
		return(x);
	finish;
	start g(fun);
		return(fun(4));
	finish;
	r = g(f);
	print r;
quit;