

data s1;
	input x y;
	datalines;
1 2
3 4
;
run;
/*
matrices in sas iml support row and column names, 
but not convenient to use. 
it is suggested that you don't use them
*/
proc iml;
	use s1;
	read all into s;
	print s;

mattrib s rowname=({jenny linda}) 
            colname=({mon tue}) 
            label='Weekly Coffee' 
            format=2.0; 
    print s; 
	x = s[, "mon"];
	print x;
quit;

proc iml;
	x = {1 2, 3 4};
	y = j(2, 1, 1);
	print x;
	print y;
	z = insert(y, x, 0, 2);
	print z;
quit;

/*
Testing cases for add_intercept.
*/
proc iml;
start add_intercept(x);
    if any(x[, 1] ^= 1) then do;
        return(insert(j(nrow(x), 1, 1), x, 0, 2));
    end;
    return(x);
finish;

x = {1 2, 3 4, 5 6};
y = add_intercept(x);
z = add_intercept(y);
print x;
print y;
print z;
quit;

proc iml;
	start f(x);
		x = x + 1;
	finish;
	x = 1;
	run f(x);
	print x;
run;

proc iml;
	start f(x);
		_x = x + 1;
		return(_x);
	finish;
	y = f(3);
	print y;
quit;