
data Mixed;  
   x=1; y=2; z=3; a='A'; b='B'; c='C'; 
run;
 
proc print data=Mixed;
run;

proc iml;
use Mixed;
   read all var _ALL_; 
close Mixed;
print x;
print y;
print z;
print a;
print b;
print c;
quit;


proc iml;
use Mixed;
   read all var _ALL_ into hhh[colname=varNames]; 
close Mixed;
print varNames;
print hhh;
hhh = hhh[, {"x", "z"}];
print hhh;
print varNames;
jjj=1000;
hhh = insert(hhh, jjj, 0, 3);
print hhh;
quit;

