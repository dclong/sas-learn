




/*
lag used in sas has very bad side effect ...
*/
data one;
	input x @@;
    if x = 3 then do;
		y = 3;
		z = 3;
	end; else do;
		y = lag1(y) + 2;
		z = lag1(x) + 100;
	end; 
    datalines;
1 2 3 4 5 6
;


proc print data=one;
   title 'LAG Output';
run;


data one;
	input x @@;
   	x = lag1(x);
    datalines;
1 2 3 4 5 6
;


proc print data=one;
   title 'LAG Output';
run;