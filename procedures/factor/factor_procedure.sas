
PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals;
     var X1-X6;
     priors smc;
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals  heywood outstat=VarStat;
     var X1-X6;
     priors smc;
run;

proc factor data=chbones(type=corr) method=ml n=2 rotate=promax residuals heywood outstat=ProStat;
     var x1-x6;
	 priors smc;
run;

PROC factor DATA=madison(TYPE=CORR) SCREE SCORE METHOD=PRIN N=5 COV EV REORDER;
run;

/*  Compute principal components from
    the correlation matrix*/

PROC factor DATA=madison(TYPE=CORR) SCREE SCORE METHOD=PRIN N=5 EV REORDER;
run;

proc factor data=set1 method=ml scree  nfactors=2 mineigen=0 simple 
             ev  msa out=scorepc  outstat=facpc;
     var hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
     priors smc;
	 title f="Arial" c=red h=4 "Factor analysis using ML method";
run;

proc factor data=set1 method=prinit n=2 rotate=varimax;
     var hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
     priors smc;
	 title f="Arial" c=red h=4 "Factor analysis using prinit method";
run;

/* Use information on the principal 
   components computed by PROC FACTOR 
   to rotate the first 8 components */

proc factor data=facpc n=8 rotate=varimax round reorder;
run;

PROC factor DATA=huba(TYPE=CORR) SCREE CORR SCORE METHOD=PRIN N=13  EV REORDER;
run;

proc factor data=set1(TYPE=CORR) method=ml nfactors=2 
   	res msa  maxiter=75 rotate=varimax outstat=fac2;
   	var x1-x6;
   	priors smc;
run;

/*  Try a PROMAX rotation */

proc factor data=fac2 nfactors=2 rotate=promax 
    reorder score ;
  	var x1-x6;
  	priors smc;
run;

proc factor data=set1(TYPE=CORR) method=ml nfactors=3 res msa
   	maxiter=75 rotate=varimax reorder;
  	var x1-x6;
  	priors smc;
run;

proc factor data=women method=ml nfactors=1 maxiter=75 heywood;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=2 maxiter=75 heywood
   					rotate=varimax out=women2v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=2 maxiter=75 heywood
   					rotate=promax out=women2p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=3 maxiter=75 heywood
   				rotate=varimax out=women3v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=3 maxiter=75 heywood
   	rotate=promax out=women3p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=4 maxiter=75 heywood
   				rotate=varimax out=women4v;
  	var x1-x7;
  	priors smc;
run;

proc factor data=women method=ml nfactors=4 maxiter=75 heywood
   			rotate=promax out=women4p;
  	var x1-x7;
  	priors smc;
run;

proc factor data=men method=ml nfactors=1 maxiter=75 heywood;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=2 maxiter=75 heywood
   				rotate=varimax out=men2v;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=2 maxiter=75 heywood
   			rotate=promax out=men2p;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=3 maxiter=75 heywood
   					rotate=varimax out=men3f;
  	var x1-x8;
  	priors smc;
run;

  
proc factor data=men method=ml nfactors=3 maxiter=75 heywood
   				rotate=promax out=men3p;
  	var x1-x8;
  	priors smc;
run;

proc factor data=men method=ml nfactors=4 maxiter=75 heywood;
  	var x1-x8;
  	priors smc;
run;

PROC FACTOR DATA=spearman(TYPE=CORR) METHOD=ml N=1;
     var C F E M D Mu;
     priors smc;
run;

proc factor data=set1 method=prin scree  nfactors=5 mineigen=0
     			simple corr ev nplot=2 res msa out=scorepc  outstat=facpc;
     var x1-x5;
run;

proc factor data=facpc n=2 rotate=varimax plot outstat=facpc2 score round reorder;
run;

/*  Compute and plot the scores for 
    the two rotated components */

proc score data=set1 score=facpc2 out=scorepc2;  
run;

proc factor data=set1 method=prinit nfactors=2 nplot=2 res msa
     			maxiter=75 conv=.0001 outstat=facpf rotate=varimax  reorder;
     var x1-x5;
     priors smc;
run;


/*  Use maximum likelihood estimation */

proc factor data=set1 method=ml nfactors=1 res msa maxiter=75 
             conv=.0001 outstat=facml1;
     var x1-x5;
     priors smc;
run;

proc factor data=set1 method=ml nfactors=2 res msa mineigen=0 maxiter=75 
             conv=.0001 outstat=facml2 rotate=varimax reorder;
     var x1-x5;
     priors smc;
run;

proc factor data=set1 method=prin nfactors=3 mineigen=0 res msa
             maxiter=75 conv=.0001 outstat=facml3 rotate=varimax reorder;
     var x1-x5;
     priors smc;
run;

/* Apply other rotations to the two
   factor solution  */

proc factor data=facml2 nfactors=2 conv=.0001 rotate=quartimax reorder;
     var x1-x5;
     priors smc;
run;

proc factor data=facml2 nfactors=2 conv=.0001 rotate=promax reorder score;
     var x1-x5;
     priors smc;
run;

proc factor data=pcorr(type=corr) method=prin n=5 rotate=varimax;
     var x1-x24;
run;

proc factor data=pcorr(type=corr) method=prinit n=5 rotate=varimax maxit=200 heywood;
     var x1-x24;
	 title f="Arial" h=4 c=red "Factor Analysis Using Principal Factor Method";
run;

PROC FACTOR DATA=PCORR(TYPE=CORR) METHOD=ML NFACTOR=5 ROTATE=VARIMAX;
     VAR X1-X24;
	 title f="Arial" c=red h=5 "Factor analysis using ML Method"; 
run;

proc factor data=pcorr(type=corr) method=ml n=4 rotate=varimax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Test of the # of factors";
run;

proc factor data=pcorr(type=corr) method=ml n=5 rotate=promax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Promax rotation on the result of ML method";
run;

proc factor data=pcorr(type=corr) method=ml n=5 rotate=quartimax;
     var x1-x24;
	 title f="Arial" c=red h=5 "Quartimax rotation on the result of ML method";
run;

