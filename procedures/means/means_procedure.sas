
/*We can also use procedure summary realize the functions of means procedure*/

proc means data=set1 noprint; 
	by variety nitrogen;
  	var yield;
  	output out=means mean=my;
run;

proc means data=set1 mean stddev median; 
	by agent;
    var time; 
    output out=means mean=mean stddev=s median=median;
run;

proc means data=set1 n mean std var stderr min max;
	by oil;
  	var y;
run;

proc means data=set1 noprint; 
  	by dosage admin;
  	var y;
  	output out=means mean=my;
run;

proc means noprint;
  	by code;
  	var percent;
  	output out = means mean = mean std = sd;
run;

proc means mean std stderr t prt clm;
  	var diff;
  	title 'Paired data: t-test and 95% confidence interval';
run;
