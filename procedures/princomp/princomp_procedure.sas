
proc princomp data=set1 cov n=3 out=scores prefix=prin;
     var x1-x10;
run;

proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
     var x1-x10;
run;

proc princomp data=females out=pcorr prefix=pcorr;
     var length width height;
run;

PROC PRINCOMP DATA=SET1 COV N=3 PREFIX=PRIN;
     PARTIAL SEX;
     VAR LENGTH WIDTH HEIGHT;
RUN;

/* COMPUTE PRINCIPAL COMPONENTS SEPARATELY FOR EACH SEX */

PROC PRINCOMP DATA=SET1 OUT=SCORE2 COV N=3 PREFIX=PRIN;
     BY SEX;
     VAR LENGTH WIDTH HEIGHT;
RUN;

/* COMPUTE PRINCIPAL COMPONENTS FROM THE COVARIANCE
MATRIX FOR THE COMBINED DATA */

PROC PRINCOMP DATA=SET1 OUT=SCORE3 COV N=3 PREFIX=PRIN;
     VAR LENGTH WIDTH HEIGHT;
RUN;

proc princomp data=females cov n=3 out=scores prefix=prin;
     var length width height;
run;

proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
  	var x1-x10;
run;
