proc reg data=set1;
     model y = x1 x2 x12 / p ss1 ss2 clm cli partial vif collin influence;
     output out=set2 r=residual p=yhat;
run;

proc reg data=set1;
     model y = x1 x2 x12 x11 x22 x112 x221  x111 x222 / 
         selection=rsquare cp aic sbc mse b adjrsq;
run;
