
proc goption;
run;

goptions cback=white colors=(black) targetdevice=ps;

axis1 label=(f=swiss h=2.0) ORDER = -3 to 3 by 1
      value=(f=swiss h=1.8) w=3.0  length= 5.5 in;

axis2 label=(f=swiss h=2.0 a=90)  order = -1 to 17 by 2
      value=(f=swiss h=1.8)  w=3.0  length = 5.5 in;

SYMBOL1 V=CIRCLE H=2.0 i=none ;

PROC GPLOT DATA=effects;
	 PLOT value1*q  / overlay vaxis=axis2 haxis=axis1;
   	 title  h=2.8 f=swiss "Normal Probability Plot";
   	 LABEL q='Normal Scores';
   	 LABEL value1 = 'Effects';
RUN;

goptions cback=white colors=(black) targetdevice=ps;

axis1 label=(f=swiss h=2.0)
      value=(f=swiss h=1.8) w=3.0  length= 5.5 in;

axis2 label=(f=swiss h=2.0 a=90) 
      value=(f=swiss h=1.8)  w=3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;

PROC GPLOT DATA=setr;
	PLOT resid*q  / overlay vaxis=axis2 haxis=axis1;
    title  h=2.8 f=swiss "Normal Probability Plot";
    LABEL q='Normal Scores';
    LABEL resid = 'Residuals';
RUN;

PROC GPLOT DATA=setr;	
	PLOT resid*yhat  / overlay vaxis=axis2 haxis=axis1;
    title  h=2.8 f=swiss "Residual Plot";
    LABEL yhat='Estimated means';
    LABEL resid = 'Residuals';
RUN;

goptions  cback=white colors=black  targetdevice=ps;

axis1 label = (h=3 r=0 a=90 f=swiss 'f(t)')
      length = 5 in
      color = black width=3.0 style=1
      order = 0 to .4 by .05;

axis2 label = (h=2.5 f=swiss 't')
      length = 6.0 in
      color=black width=3.0 style=1
      order = -5 to 5 by 1;

symbol1 v=none i=spline l=1 h=2 c=black width=3;
symbol2 v=none i=spline l=3 h=2 c=black width=3;
symbol3 v=none i=spline l=9 h=2 c=black width=3;

proc gplot data=set1;
  	plot (fn f2 f10)*x / overlay vaxis=axis1 haxis=axis2;
  	title1 h=4;
  	title2 h=3.5 f=triplex c=black 'Density functions for the t-distribution';
run;



goptions rotate=landscape targetdevice=ps;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to .4 by .1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2  by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

      SYMBOL1 V=CIRCLE H=3.0 ;
      SYMBOL2 V=none H=3.0 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT (residual)*yhat /  vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL yhat='Predicted values';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT residual*q / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Normal probability plot";
      LABEL q='q';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to 5 by 1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT residual*x1 / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL x1='Amount of Copper (mg)';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to 25 by 5
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by  .1
      value=(f=swiss h=3.0)
      length = 60 pct;

      SYMBOL1 V=CIRCLE H=2.5 ;
      SYMBOL2 V=none H=2.5 l=1 i=spline ;

PROC GPLOT DATA=set2;
      PLOT residual*x2 / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL x2='Time (days)';
      LABEL residual = 'Residuals';
RUN;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      order = -1 to 1 by .5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

  proc gplot data=set3;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Ordered Residuals';
      label  q = 'Standard Normal Quantiles';

   run;


 axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = 1 to 5 by 1
      length = 5 in;


proc gplot data=set3;
      plot r*yhat / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Residual Plot';
      label  r = 'Residuals';
      label  yhat = 'Estimated Mean Capture Counts';

   run;



goptions cback=white colors=(black)
     device=win target=winprtc rotate=portrait;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Predicted Biomass')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=set1;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=set1;
  plot resid*yhat / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;


goptions targetdevice=ps rotate=portrait;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Mean Muscle Weight')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=setr;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=setr;
  plot resid*yhat / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;


   goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)  ORDER = 120 to 270 by 30
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

axis2 label=(f=swiss h=2.0)  order = 50 to 80 by 10
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=DIAMOND H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=square H=2.0 w=3 l=9 i=join ;

 PROC GPLOT DATA=means;
   PLOT my*nitrogen=variety / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Sugar Cane Yields";
       LABEL my='Mean Yield';
       LABEL nitrogen = 'Nitrogen (lb/acre)';
   RUN;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 a=90)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Residuals';
      label  q = 'q';

   run;

axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length = 5 in;

  symbol v=dot h=2 I=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set4;
      plot lstd*lmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Plot to Determine Power Transformation';
      label  lstd = 'Log(Standard Deviation)';
      label  lmean = 'Log(Mean)';
   run;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Ordered Residuals';
      label  q = 'Standard Normal Quantiles';

   run;


axis1 label=(f=swiss h=2.5)  ORDER = 0.5 to 2.0 by 0.5
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

axis2 label=(f=swiss h=2.0)  order = 0.3 to 0.8 by 0.1
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=DIAMOND H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=square H=2.0 w=3 l=9 i=join ;

 PROC GPLOT DATA=means;
   PLOT my*dosage=admin / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Concentration in the Liver";
       LABEL my='Sample Mean';
       LABEL dosage = 'Dosage (mg)';
   RUN;

goptions  cback=white colors=black targetdevice=ps;

  axis1 label = (h=3 r=0 a=90 f=swiss 'density')
        length = 6 in
        color = black width=6.0 style=1
        order = 0 to 0.5 by .1;

  axis2 label = (h=3 f=swiss 'quantile')
        length = 6.5 in
        color=black width=6.0 style=1
        order = 0 to 10 by 1;

  symbol1 v=none i=spline l=1 h=2 c=black width=4;
  symbol2 v=none i=spline l=3 h=2 c=black width=4;
  symbol3 v=none i=spline l=9 h=2 c=black width=4;
  symbol4 v=none i=spline l=16 h=2 c=black width=4;

proc gplot data=set1;
  plot (f1 f3 f5 f7)*x / overlay vaxis=axis1 haxis=axis2;
  title1 h=4;
  title2 h=3.5 f=swiss c=black 'Central Chi-Square Densities';
  run;

goptions targetdevice=ps rotate=landscape colors=(black) ;

axis1 label=(f=swiss h=2.5)  ORDER = 15 to 35 by 5
      value=(f=swiss h=2.0)  w=3.0  length= 7.5 in;

axis2 label=(f=swiss h=2.0 r=0 a=90)  order = 15 to 50 by 5
      value=(f=swiss h=2.0) w= 3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 i=none ;
       SYMBOL2 V=diamond H=2.0 w=3 i=none ;
       SYMBOL3 V=square H=2.0 w=3 i=none ;
	   SYMBOL4 V=none H=2.0 w=3 l=1 i=spline ;
       SYMBOL5 V=none H=2.0 w=3 l=1 i=spline ;
       SYMBOL6 V=none H=2.0 w=3 l=1 i=spline ;

 PROC GPLOT DATA=setrall;
    PLOT y1*x1 y2*x2 y3*x3 yhat1*x1 yhat2*x2 yhat3*x3 / overlay vaxis=axis2 haxis=axis1;
    TITLE1  H=3.0 F=swiss "Cracker Sales (cases)";
       LABEL y1='Post-promotion Sales';
       LABEL x1 = 'Pre-promotion Sales';
   RUN;



axis1 label=(f=swiss h=2.5 )  ORDER = 15 to 45 by 5
      value=(f=swiss h=2.0)  w=3  length= 7.5 in;

axis2 label=(f=swiss h=2.2 r=0 a=90)  order = -3 to 3 by 1
      value=(f=swiss h=2.0)  w=3  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;
       SYMBOL2 V=none H=3.0 w=2 l=1 i=spline;

PROC GPLOT DATA=SETR;
      PLOT resid*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
  TITLE1  H=3.0 F=swiss "Residuals";
       LABEL yhat='Estimated Means';
       LABEL resid = 'Residuals';
   RUN;

axis1 label=(f=swiss h=2.5 ) ORDER = -3 to 3 by 1
      value=(f=swiss h=2.0) w=3.0  length= 7.5 in;

axis2 label=(f=swiss h=2.2 r=0 a=90)  order = -3 to 3 by 1
      value=(f=swiss h=2.0)  w=3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;
       SYMBOL2 V=none H=2.0 w=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SETR;
      PLOT resid*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=3.0 F=swiss "Normal probability plot";
       LABEL q='Normal Scores';
       LABEL resid = 'Residuals';
   RUN;

goptions cback=white colors=(black) rotate=portrait
     targetdevice=ps;

axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by .5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = 2.0 to 4.5 by .5
      value=(f=swiss h=2.0)
      length = 5.0 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 w=4 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT y*x / vaxis=axis2 haxis=axis1;
    TITLE1  H=5.0 F=swiss ;
    TITLE2  H=4.0 F=swiss "Cup Loss Data";
       LABEL x='Bottle loss';
       LABEL  Y = 'Cup loss';
   RUN;

goptions cback=white colors=(black) rotate=portrait
     targetdevice=ps;

axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by .5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = 2.0 to 4.5 by .5
      value=(f=swiss h=2.0)
      length = 5.0 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 w=4 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT y*x yhat*x / overlay vaxis=axis2 haxis=axis1;
    TITLE1  H=5.0 F=swiss ;
    TITLE2  H=4.0 F=swiss "Cup Loss Data";
       LABEL x='Bottle loss';
       LABEL  Y = 'Cup loss';
   RUN;


/*  Plot residuals against bottle  
    loss to check for homogeneity of 
    variances and other patterns   */


axis1 label=(f=swiss h=2.2)
      ORDER = 2.0 to 4.0 by 0.5
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = -1 to 1 by .5
      value=(f=swiss h=2.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss ;
TITLE2  H=3.0 F=swiss "Cup Loss Data";
title3  h=3.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=2.2)
      ORDER = -2.0 to 2.0 by 1
      value=(f=swiss h=2.0)
      length= 5.5 in;

axis2 label=(f=swiss h=2.2 a=90)
      order = -1.0 to 1.0 by 0.5
      value=(f=swiss h=2.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;
       SYMBOL3 V=NONE  H=2 L=3 I=SPLINE;
       SYMBOL4 V=NONE  H=2 L=3 I=SPLINE;

   PROC GPLOT DATA=SET2;
      PLOT residual*q  / vaxis=axis2 haxis=axis1;
     TITLE1  H=4.0 F=swiss ;
     TITLE2  H=3.0 F=swiss "Cup Loss Data";
     title3  h=3.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


goptions reset=global rotate=landscape targetdevice=ps;

  axis1 label = (h=2 r=0 a=90 f=swiss 'Percent')
        length = 4.8 in
        value = (h=2.0 f=swiss);

  axis2 label = (h=2 f=swiss 't-statistics')
        length = 7.0 in
		order = (-5 to 5 by 1)
        value = (h=2.0 f=swiss);

  pattern1 value=empty;

  proc gchart data=stats3;
     vbar t / type=percent space=0 frame
              raxis = axis1 gaxis=axis2;
     title h=2.5 f=swiss "t Values for 10000 Random Permutations";
     run;

	goptions rotate=landscape targetdevice=ps hsize=6in vsize=6.7in;

 axis1 label=(f=swiss r=0 a=90 h=2.2 'Scores')
   ORDER = 0 to 35 by 5
   value=(f=swiss h=2.0)
   length= 4.0 in;

 axis2 label=(f=swiss h=2.2 'Normal Quantiles')
      order = -3.0 to 3.0 by 1
      value=(f=swiss h=2.0)
      length = 4.5in;
      

 symbol1 V=circle H=2.0;

PROC GPLOT DATA=set3; 
      PLOT Y*Q / vaxis=axis1 haxis=axis2;
      TITLE  H=3.0 f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Intrinsic Values";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;

PROC GPLOT DATA=set4; 
      PLOT Y*Q / vaxis=axis1 haxis=axis2;
      TITLE  H=3. f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Extrinsic Values";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;

goptions rotate=landscape targetdevice=ps;

 axis1 label=(f=swiss r=0 a=90 h=2.2 'Residuals')
   ORDER = -15 to 15 by 5
   value=(f=swiss h=2.0)
   length= 4.0 in;

 axis2 label=(f=swiss h=2.2 'Normal Quantiles')
      order = -3.0 to 3.0 by 1
      value=(f=swiss h=2.0)
      length = 4.5 in;

 symbol1 V=circle H=2.0;

PROC GPLOT DATA=resid; 
      PLOT r*q / vaxis=axis1 haxis=axis2;
      TITLE  H=3.0 f=swiss "Creative Writing Study";
	  Title2  h=2.5 f=swiss "Residuals";
      Title3  h=2.5 f=swiss "Normal probability plot";
   RUN;

proc plot data=set2;
	plot r*(q yhat)='*' / hpos=60;
run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3)
      ORDER = -3 to 3 by 1
      value=(f=triplex h=2.3)
      length= 5.5 in;

  axis2 label=(f=swiss h=2.3 a=90 r=0)
      order = -20 to 25 by 5
      value=(f=swiss h=2.3)
      length = 5.0 in;

  axis3 label=(f=swiss h=2.3)
      order = 0 to 5 by 1
      value=(f=swiss h=2.3)
      length = 5 in;

  axis4 label=(f=swiss h=2.3 a=90 r=0)
      order = -10 to 50 by 10
      value=(f=swiss h=2.3)
      length = 4.5 in;

  SYMBOL1 V=CIRCLE H=2.5 ;

 PROC GPLOT DATA=SET2;
   PLOT y*oil /  vaxis=axis4 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Fat Absorbtion";
   TITLE2  H=2.2 F=swiss "Doughnut Cooking Oils";
       LABEL  y = 'Fat Absorption-150g';
	   LABEL  oil = 'Type of Cooking Oil';
   RUN;


  PROC GPLOT DATA=SET2;
   PLOT r*q / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
   TITLE2  H=2.2 F=swiss "Residuals Analysis for Doughnut Cooking Oils";
       LABEL q='Standard Normal Expectations';
       LABEL  r = 'Ordered Residuals';
   RUN;

   PROC GPLOT DATA=SET2;
   PLOT r*oil / vaxis=axis2 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Residual Plot";
   TITLE2  H=2.2 F=swiss "Analysis of Doughnut Cooking Oils";
       LABEL  r = 'Residuals';
	   LABEL  oil = 'Type of Cooking Oil';
   RUN;

goptions  cback=white colors=black device=WIN target=WINPRTC;

  axis1 label = (h=2.5 r=0 a=90 f=swiss 'density')
        length = 6 in
        color = black width=4.0 style=1
        order = 0 to 1.4 by .2;

  axis2 label = (h=2.5 f=swiss 'quantiles')
        length = 6.5 in
        color=black width=4.0 style=1
        order = 0 to 3 by .5;

  symbol1 v=none i=spline l=1 h=2 c=black width=4;
  symbol2 v=none i=spline l=3 h=2 c=black width=4;
  symbol3 v=none i=spline l=9 h=2 c=black width=4;
  symbol4 v=none i=spline l=16 h=2 c=black width=4;

proc gplot data=set1;
  plot (f110 f1010 f104 f1050)*x / overlay vaxis=axis1 haxis=axis2;
  title1 h=4;
  title2 h=3.5 f=swiss c=black 'F-distribution Densities';
  run;


goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;
 
PROC GPLOT DATA=set1;
   PLOT Y*x /  vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;

goptions targetdevice=ps rotate=landscape colors=black cback=white;

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 c=black V=circle H=2.0 i=none;
   symbol2 c=black v=none w=3 l=1 i=splines;

PROC GPLOT DATA=SET2;
   PLOT y*x yhat*x/ overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;


axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = -.01 to 0.04 by 0.01
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;
   symbol2 v=none w=3 l=1 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

PROC GPLOT DATA=SET2;
   PLOT residual*x yzero*x / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data Residuals";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;

axis1 label=(f=swiss h=2.5)
   order = -3 to 3 by 1
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = -0.01 to 0.04 by 0.01
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=none;

PROC GPLOT DATA=SET2;
   PLOT residual*q /  vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Forbes Data Residuals";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  Y = 'Log Pressure';
   RUN;

axis1 label=(f=swiss h=2.5)
   ORDER = 190 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 3.0 to 3.5 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

   
   symbol1 v=none w=3 l=1 i=splines;
   symbol2 v=none w=2 l=20 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

 PROC GPLOT DATA=SET2;
   PLOT yhat*x lower*x upper*x / 
         overlay vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.2 F=swiss "Simultaneous 95% Confidence Bands";
TITLE3  h=3.2 F=SWISS "for the True Regression Line";
       LABEL x='Boiling point of water (degrees F)';
       LABEL  yhat = 'Log Pressure';
   RUN;

goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER = 15 to 45 by 5
      value=(f=triplex h=2.5)
      length= 5in;

axis2 label=(f=swiss h=3.0 a=90)
      order = 15 to 45 by 5
      value=(f=swiss h=2.5)
      length = 5in;

SYMBOL1 V=CIRCLE H=2.5 ;
    
PROC GPLOT DATA=SET1;
      plot y*x / vaxis=axis2 haxis=axis1;
  TITLE1  H=2.0 F=swiss " ";
  TITLE2  H=3.0 F=swiss "Fisher Broadbalk Wheat Data";
       LABEL y='Yield in second plot';
       LABEL x = 'Yield in first plot';
   RUN;

goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
      ORDER = 1 to 3 by 1 
      offset=(1cm, 1cm)
      value=(f=swiss h=2.0)
      w=3.0 length= 6.5 in;

axis2 label=(f=swiss h=2.0)
      order = 2 to 14 by 2
      value=(f=swiss h=2.0)
      w= 3.0 length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=square H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=diamond H=2.0 w=3 l=17 i=join ;


  PROC GPLOT DATA=means;
    PLOT my*a=b / vaxis=axis2 haxis=axis1;
    title1  H=3.0 F=swiss "Hayfever Relief";
       LABEL my='Hours';
       LABEL a = 'Factor A';
   RUN;

goptions targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5)
   ORDER = 180 to 215 by 5
   value=(f=swiss h=2.0)
   length= 6.5 in;

axis2 label=(f=swiss h=2.0  a=90)
      order = 2.7 to 3.4 by .1
      value=(f=swiss h=2.0)
      length = 4.0 in;

axis3 label=(f=swiss h=2.0  a=90)
      order = 15 to 30 by 5
      value=(f=swiss h=2.0)
      length = 4.0 in;

   symbol1 V=circle H=2.0 i=r;
 
PROC GPLOT DATA=set1;
PLOT Y*X /  vaxis=axis3 haxis=axis1;
PLOT LOGY*X / vaxis=axis2 haxis=axis1;
TITLE1  H=2.0 F=swiss ;
TITLE2  H=3.5 F=swiss "Hooker Data";
       LABEL X='Boiling point of water (degrees F)';
       LABEL LOGY = 'Log Pressure';
	   LABEL Y = 'Pressure';
   RUN;

goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 250 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;

goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER =35 to 75 by 10
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency  Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;

goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER =35 to 75 by 10
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency  Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;

goptions rotate=landscape targetdevice=pscolor
            hsize=8 in  vsize=6 in;

   axis1 color=tomato 
         label = (c=blue h=1.5 a=90 "Rainfall");
   axis2 color=tomato 
         label = (c=violet h=2.0 "Normal Quantiles");

   symbol1 c=steelblue v=dot i=none;

   proc gplot data=quantiles;
     plot rain*normal=1 / vaxis=axis1 haxis=axis2 frame;
	 title "Normal probability Plot" h=4.0;  run;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.5 a=90 )
      order = 0 to 10 by 1
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.5)
      value=(f=swiss h=2.3)
      order = 1 to 4 by 1
	  offset=(0.3in, 0.3in)
      length = 7 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set1;
      plot elasticity*intensity / vaxis=axis1 haxis=axis2;
      title  h=3 F=swiss 'Dot Plot for Skin Elasticity';
      label  elasticity = 'Elasticity';
      label  intensity = 'Light Intensity';
   run;

proc boxplot data=set1;
  	plot elasticity*intensity / boxstyle=schematic 
                         			vaxis=axis1 haxis=axis2;
  	title h=3 "Box Plots for Skin Intensity";
  	label  elasticity = 'Elasticity';
  	label  intensity = 'Light Intensity';
run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 angle=90)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -2.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;
  
proc gplot data=set2; by intensity;
      plot elasticity*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  elasticity = 'Elasticity';
      label  q = 'Standard Normal Quantiles';

   run;


goptions targetdevice=pscolor rotate=landscape;

  axis1 label=(f=swiss h=2.3 angle=90 ' Residuals ')
      order = -4 to 4 by 2
      value=(f=swiss h=2.0)
      length= 5 in;

  axis2 label=(f=swiss h=2.3 ' Standard Normal Quantiles ' )
      value=(f=swiss h=2.0)
      order = -3.0 to 3.0 by 1
      length = 6.5 in;

  axis3 label=(f=swiss h=2.3 ' Light Intensity ' )
      value=(f=swiss h=2.0)
      order = 1 to 4 by 1
	  offset=(0.3in, 0.3in)
      length = 6.5 in;

  axis4 label=(f=swiss h=2.3 angle=90 ' Skin Elasticity ')
      order = 0 to 10 by 1
      value=(f=swiss h=2.0)
      length= 5 in;

  symbol1 c=black v=circle h=2 i=none;
  symbol2 c=black v=dot h=2 i=none;
  symbol3 c=darkblue v=none i=join w=3;
  symbol4 c=bgr v=K f=special i=none h=3; 

proc gplot data=set3;
      plot r*q=4 / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      plot r*intensity=2 / vaxis=axis1 haxis=axis3;
      title1  h=3. F=swiss 'Residual Dot Plot';
      plot pred*intensity=3 elasticity*intensity=1 / 
               overlay vaxis=axis4 haxis=axis3;
      title1  h=3. F=swiss 'Trend in the Estimated Means';

   run;


axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.1)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.1)
      length = 5 in;

  symbol v=dot h=2 i=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;

axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.1)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.1)
      length = 5 in;

  symbol v=dot h=2 i=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;

 goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )  ORDER = 0.0 to 1.5 by 0.5
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

axis2 label=(f=swiss h=2.0 a=90)  order = 300 to 380 by 20
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;

axis3 label=(f=swiss h=2.5)  ORDER = 0.0 to 2.0 by 1.0
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=DIAMOND H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=square H=2.0 w=3 l=9 i=join ;
       SYMBOL4 V=dot H=2.0 w=3 l=15 i=join ;

 PROC GPLOT DATA=means;
   PLOT my*Z=W / vaxis=axis2 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Pea Yields";
       LABEL my='Sample Mean';
       LABEL Z = 'Level of Z (pints/acre)';
   RUN;

  PROC GPLOT DATA=means;
   PLOT my*W=Z / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Pea Yields";
       LABEL my='Sample Mean';
       LABEL W = 'Level of W (pints/acre)';
   RUN;

 axis1 label=(f=swiss h=2.5)
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

  axis2 label=(f=swiss h=2.0 a=90)
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;


  SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=none ;


 PROC GPLOT DATA=setr;
   PLOT resid*yhat / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Pea Yield Residuals";
       LABEL resid='Resdiuals';
       LABEL yhat = 'Estimated mean yield';
   RUN;

axis1 label=(f=swiss h=2.5 )
         value=(f=swiss h=2.0)
         length= 6.5 in;

   axis2 label=(f=swiss h=2.5 a=90)
         value=(f=swiss h=2.0)
         length = 4.5 in;

   SYMBOL1 V=CIRCLE H=3.0 ;

   PROC GPLOT DATA=SETr;
      PLOT resid*q / overlay vaxis=axis2 haxis=axis1;
      TITLE1  H=4.0 F=swiss ;
      TITLE2  H=3.5 F=swiss "Pea Yield Data";
      title3  h=3.5 f=swiss "Normal Probability Plot";
       LABEL yhat='Standard Normal Percentiles';
       LABEL resid = 'Ordered Residuals';
   RUN;

goptions colors=(black)targetdevice=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Yield')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Production Process')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=dot i=none h=3 w=3 c=black;

proc gplot data=set2;
  plot yield*process / vaxis=axis1 haxis=axis2;
  title h=4.0 ls=0.5in f=swiss 
        c=black 'Dot Plot of Penicillin Process Yields';
  footnote ls=0.6in '  ';
  run;

goptions colors=(black)targetdevice=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Production Process')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=set2;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=set2;
  plot resid*process / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;

goptions targetdevice=ps rotate=portrait;

  axis1 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length= 5.5 in;

  axis2 label=(f=swiss h=2.3 a=90 r=0)
      order = 20 to 90 by 10
      value=(f=swiss h=2.3)
      length = 5.5 in;
  
  SYMBOL1 V=CIRCLE H=2.5 ;

 PROC GPLOT DATA=set1;
   PLOT y*(x1 x2 x3) /  vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss "Incidence of Heart Disease";
   TITLE3  H=2.2 F=swiss "(55-59 year old males)";
       LABEL  y = "100 log(deaths per 100000 males)";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;
axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 c=black V=circle H=2.0 i=none;
   symbol2 v=none w=3 l=1 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

PROC GPLOT DATA=setall;
   PLOT r*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X1";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X2";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;


PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x12 zero*x12 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;

axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 V=circle H=2.0 i=none;

PROC GPLOT DATA=setall;
   PLOT r*q /  vaxis=axis2 haxis=axis1;
TITLE  H=8.0 F=swiss ;
TITLE2  H=3.5 F=swiss ;
TITLE3  H=3.5 F=swiss "Normal Probability Plot for Residuals";
       LABEL q='Expected N(0,1) Values';
       LABEL  r = 'Residuals';
   RUN;

axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 c=black V=circle H=2.0 i=none;
   symbol2 v=none w=3 l=1 i=splines;
   symbol3 v=none w=2 l=20 i=splines;

PROC GPLOT DATA=setall;
   PLOT r*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X1";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X2";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;


PROC GPLOT DATA=setall;
   PLOT r*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus X3";
       LABEL  y = "Residuals";
	   LABEL  x1 = 'Telephones per 1000 persons';
	   LABEL  x2 = 'Percent fat calories';
	   Label  x3 = 'Percent animal protein calories';
   RUN;

PROC GPLOT DATA=setall;
   PLOT r*x12 zero*x12 / overlay vaxis=axis2 haxis=axis1;
   TITLE H=8.0 " ";
   TITLE2  H=3.0 F=swiss " ";
   TITLE3  H=2.2 F=swiss "Residuals versus Predicted Values";
       LABEL  r = "Residuals";
	   LABEL  yhat = 'Predicted Values';

   RUN;

axis1 label=(f=swiss h=2.5)
   value=(f=swiss h=2.0)
   length= 5.5 in;

axis2 label=(f=swiss h=2.0)
      value=(f=swiss h=2.0)
      length = 5.5 in;

   symbol1 V=circle H=2.0 i=none;

PROC GPLOT DATA=setall;
   PLOT r*q /  vaxis=axis2 haxis=axis1;
TITLE  H=8.0 F=swiss ;
TITLE2  H=3.5 F=swiss ;
TITLE3  H=3.5 F=swiss "Normal Probability Plot for Residuals";
       LABEL q='Expected N(0,1) Values';
       LABEL  r = 'Residuals';
   RUN;

proc reg data=set1 outest=parms covout;
  model y = x1 x2 x3 / vif ;
  plot y*(x1 x2 x3) / hplots=2 vplots=2;
  plot residual.*(x1 x2 x3 predicted. )/ hplots=1 vplots=4;
  run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.5 a=90 )
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.5)
      value=(f=swiss h=2.3)
      order = 1 to 5 by 1
	  offset=(0.3in, 0.3in)
      length = 7 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set1;
      plot time*agent / vaxis=axis1 haxis=axis2;
      title  h=3 F=swiss 'Dot Plot for Agent Transaction Times';
      label  agent = 'Agent';
      label  time = 'Transaction Time (in days)';
   run;

proc boxplot data=set1;
  plot time*agent / boxstyle=schematic
				    vaxis=axis1 haxis=axis2;
  title  h=3 F=swiss 'Dot Plot for Agent Transaction Times';
      label  agent = 'Agent';
      label  time = 'Transaction Time (in days)';
  run;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 a=90)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Residuals';
      label  q = 'q';

   run;goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss h=2.3 a=90)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Residuals';
      label  q = 'q';

   run;

axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length = 5 in;

  symbol v=dot h=2 I=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;


goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )
      ORDER = 100 to 150 by 25 
      offset=(1cm, 1cm)
      value=(f=swiss h=2.0)
      w=3.0 length= 6.5 in;

axis2 label=(f=swiss h=2.0 a=90)
      order = 500 to 1400 by 100
      value=(f=swiss h=2.0)
      w= 3.0 length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=square H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=diamond H=2.0 w=3 l=17 i=join ;


  PROC GPLOT DATA=means;
    PLOT my*temp=glass / vaxis=axis2 haxis=axis1;
    title1  H=3.0 F=swiss "Oscilloscope Light Ouput";
       LABEL my='Light Output';
       LABEL temp = 'Temperature (degrees C)';
   RUN;


goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 0 to 100 by 20
      value=(f=triplex h=3.0)
      length= 5 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 0 to 100 by 20
      value=(f=swiss h=3.0)
      length = 5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=square H=2.2 ;
	   symbol3 V=dot h=2.2;


PROC GPLOT DATA=set1;
      PLOT x2*x1=group / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Plot of Aptitude Scores Against Mathematics Scores";
      LABEL x1='Aptitude ';
      LABEL x2 = 'Mathematics';
RUN;

goptions colors = (black black blue green);

goptions device=WIN target=ps rotate=portrait;

/* Specify features of the plot */

axis1 label=(f=swiss h=2.5  
      "Standard Normal Quantiles")
      ORDER = -2.5 to 2.5 by .5
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Data ")
      value=(f=triplex h=2.0)
      length = 5.0in;

  SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT X1*Q1 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X1";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X2*Q2 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X2";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X3*Q3 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X3";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X4*Q4 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X4";
     footnote ls=1.0in;
RUN;

goptions cback=white colors=(black) device=WIN target=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss)
        length = 5 in
        value=(h=0.25in f=swiss)
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Standard Normal quantiles')
        value=(h=0.25in f=swiss)
        length = 5.5 in
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=bnotes; 
     PLOT X1*Q1 / vaxis=axis1 haxis=axis2;
     PLOT X2*Q2 / vaxis=axis1 haxis=axis2;
     PLOT X3*Q3 / vaxis=axis1 haxis=axis2;
     PLOT X4*Q4 / vaxis=axis1 haxis=axis2; 
     PLOT X6*Q6 / vaxis=axis1 haxis=axis2;  
     title ls=2.0in h=4 f=swiss  'Normal Q-Q Plot';
run;

 axis1 label = (h=2.5 r=0 a=90 f=swiss 'Ordered distances')
        value =(h=0.25in f=swiss)
        length = 5 in
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Chi-square quantiles')
        length = 5.5 in
		value =(h=0.25in f=swiss)
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=CHISQ;
  	plot col1*col2 / vaxis=axis1 haxis=axis2;
  	title ls=2.0in h=4 f=swiss  'Chi-square Plot';
run;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Distances ")
      value=(f=triplex h=2.0)
      length = 5.0in;

axis1 label=(f=swiss h=2.5 
      "Chi-Square Quantiles")
      value=(f=triplex h=1.6)
      length= 5.5in;
 
 SYMBOL1 V=CIRCLE H=2.0 ;
 
PROC GPLOT DATA=CHISQ;
   	PLOT Col1*COL2 / vaxis=axis2 haxis=axis1;
	TITLE1 ls=1.5in H=3.0 F=swiss "CHI-SQUARE PLOT";
	Title2 H=2.3 F=swiss "Board Stiffness Data";
	footnote ls=1.0in;
RUN;

proc gplot data=ALoadings;
     plot VF2*VF1 PF2*pf1/overlay legend=legend1 haxis=axis1 vaxis=axis2;
	 goptions reset=all;
	 symbol1 v=circle c=red h=1.5 w=2;
	 symbol2 v=triangle c=blue h=1.5 w=2;
	 axis1 label=(f="Arial" c=magenta h=5pct "Factor1") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct);
     axis2 label=(f="Arial" c=magenta h=5pct "Factor2") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct); 
	 legend1 down=2 position=(top left inside) cshadow=black frame
	           value=(f="Arial" h=12pt t=1 "Varimax" t=2 "Promax")
			   label=(f="Arial" h=2 "Method");
	 title f="Arial" c=black h=18pt "Factor plot of Varimax and Promax";
run;

proc plot data=results vtoh=2;
   plot dim2*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim2='*' $ work  /box href=0 vref=0;
run;

PROC PLOT;
     PLOT CAN2*CAN1=CLUSTER/HPOS=56;
     PLOT CAN3*CAN1=CLUSTER/HPOS=56;
     TITLE2 "PLOT OF &N CLUSTERS FROM METHOD=&METHOD";
RUN;

goptions targetdevice=ps300 rotate=portrait;

axis1 label=(f=swiss h=2.5  
      "Differences in BOD Measurements")
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Differences in SS Measurements ")
      value=(f=triplex h=2.0)
      length = 5.0in;

SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT dBOD*dSS / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "Differences in Effluent Measurements";
     footnote ls=1.0in;
RUN;

goptions cback=white colors=(black) targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.5 )
      ORDER = 350 to 700 by 50
      value=(f=triplex h=3.0)
      length= 8.0 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 2 to 4 by 0.2
      value=(f=swiss h=3.0)
      length = 4.5 in;

      SYMBOL1 V=CIRCLE H=2.5 ;
      SYMBOL2 V=square H=2.2 ;
	  Symbol3 V=dot H=2.2;

PROC GPLOT DATA=SET1;
      PLOT GPA*GMAT=admit / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Business Graduate School Admissions Data";
RUN;

PROC PLOT;
   	PLOT CAN2*CAN1=CLUSTER/HPOS=56;
RUN;

proc g3d data = one ;
  	plot x1 * x2 = likelihood ;
run;

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 ;
run;

proc g3d data = one ;
  	plot x1 * x2 = likelihood / rotate = 45 tilt = 20 ;
run;

axis1 order = (0 to 0.5 by 0.05) minor = none length = 70 pct
   		label = (f=swiss a=90 h=1.5 'Sample quantiles') ;

axis2 order = (-2.4 to 2.4 by 0.4) minor = none length = 70 pct
   		label = (f=swiss h=1.5 'Normal quantiles');

symbol1 v = dot c = green i = none h = 1 ;

proc gplot data = all ;
  plot radiation * q / vaxis = axis1 haxis = axis2 frame ;
  title 'Q-Q plot for microwave example' h=2 f=swiss ;
run ;

proc plot data=scores;
     plot prin1*prin2=newage/ hpos=56;
     plot prin1*prin3=newage/ hpos=56;
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=newage / hpos=56;
     plot pcorr1*pcorr3=newage / hpos=56;
run;

goptions cback=white colors=(black) 
    targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=2.5 )
      ORDER = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length= 5.5 in;

axis2 label=(f=swiss h=2.6 r=0 a=90)
      order = -2.5 to 2.5 by 1.
      value=(f=triplex h=2.5)
      length = 4.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=men2v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Promax Factor Scores";
   title2 h=2.5 f=swiss "Male Track Records";
     label factor1='Distance Races ';
     label factor2 = 'Sprints';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor2*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
     label factor2 = 'Distance Races';
   footnote ls=0.4in '  ';
   RUN;

 proc gplot data=women3v;
   plot factor3*factor1 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor1='Sprints ';
   footnote ls=0.4in '  ';
   RUN;

   proc gplot data=women3v;
   plot factor3*factor2 / vaxis=axis2 haxis=axis1;
   title1 ls=0.1in h=3.0 f=swiss "Varimax Factor Scores";
   title2 h=2.5 f=swiss "Female Track Records";
     label factor2='Distance Races ';
   footnote ls=0.4in '  ';
   RUN;

proc gplot data=coor;
  	symbol1 v=point;
  	axis1 length=6 IN order=-0.60 to 1.00 by .2;
  	axis2 length=6 IN order=-0.40 to 0.40 by .1;
  	plot   y*x=1/haxis= axis1 vaxis= axis2 frame href=0 vref=0 annotate=coor;
run;

proc plot data=pcorr;
  	plot pcorr1*pcorr2=type / hpos=56;
  	plot pcorr1*pcorr3=type / hpos=56;
run;

proc plot data=results vtoh=2;
   	plot dim2*dim1='*' $ parents  /box href=0 vref=0;
   	plot dim3*dim1='*' $ parents  /box href=0 vref=0;
run;

proc gplot data=cplot;
  	symbol1 v=point;
  	axis1 length = 6 in order = -.3 to .3 by .1;
    axis2 length = 6 in order = -.3 to .3 by .1;
  	plot y*x = 1 / haxis=axis1 vaxis=axis2 frame href=0 vref=0 annotate=cplot;
  	title1 'Associations Between Mental Health Status' h=2.5 f=swiss;
  	title2 'and Socioeconomic Status' h=2.5 f=swiss;
run;

goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 32 to 44 by 2
      value=(f=triplex h=3.0)
      length= 6 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 56 to 66 by 2
      value=(f=swiss h=3.0)
      length = 6 in;

SYMBOL1 V=CIRCLE H=2.5 ;
SYMBOL2 V=square H=2.2 ;

PROC GPLOT DATA=SET1;
      PLOT x2*x1=temp / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Strength of Steel Samples";
      TITLE2  H=3.0 F=swiss "Effect of Rolling temperature";
      LABEL x1='Yield Point ';
      LABEL x2 = 'Ultimate Strength';
RUN;

goptions cback=white colors=(black) targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.0 )
      ORDER = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length= 5.5 in;

axis2 label=(f=swiss h=3.1 r=0 a=90)
      order = -3 to 3 by 1
      value=(f=triplex h=3.0)
      length = 5.5 in;

SYMBOL1 V=circle H=2.5 l=1 w=3 i=none;
      
proc gplot data=scorepc2;
     plot factor2*factor1 / vaxis=axis2 haxis=axis1;
     title1 ls=0.3in h=4.0 f=swiss "Factor Scores";
     title2 h=3.0 f=swiss "Stock Market Data";
     label factor1='Market Factor ';
     label factor2 = 'Chemical vs Oil Stocks';
     footnote ls=0.4in '  ';
RUN;

proc plot data=scores ;
     plot prin1*prin2 / hpos=56;
     plot prin1*(length width height)=id / hpos=56;
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=id / hpos=56;
run;

proc plot;
  	by smoothingparameter;
  	plot DepVar*year pred*year = '*' /overlay;
run;

proc plot;
  plot y*blockmean =  trt;
  title 'Responses in each block, sorted by block mean';
run;

proc plot;
  plot bresid*gresid yhat*gresid = '*' /overlay;
run;

proc plot;
   	plot rs*yhat = '*' $ i;
   	plot rs*resid;
   	plot rs*i =  '*';
   	plot cookd*i = '*' ;
   	plot dffit*i = '*' ;
   	plot h*i = '*' ;
run;

proc plot;
  	where ph = .;
  	plot yhat*logtime = '*' uciobs*logtime = 'u' lciobs*logtime = 'l' /overlay;
run;

proc chart;
  	vbar yield;
  	title 'Histogram of tomato yields';
run;

proc chart;
  	vbar yield;
  	title 'Histogram of tomato yields';
run;
