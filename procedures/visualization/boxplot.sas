
PROC BOXPLOT DATA=set1;
     PLOT (X1-X4)*Z / BOXSTYLE=SCHEMATICID;
run;

PROC BOXPLOT DATA=set1;
     PLOT (x1 x2 x3 x4)*group / BOXSTYLE=SCHEMATICID;
run;

PROC BOXPLOT DATA=set1;
     PLOT (X1 X2)*temp / BOXSTYLE=SCHEMATICID;
run;

proc boxplot data=grain;
     plot (x1-x11)*type/boxstyle=schematic;
run;

proc boxplot data=loans;
     plot (x1-x8)*type / boxstyle=schematic;
run;

proc boxplot data=set1;
	plot time*agent / boxstyle=schematic;
  	title "Box Plots";
run;

proc boxplot data=set1;
  	plot time*agent / boxstyle=schematic vaxis=axis1 haxis=axis2;
  	title  h=3 F=swiss 'Dot Plot for Agent Transaction Times';
    label  agent = 'Agent';
    label  time = 'Transaction Time (in days)';
run;
