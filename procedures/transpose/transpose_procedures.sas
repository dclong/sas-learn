
libname study "~/projects/study/lib";


id is used to decide whether different rows (in a group) are consider to a same record or not
and it be used to name columns after transpose.

/* a fake wide data set */
data wide1; 
 	input famid faminc96 faminc97 faminc98 ; 
	datalines; 
1 40000 40500 41000 
2 45000 45400 45800 
3 75000 76000 77000 
;
run; 

/* transpose by group */
proc transpose data=wide1 out=long1;
    by famid;
	id faminc96;
run;

proc print data=long1;
run;

/* a second fake wide data */
data wide2 ; 
    input famid faminc96 faminc97 faminc98 spend96 spend97 spend98 ; 
    datalines; 
1 40000 40500 41000 38000 39000 40000 
2 45000 45400 45800 42000 43000 44000 
3 75000 76000 77000 70000 71000 72000 
; 
run;

/* transpose by group */
proc transpose data=wide2 out=long2;
    by famid;
run;

/* a second fake wide data */
data wide2 ; 
    input famid faminc96 faminc97 faminc98 spend96 spend97 spend98 ; 
    datalines; 
1 40000 40500 41000 38000 39000 40000 
2 45000 45400 45800 42000 43000 44000 
3 75000 76000 77000 70000 71000 72000 
; 
run;



/* transpose by group for a subset of variables (variables with prefix famid) */
proc transpose data=wide2 out=longf (rename=(_name_=year));
    by famid;
    var faminc96-faminc98 ;
run;

proc print data=longf;
run;

/* transpose by group for a subset of variables (variables with prefix spend) */
proc transpose data=wide2 out=longs prefix=spend ;
    by famid;
    var spend96-spend98;
run;

/* merge the two transposed data set */
data long2;
   merge longf (rename=(faminc1=faminc) drop=_name_) longs (rename=(spend1=spend));
   by famid;
   year=input(substr(_name_, 6), 5.);
   drop _name_;
run;

proc print data=long2;
run;


data faminc_1 ; 
  input famid year faminc ; 
cards ; 
1 96 40000 
1 97 40500 
1 98 41000 
2 96 45000 
2 97 45400 
2 98 45800 
3 96 75000 
3 97 76000 
3 98 77000 
; 
run;

proc print data=longf;
run;

proc transpose data=s2 out=s3 (drop=_name_);
    by famid ;
	id year;
run;

proc transpose data=long1 out=wide1 prefix=faminc;
    by famid ;
    id year;
    var faminc;
run;

proc print data = wide1;
run;

data long2; 
  input famid year faminc spend ; 
cards; 
1 96 40000 38000 
1 97 40500 39000 
1 98 41000 40000 
2 96 45000 42000 
2 97 45400 43000 
2 98 45800 44000 
3 96 75000 70000 
3 97 76000 71000 
3 98 77000 72000 
; 
run ;

proc transpose data=long2 out=widef prefix=faminc;
   by famid;
   id year;
   var faminc;
run;

proc transpose data=long2 out=wides prefix=spend;
   by famid;
   id year;
   var spend;
run;

data wide2;
    merge  widef(drop=_name_) wides(drop=_name_);
    by famid;
run;

proc print data=wide2;
run;

data long3; 
  INPUT famid birth age ht ; 
cards; 
1 1 1 2.8 
1 1 2 3.4 
1 2 1 2.9 
1 2 2 3.8 
1 3 1 2.2 
1 3 2 2.9 
2 1 1 2.0 
2 1 2 3.2 
2 2 1 1.8 
2 2 2 2.8 
2 3 1 1.9 
2 3 2 2.4 
3 1 1 2.2 
3 1 2 3.3 
3 2 1 2.3 
3 2 2 3.4 
3 3 1 2.1 
3 3 2 2.9 
; 
run; 
proc transpose data=long3 out=wide3 prefix=ht;
   by famid birth;
   id age;
   var ht;
run;

proc print data=wide3;
run;

data long5; 
  length debt $ 3; 
  input famid year faminc spend debt $ ; 
cards; 
1 96 40000 38000 yes 
1 97 40500 39000 yes 
1 98 41000 40000 no 
2 96 45000 42000 yes 
2 97 45400 43000 no 
2 98 45800 44000 no 
3 96 75000 70000 no 
3 97 76000 71000 no 
3 98 77000 72000 no 
; 
run; 

proc transpose data=long5 out=widef prefix=faminc;
  by famid;
  id year;
  var faminc;
run;

proc transpose data=long5 out=wides prefix=spend;
  by famid;
  id year;
  var spend;
run;

proc transpose data=long5 out=wided prefix=debt;
  by famid;
  id year;
  var debt;
run;


proc transpose data=parms out=effects prefix=value name=effect;
  	var a b c d e ab ac ad ae bc bd be
            cd ce de abc abd abe acd ace ade
            bcd bce bde cde abcd abce abde 
            acde bcde abcde;
run;

