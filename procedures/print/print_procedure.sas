
proc print data=model1;
     title f=arial c=red h=4 "Fisher's Linear Discrimination Rule:";
run;

PROC PRINT DATA=LOANS N UNIFORM;
     VAR TYPE X1-X8;
RUN;

proc print data=bottle contents="Bottle" obs="ID";
	title "Bottle";
run;

proc print data=bottle contents="Bottle" noobs;
    title f=arial c=red h=5 "Bottle";
run;


proc print data=set1;
     title f="Arial" c=red h=4 "Data of husbands and wives";
run;

PROC PRINT DATA=grain N UNIFORM;
     VAR TYPE X1-X11;
RUN;

PROC PRINT DATA=SET1 UNIFORM SPLIT='*';
  	VAR X P Y;
  	LABEL X = 'BOILING POINT*OF WATER*(degrees F)'
          P = 'ATMOSPHERIC*PRESSURE*(inches Hg)'
          Y = 'NATURAL LOG OF*ATMOSPHERIC*PRESSURE';
  	TITLE 'FORBES DATA';
run;

PROC PRINT DATA=SET1 UNIFORM SPLIT='*';
	VAR X Y;
  	LABEL X = 'Bottle*Loss'
          Y = 'Cup*Loss';
    TITLE 'Bottle Loss Data';
run;

proc print data = fit (obs = 10);
run;
