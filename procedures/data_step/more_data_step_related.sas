
filename dst "C:\Documents and Settings\dclong\My Documents\Verizon";

%let path=C:\Documents and Settings\dclong\My Documents\Verizon\;

%put &path;

;
%let x=var

%put _user_;


%let path=

data calls;
	infile dst("call history.txt");
	input Date $ Time $ Number $ Rate $ Usage $ Type $ Origination $ Destination $ Minute Charges $;
run;


proc import datafile='C:\Documents and Settings\dclong\My Documents\Verizon\calls.xls' 
				out=calls dbms=xls;
run;

proc import datafile="&path.calls.xls" out=set5 dbms=xls replace ;
run;


proc import datafile=dst(calls.txt) out=set2 dbms=xls replace ;
run;

proc import datafile=&dst.calls out=set3 dbms=xls replace ;
run;

%let dst=""

proc print data=set1;
run;

proc print data=calls;
run;

proc sql;
	create table PeakCalls as 
		select rate,sum(min) as TotMin
			from calls
			group by rate
			order by rate;
	select *
		from PeakCalls;
quit;

proc sql;
	create table PlanCalls as 
		select usagetype,sum(min) as TotMin
			from calls
			group by usagetype
			order by usagetype;
	select *
		from PlanCalls;
quit;

proc print data=calls;
	where rate="Peak";
run;

proc sql;
	create table PayCalls as 
		select rate,usagetype,sum(min) as TotMin
			from calls
			group by rate,usagetype
			order by rate,usagetype;
	select *
		from PayCalls;
run;

