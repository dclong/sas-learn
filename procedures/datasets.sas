proc datasets library=tempDir noList;
	delete raw_all raw_all_temp;
	copy in=clean out=tempDir index=yes;
	select raw_all;
	change raw_all=raw_all_temp;
run;

proc datasets library=clean;
	delete bmo_all_st;
    change st_master=bmo_all_st;
	modify bmo_all_st;
	index create loanID / updatecentiles=always;
run;
