
proc stepdisc data=grain sw sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain backward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain forward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;


PROC STEPDISC DATA=LOANS SW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using stepwise method";
run;

PROC STEPDISC DATA=LOANS BW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using backward method";
run;
