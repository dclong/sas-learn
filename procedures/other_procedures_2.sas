
PROC FORMAT;
     VALUE ADMITFMT 1 = 'Admit'
                    2 = 'Do Not Admit'
                    3 = 'Borderline';
run;

PROC CORR DATA=bnotes;
     VAR X1-X6; 
run;

PROC RANK DATA=bnotes NORMAL=BLOM OUT=bnotes;
     VAR X1-X6; 
     RANKS Q1-Q6;
run;

proc insight data=set1;
 	scatter x1 x2 x3*x1 x2 x3;
run;

PROC CORR DATA=set1;
     VAR X1-X4;
RUN;

proc corr data=scores;
     var prin1 prin2 prin3 x1-x10 age;
run;

proc corr data=set1;
     var dBOD dSS; 
run;

PROC BOXPLOT DATA=set1;
     PLOT (X1-X4)*Z / BOXSTYLE=SCHEMATICID;
run;


PROC BOXPLOT DATA=set1;
     PLOT (x1 x2 x3 x4)*group / BOXSTYLE=SCHEMATICID;
run;

PROC SORT DATA=set1; 
      BY group; 
run;

proc freq data=set2;
  	tables work*attribute / chisq;
  	weight y;
run;

proc corresp data=set1 out=results cp rp dimens=2 profile=both short;
  	var c1-c15;
  	id work;
run;


proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
  	var x1-x10;
run;
/*   Compute Q-Q plots  */

PROC RANK DATA=set1 NORMAL=BLOM OUT=set1;
     VAR X1-X4; 
     RANKS Q1-Q4;
RUN;

proc freq data=x2;
  	tables work*job / chisq;
  	weight y;
run;

PROC FREQ DATA=SET3;
  	TABLES COL2*COL1 / NOPERCENT NOCOL;
RUN;

proc freq data=set2 order=data;
  	tables parents*health / chisq;
  	weight y;
run;

proc corresp data=france out=results cp rp dimens=3 profile=both short  ;
     var      varw free huma sche sala secu comp inte near atmo soci auto like
           othe none outd noa node grad smal medi larg;
     supplementary node grad smal medi larg;
     id  work;
run;

proc corresp data=set1 out=results cp rp dimens=3 profile=both short  ;
     var well mild moderate impaired;
     id parents;
run;

PROC BOXPLOT DATA=set1;
     PLOT (X1 X2)*temp / BOXSTYLE=SCHEMATICID;
run;

proc print data=set1;
     title f="Arial" c=red h=4 "Data of husbands and wives";
run;

PROC PRINT DATA=grain N UNIFORM;
     VAR TYPE X1-X11;
RUN;

proc standard data=set1 out=set2 mean=0 std=1 ;
   	var x1-x5;
run;

PROC TREE DATA=TREE NOPRINT OUT=OUT NCL=&N;
  	COPY PATIENT x1-x5;
   	TITLE2 "COMPUTE &N CLUSTERS USING METHOD=&METHOD";
run;

PROC CANDISC data=out simple OUT=CAN ncan=5 prefix=can;
     CLASS cluster;
     VAR x1-x5;
run;

PROC DISCRIM DATA=set1 POOL=test SLPOOL=.01 
     WCOV PCOV;  
     CLASS group;  
     VAR X1-X4;
run;

PROC DISCRIM DATA=set1 POOL=TEST SLPOOL=.001 WCOV PCOV CROSSLIST
                WCORR PCORR Manova testdata=set2 testlist;
     CLASS Admit;
     FORMAT TYPE ADMITFMT.;
     VAR GPA GMAT;
run;

PROC DISCRIM DATA=SET1 POOL=TEST SLPOOL=.01 
     WCOV PCOV;
     CLASS TEMP;
     VAR X1-X2;
run;

proc princomp data=females cov n=3 out=scores prefix=prin;
     var length width height;
run;

proc corr data=scores;
     var prin1 prin2 prin3 length width height;
run;

proc princomp data=females out=pcorr prefix=pcorr;
     var length width height;
run;

PROC PRINCOMP DATA=SET1 COV N=3 PREFIX=PRIN;
     PARTIAL SEX;
     VAR LENGTH WIDTH HEIGHT;
RUN;

/* COMPUTE PRINCIPAL COMPONENTS SEPARATELY FOR EACH SEX */

PROC PRINCOMP DATA=SET1 OUT=SCORE2 COV N=3 PREFIX=PRIN;
     BY SEX;
     VAR LENGTH WIDTH HEIGHT;
RUN;

/* COMPUTE PRINCIPAL COMPONENTS FROM THE COVARIANCE
MATRIX FOR THE COMBINED DATA */

PROC PRINCOMP DATA=SET1 OUT=SCORE3 COV N=3 PREFIX=PRIN;
     VAR LENGTH WIDTH HEIGHT;
RUN;


proc discrim data=owls pool=test wcorr pcorr;
  	class type;
  	var km91 km118 km140 km160 km177 km241 km338 ;
run;

proc discrim data=grain pool=test anova crosslist manova method=normal 
              posterr simple slpool=0.01 outstat=dis1;
	 class type;
	 var x1-x11;
	 format type t.;
run;

proc boxplot data=grain;
     plot (x1-x11)*type/boxstyle=schematic;
run;

/*  Force the program to do linear discriminant analysis  */

PROC DISCRIM DATA=grain POOL=YES CROSSVALIDATE METHOD=NORMAL outstat=model1;
     CLASS TYPE;
     VAR X1-X11;
     FORMAT TYPE T.;
run;

proc print data=model1;
     title f=arial c=red h=4 "Fisher's Linear Discrimination Rule:";
run;

proc stepdisc data=grain sw sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain backward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain forward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x6 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=yes CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

proc standard data=set1 out=set2 mean=0 std=1 ;
   	var x1-x5;
run;


/*  Use  FASTCLUS  to do K-means cluster analysis  */

PROC FASTCLUS DATA=SET2 OUT=SET3 MAXC=9 MAXITER=30 CONVERGE=.02 LIST;
   	VAR X1-X5;
   	ID patient;
   	TITLE "K-means Cluster ANALYSIS: Stanford Diabetes Data";
   	TITLE2;
RUN;

PROC CANDISC DATA=SET3 SIMPLE OUT=CAN NCAN=5 PREFIX=CAN;
   	CLASS CLUSTER;
   	VAR x1-x5;
RUN;

PROC PRINT DATA=LOANS N UNIFORM;
     VAR TYPE X1-X8;
RUN;


/* Examine Box Plots */

proc boxplot data=loans;
     plot (x1-x8)*type / boxstyle=schematic;
run;


/* Test for homogeneous covariance matrices.
   In this case the program decides to use
   quadratic discriminant analysis        */

PROC DISCRIM DATA=roos POOL =Yes OUTSTAT=outdat.roos1;
  	CLASS type;
  	VAR x1 x3 x7 x9 x14 x15;
  	format type troo.;
run;

proc discrim data=outdat.roos1 testdata=oldroos testlist;
  	class type;
  	var x1 x3 x7 x9 x14 x15;
  	testclass type;
  	FORMAT TYPE troo.;
run;

PROC DISCRIM DATA=LOANS POOL=TEST ANOVA CROSSLIST MANOVA METHOD=NORMAL POSTERR
               SIMPLE SLPOOL=.01 OUTSTAT=DISCRIM1;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE METHOD=NORMAL;
     CLASS TYPE;
     VAR X1-X8;
     ID ID;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f="Arial" c=red h=4 "Discrimination Analysis Using Linear Rule";
run;


/*  Use linear discriminant analysis to
    search for a good subset of variables */

PROC STEPDISC DATA=LOANS SW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using stepwise method";
run;

PROC STEPDISC DATA=LOANS BW SLE=.25 SLS=.25;
     VAR X1-X8;
     CLASS TYPE;
	 title f="Arial" c=red h=4 "Variable selection using backward method";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discriminaiton using variable X2";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=Arial c=red h=4 "Discrimination analysis using variable X2 and X7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using variables x2, x4 and x7";
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=LOANS POOL=YES CROSSVALIDATE;
     CLASS TYPE;
     VAR X2 X7 X4 X8 X3 X6;
     PRIORS 'GOOD'=.8 'BAD'=.2;
     FORMAT TYPE T.;
run;

PROC DISCRIM DATA=loans POOL =Yes OUTSTAT=outdat.rule1;
     CLASS type;
     VAR x2 x7;
     PRIORS "GOOD"=0.8 "BAD"=0.2;
     FORMAT TYPE T.;
run;

proc discrim data=outdat.rule1 testdata=set2 testlist;
     class type;
     var x2 x7;
     testclass type;
     FORMAT TYPE T.;
run;

proc princomp data=set1 cov n=3 out=scores prefix=prin;
     var x1-x10;
run;

proc princomp data=set1 n=3 out=pcorr prefix=pcorr;
     var x1-x10;
run;

title ;
