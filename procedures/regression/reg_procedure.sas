
proc reg data=set1;
  	model y1 = x3 x12 x22 x33 / partial influence all vif  collin;
  	output out=set2 r=residual p=yhat;
run;

proc reg data=set1;
     model y = x1 x2 x12 / p ss1 ss2 clm cli partial
                        vif collin influence;
     output out=set2 r=residual p=yhat;
run;

proc reg data=set1 outest=parms covout;
  	model y = x1 x2 x3 /p covb clm cli ss1 ss2;
  	output out=setall residual=r predicted=yhat stdp=stdmean
       		l95m=lmeam u95m=umean stdi=stdpred l95=lpred u95=upred;
run;

/*  Fit a bunch of models  */
proc reg data=set1;
    model y = x1 x2 x12 x11 x22 x112 x221  x111 x222 / 
         selection=rsquare cp aic sbc mse b adjrsq;
run;

proc reg data=set1 outest=parms covout;
  	model y = x1 x2 x3 /p covb clm cli ss1 ss2;
  	output out=setall residual=r predicted=yhat stdp=stdmean
       		l95m=lmeam u95m=umean stdi=stdpred l95=lpred u95=upred;
run;

proc reg data=set1 outest=parms covout;
  	model y = x1 x2 x3 / vif ;
  	plot y*(x1 x2 x3) / hplots=2 vplots=2;
  	plot residual.*(x1 x2 x3 predicted. )/ hplots=1 vplots=4;
run;

proc reg data=set1 outest=parms covout;
  	model y = x1 x2 x3 / selection=rsquare cp aic bic 
				start=0 stop=3 best=5;
run;

proc reg data=set1;
	model biomass = x1-x5 /vif collin partial;
    output out=set1 residual=resid predicted=yhat; 
run;

proc reg data=set1;
 	model biomass = x1-x5 / selection= rsquare
          cp aic sbc mse best=5 stop=5; 
run;

proc reg aic sbc cp mse;
  	model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    					/ selection = cp best = 5 b;
  	model lweight = lage llength nsex lchest lheadlen lheadwid lneck
    			/ selection = stepwise slstay = 0.20 slentry = 0.20;
run;
proc reg;
  	model logbrain = loggest logbody loglitter /vif influence ;
/* the vif option gives you the vif for each obs */
/* the influence option gives you h, dffits and dfbeta for each obs */    
  	output out = resids r = resid rstudent = rs 
                      p = yhat cookd = cookd dffits = dffit h = h;
                      
/* various diagnostics can be stored in the output dataset */
/*   the syntax for all is keyword =  variable name */
/*   rstudent =  gives you externally studentized residual */
/*   cookd =  gives you cooks distance */
/*   dffits = gives you dffits */
/*   h =  gives you Hii */
     
/* there are many other pieces of info that can be stored.  See the proc reg */
/*   documentation for all the details */                      
run;

proc reg;
   	model logbrain = logbody loglitter loggest;
	test1: test loglitter = 0;
	test2: test loglitter = 0, loggest = 0;
	test3: test loglitter+loggest = 1;
   	title 'Regression of log brain size on log body and log litter';
   	title2' With test statements'; 
run;

proc reg;
  	model y =  x /dwprob;
  	title 'regression with Durbin-Watson statistic';
run;

