
PROC IML;
     START NORMAL;
     USE set2;            /* Enter the data */
     READ ALL  INTO X;
     N=NROW(X);             /* Number of observations is N */
     P=NCOL(X);             /* Number of traits is p */
     SUM=X[+, ];            /* Total for each trait */
     A=X`*X-SUM`*SUM/N;     /* Corrected crossproducts matrix */
     S=A/(N-1);             /* Sample covariance matrix */
     XBAR=SUM/N;            /* Sample mean vector */
     SCALE=INV(SQRT(DIAG(A)));
     R=SCALE*A*SCALE;       /* Sample correlation matrix */
     PTR=J(P,P); /* 1 matrix with dimension P*P*/
     TR=J(P,P);
     DF=N-2;
     DO I1=1 TO P;          /* T-tests for correlations */
         IP1=I1+1;
         DO I2=IP1 TO P;
               TR[I1,I2]=SQRT(N-2)#R[I1,I2]/SQRT(1-R[I1,I2]##2);
               TR[I2,I1]=TR[I1,I2];
               PTR[I1,I2]=(1-PROBT(ABS(TR[I1,I2]),DF))*2;
               PTR[I2,I1]=PTR[I1,I2];
         END;
     END;
     RINV=INV(R);           /* Partial Correlations */
     SCALER=INV(SQRT(DIAG(RINV)));
     PCORR=-SCALER*RINV*SCALER;
     DO I = 1 TO P;
          PCORR[I,I]=1.0;
     END;
     PTPCORR=J(P,P);
TPCORR=J(P,P);
DF=N-P;
DO I1=1 TO P;  /* T-tests for partial correlations */
IP1=I1+1;
  DO I2=IP1 TO P;
    TPCORR[I1,I2]=SQRT(N-P)#PCORR[I1,I2]/
                  SQRT(1-PCORR[I1,I2]##2);
    TPCORR[I2,I1]=TPCORR[I1,I2];
    PTPCORR[I1,I2]=
        (1-PROBT(ABS(TPCORR[I1,I2]),DF))*2;
    PTPCORR[I2,I1]=PTPCORR[I1,I2];
  END;
END;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;
PRINT,,,,," VALUES OF T-TESTS FOR ZERO CORRELATIONS";
PRINT TR;
PRINT,,,,," P-VALUES FOR THE T-TESTS FOR ZERO CORRELATIONS";
PRINT PTR;
PRINT,,,,," THE MATRIX OF PARTIAL CORRELATIONS CONTROLLING FOR";
PRINT "       ALL VARIABLES NOT IN THE PAIR";
PRINT PCORR;
PRINT,,,,," VALUES OF T-TESTS FOR PARTIAL CORRELATIONS";
PRINT TPCORR;
PRINT,,,,," P-VALUES FOR T-TESTS FOR PARTIAL CORRELATIONS";
PRINT PTPCORR;

/* Compute plotting positions
   for a chi-square probability plot */

E=X-(J(N,1)*XBAR);       
D=VECDIAG(E*INV(S)*E`);  /* Squared Mah. distances */
RD = RANK(D);            /* Compute ranks  */
  RD=(RD-.5)/N;
PD2=P/2;
  Q=2*GAMINV(RD,PD2);    /* Plotting positions */
  DQ=D||Q;
  print DQ;
CREATE CHISQ FROM DQ ;   /* Open a file to store results */
APPEND FROM DQ;

                         /* Compute test statistic */
rpn = t(D)*Q - (sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)
         *(ssq(Q)-(sum(Q)**2)/N));

/* Simultate a p-value for the correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)
           *(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;

RUN NORMAL;
quit;
