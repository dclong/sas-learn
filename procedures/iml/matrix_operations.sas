


/*  
The IML procedure in SAS can perform 
    basic matrix computations as well as 
    a variety of other functions 

    We will use the "start" command to create
    a module named "new".  This module is 
    executed with the statement  "run new:" 
*/ 


proc iml;
	start new;
 
/* A matrix can be entered directly using
   the { } brackets.  A comma is used to 
   end each row in the matrix.  The following
   enters the 5x2 matrix on page 21 of the 
   notes.  Note that any command must be 
   ended with a semicolon. */

	x = {42 4, 52 5, 88 7, 58 4, 60 5};

/* This matrix is printed with the 
   following command  */

	print x;

/* Compute the number of rows in X with the
   nrow function and compute the numbers of 
   columns with the ncol function.  */

	n = nrow(x);
	p = ncol(x);	
	print ,, "number of rows: " n;
	print ,, "number of columns: " p;

/* Compute column sums.  Square brackets
   are used to select rows or columns of
   a matrix.  The plus sign below is used
   to sum across rows. */

	csum = X[+, ];    

/* Compute a row vector of sample means */

	means = csum/n;

	print ,,, n,,, csum,,, means;

/*  Compute the sample covariance matrix
    on page 22 of the notes.  This requires
    matrix multiplication denoted by the *
    operator and the transpose of a matrix
    obtained with the t( ) function  */

	s = (x`*x-csum`*csum/n)/(n-1);    

/* Compute the correlation matrix on page 22 
   The diag( ) function creates a diagonal matrix.
   The sqrt( ) function computes the square root
               of each element in a matrix 
   The inv( ) function computes the inverse of a 
               matrix */

	scale = inv(sqrt(diag(s)));
	r = scale*s*scale;     
	print ,,, S,,, scale,,, R;

/* Define the vectors on page 49 and
   compute transpose, multiply by a 
   scalar, and add two vectors  */

	x = {2, 1, -4};
	y = {5, -2, 0};
	xt = t(x);
	w = 6 * x;
	z = x + y;
	d = x - y;
	print ,, x y xt w z d;

/* sum of squares and length (page 52)  */

	x={2, 1, -4, -2};
	ssx = ssq(x); 
	lengthx = sqrt(ssq(x));
	print x ssx lengthx;

/* inner product and cos of angle */

	x = {2, 1, -4};
	y = {5, -2, 0};
	lx = sqrt(t(x)*x);
	ly = sqrt(sum(y#y));
	xy = sum(x#y);
	cos = xy/(lx*ly);
	print x y lx ly xy cos;

/* examples on page 62 */

	x = {2 1 -4, 5 7 0};
	xt = t(x);
	z = 6 * x;
	w = {2 -1, 0 3} + {2 1, 5 7};
	print x xt z w;

/* matrix multiplication (page 64) */

	a = {2 0 1, 5 1 3};
	b = {1 4, -1 3, 0 2};
	ab = a * b;
	c = {2 1 , 5 3};
	d = {1 4, -1 3};
	cd = c * d;
	dc = d * c;

/* Elementwise multiplication uses the
   "#" operator  */

	w = c # d;

	print ,, a b ab  ,, c d cd dc w;

/* Create identity matrices (page 65) */

	i3 = i(3);
	d = i(2);

	print d i3;

/* create matrices in which all
   entries are the same  */

	a = j(2, 3);
	b = j(2, 3, 0);
	c = j(4,2,-3);

	print ,,a b c;

/* Inverse matrix */

	a = {9 2, 2 4};
	ainv = inv(a);
	w = a * ainv;

	b= { 6 3 1, 3 9 2, 1 2 5};
	binv = inv(b);

	print ,, a ainv w ,, b binv ;

/* determinant of a matrix */

	da = det(a);
	db = det(b);

	print ,,"determinants: " da db;

/* Compute eigenvalues and eigenvectors.  In
   the following call to the eigen function to
   find the eigenvalues and eigenvectors of A,
   M names a diagonal matrix of eigenvalues and
   E names a matrix with eigenvectors as columns */

	a = { 6 3 1, 3 9 2, 1 2 5};
	call eigen(m, e, a);

	print ,,a m e;

	/* Show that E is an orthogonal matrix */

	ete=t(e)*e;
	eet=e*t(e);

	print ,, ete eet;

	/* Compute the inverse of A, a square root matrix,
	and an inverse square root matrix */

	ainv= e*inv(diag(m))*t(e);
	asqrt= e*sqrt(diag(m))*t(e);
	asqrtinv = e*inv(sqrt(diag(m)))*t(e);

	print ,, ainv asqrt asqrtinv;

/* Compute trace and determinant of A */

	trace_a=trace(a);
	det_a=det(a);

	print ,, trace_a det_a;

/* Compute covariance matrix for conditional
   normal distribution (pages 126-127)  */

	s={4 0 1 3, 0 4 1 1, 1 1 3 1, 3 1 1 9};
	r1 = {1 3};
	r2 = {2 4};
	s11=s[r1,r1];
	s22=s[r2, r2];
	s12=s[r1, r2];
	s21=s[r2, r1];
	v = s11-s12*inv(s22)*s21;

	print ,,, s ,,, s11 s22 s12 s21 ,,, v;

/* the "finish" statement ends the module */

	finish;

/* Now execute the module */
	run new;
quit;

