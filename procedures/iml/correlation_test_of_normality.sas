
proc iml;
/*
correlation test of normality
*/
    start normal;
    use newnotes;          *enter the data;
    read all  into x;
    n=nrow(x);             *number of observations is n;
    p=ncol(x);             *compute number of traits = p;
    sum=x[+, ];            *compute total for each trait;
    a=x`*x-sum`*sum/n;     *compute corrected crossproducts matrix;
    s=a/(n-1);             *compute sample covariance matrix;
    xbar=sum/n;            *compute transpose of sample mean vector;
    scale=inv(sqrt(diag(a)));
    r=scale*a*scale;       *compute sample correlation matrix;

    print,,,,," the sample mean vector";
    print xbar;
    print,,,,," the sample correlation matrix";
    print r;

    * compute a chi-square probability plot;

    e=x-(j(n,1)*xbar);     *matrix of deviations from mean vector;
    d=vecdiag(e*inv(s)*e`);    *squared mahalanobis distances;
    rd = rank(d);            * compute ranks for the d values;
    rd =(rd-.5)/n;
    pd2=p/2;
    q=2*gaminv(rd,pd2);  

    /* compute chi-square plotting positions */
    dd=d||q;
    create chisq from dd ;   

    /* open a file to temporarily store results */
    append from dd;

    /* compute the p-value for a correlation test
    of multivariate normality, using the points on 
    chi-square probability plot */

    rpn = t(d)*q-(sum(d)*sum(q))/n;
    rpn = rpn/sqrt((ssq(d)-(sum(d)**2)/n)*(ssq(q)-(sum(q)**2)/n));

    /* simultate the p-value for the 
    correlation test  */

    ns=10000;
    pvalue=0;
    do i=1 to ns;
    do j1=1 to n;
    do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

    sumx=x[+, ];           
    a=t(x)*x-t(sumx)*sumx/n;     
    s=a/(n-1);             
    xbar=sumx/n;           
    e=x-(j(n,1)*xbar);   
    dn=vecdiag(e*inv(s)*t(e));   
    rn = rank(dn);            
    rn =(rn-.5)/n;
    pd2=p/2;
    qn=2*gaminv(rn,pd2);  
    rpnn = t(dn)*qn-(sum(dn)*sum(qn))/n;
    rpnn = rpnn/sqrt((ssq(dn)-(sum(dn)**2)/n)*(ssq(qn)-(sum(qn)**2)/n));
    if(rpn>rpnn) then pvalue=pvalue+1;   
    end;
    pvalue=pvalue/ns;

    print,,,,,"correlation test of normality";
    print n p rpn pvalue;

    finish;
    run normal;
quit;

