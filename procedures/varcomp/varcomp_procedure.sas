
proc varcomp data=set1 method=reml;
  	class batch sample;
  	model y = batch sample(batch);
run;

proc varcomp method = type1;
  	class bale;
  	model clean = bale ;
  	title 'Method of moments estimates, all data ';
run;

proc varcomp method = ml;
  	class bale;
  	model clean = bale ;
  	title 'ML estimates, all data ';
run;

proc varcomp method = type1;
  	class bale farm;
  	model clean = farm bale(farm);
run;
