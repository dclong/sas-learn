
PROC RANK DATA=bnotes NORMAL=BLOM OUT=bnotes;
     VAR X1-X6; 
     RANKS Q1-Q6;
run;

proc rank data=set2 normal=blom out=set2;
      var residual; 
      ranks q;
run;

proc rank data=sasuser.brain out=nrb (keep=rh rd);
	var brain body;
	ranks rh rd;
run;

proc rank data=bottle out=rb;
	var day1-day4;
	ranks q1-q4;
	by model;
run;

PROC RANK DATA=set1 NORMAL=BLOM OUT=set1;
     VAR X1-X4; 
     RANKS Q1-Q4;
RUN;

