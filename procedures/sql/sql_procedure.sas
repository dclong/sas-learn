
filename bottle url "http://www.public.iastate.edu/~pdixon/stat500/data/bottle.txt";

data sasuser.bottle;
	infile bottle;
	input model machine day1 day2 day3 day4 day5;
	if _n_ ne 1;
run;

proc print data=bottle;
	title "Data Set bottle";
run;

proc sql;
	title "SQL Procedure";
	select model,machine,day1,day2 as set2
		from bottle
		where machine>=3
		order by day1;
quit;

proc print data=set2;
	title "Data Set set2";
run;

proc sql;
	title "SQL Procedure";
	select model,machine,day1,day2,day3+day4+day5 as SUM
	from bottle
	where machine ge 3
	order by model desc, 2 desc;
quit;

data set1;
	title "Data Set set1";
	set bottle;
	keep model machine day1 day2;
run;

proc print data=set1;
run;

data set2;
	title "Data Set set2";
	set bottle;
	keep model machine day3 day4;
run;

proc print data=set2;
run;

proc sql;
	title "Combine Two Data Sets";
	select set1.model,set1.machine,day1,day2,day3,day4
		from set1,set2
		order by 1 desc,2 desc;
quit;

proc print data=bottle;
run;

proc sql;
	title "PROC SQL GROUP";
	select model,mean(day1) as avg
		from bottle
		group by model
		order by model;
quit; 

proc sql;
	title "PROC SQL, COUNT";
	select model,day1, count(day1) as count
		from bottle
		where model<3
		group by model
		order by 1 desc;
quit;



		

proc print data=sasuser.bottle;
run;

proc sql;
	title "proc sql CSS";
	select model,css(day1) as css
		from sasuser.bottle
		where model ne 4
		group by model
		order by model desc;
quit;

proc sql;
	title "Create a Table";
	create table set1 as 
		select model,machine,day1,day2 
			from sasuser.bottle
			where mod(model,2) ne 1
			order by 1,2;
quit;

proc print data=set1;
run;

proc sql;
	select model,avg(day1)
		from sasuser.bottle
		group by model
		order by 1;
quit;

proc sql;
	title "Use of Having Clause";
	select model,day1
		from sasuser.bottle
		group by model
		having avg(day1)>60
		order by model;
quit; 

proc print data=sasuser.bottle;
run;

proc sql;
	select avg(day1) as average
		from sasuser.bottle;
quit;

proc sql;
	select model,avg(day1) as average
		from sasuser.bottle
		group by model 
		order by model;
quit;

proc sql;
	title "Subqueries";
	title2 "> average";
	select model,avg(day1) as average format=dollar8.2
		from sasuser.bottle
		group by model
		having avg(day1)>
			(select avg(day1) from sasuser.bottle)
		order by model desc;
quit;

proc sql feedback;
	title "Select All Columns";
	select * 
		from sasuser.bottle;
quit;







proc print data=sasuser.bottle;
run;

proc sql outobs=5;
    create table part as 	
		select * 
			from sasuser.bottle;
quit;

proc print data=part;
run;

proc sql inobs=3;
	create table set1 as 
		select *
			from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	select distinct model
		from sasuser.bottle
			order by 1;
quit;





filename brain url "http://www.public.iastate.edu/~pdixon/stat500/data/brain.txt";

data sasuser.brain;
	infile brain;
	input gest brain body litter species $;
run;

proc print data=sasuser.brain;
	title "Data Set brain";
run;

proc sql;
	title "concatenation";
	select * 
		from sasuser.brain
		where species="""White" || "-h"
		order by gest;
quit;

proc sql;
	title "and";
	select *
		from sasuser.brain
		where gest>=100 and gest <=200;
quit;

proc sql;
	title "between and";
	select *
		from sasuser.brain
		where gest between 100 and 200;
quit;

proc sql;
	title "contains";
	select * 
		from sasuser.brain
		where species contains "White";
quit;

proc sql;
	title "in";
	select *
		from sasuser.brain
		where species in ("""White-h","""Tree","""Slow")
		order by gest;
quit;

proc sql;
	title "missing values";
	select *
		from sasuser.brain
		where gest is missing
		order by 1;
quit;

proc sql;
	title "like";
	select *
		from sasuser.brain
		where species like "%h_"
		order by 1;
quit;

proc sql;
	title "Sound like";
	select *
		from sasuser.brain
		where species=*"slow"
		order by 1;
quit;

proc sql;
	footnote "footnote";
	select model,machine,day1+day2+day3 as total label="Total"
		from sasuser.bottle
		order by 1,2;
quit;

proc sql;
	footnote;
	select model,machine,"the sum is" as SUM,day1+day2 as sum
		from sasuser.bottle
		order by 1,2;
quit;

proc sql;
	select day1,avg(day1) as average
		from sasuser.bottle
		where day1>70;
quit;

proc sql;
	select avg(day1) 
		from sasuser.bottle;
quit;

proc sql;
	select day1+day2 as average
		from sasuser.bottle;
quit;





filename plate url "http://www.public.iastate.edu/~pdixon/stat500/data/platelet2.txt";

data sasuser.plate;
	infile plate;
	input subject time $ aggreg;
run;

proc print data=sasuser.plate;
run;

proc sql;
	title "sound like";
	select *
		from sasuser.plate
		where time=*"BeFore"
		order by subject;
quit;

proc print data=sasuser.plate;
run;

proc sql;
	title "exist";
	select * 
		from sasuser.plate
		where exists
		(select subject 
		 	from sasuser.plate)
		order by 1;
quit;






proc print data=sasuser.bottle;
run;

proc sql;
	title "all";
	select *
		from sasuser.bottle
		where model>1 and day1>all
		(select day1
			from sasuser.bottle
			where model=1)
		order by model,machine;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	select model,machine,day1+day2+day3 as total
		from sasuser.bottle
		where calculated total<=200
		order by 1,2;
quit;

proc sql;
	select avg(day1+day2+day3)
		from sasuser.bottle;
quit;

proc sql;
	create table set1 as 
	select model,machine,day1+day2+day3 as total
	from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	select avg(total) 
		from set1;
quit;

proc sql;
	select model,machine,day1+day2+day3 as total,calculated total/2 as half
		from sasuser.bottle;
quit;



proc print data=sasuser.plate;
run;

data sasuser.plate2;
	set sasuser.plate;
	if time="time" then time="";
run;

proc print data=sasuser.plate2;
run;

proc sql;
	select nmiss(time) as count 
		from sasuser.plate2;
quit;

proc sql;
	select distinct time as count
		from sasuser.plate2;
quit;

proc sql;
	select time,count(subject) as count
		from sasuser.plate2
		group by 1;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	select model,machine,avg(day1)
		from sasuser.bottle
		group by model;
quit;

proc sql;
	validate
	select model,machine,day1/sum(day1) as percent
		from sasuser.bottle
		order by 1,2;
quit;

data sasuser.one;
	input x A $;
	datalines;
	1 a
	2 b
	4 d
run;

proc print data=sasuser.one;
run;

data sasuser.two;
	input x B $;
	datalines;
	2 x
	3 y
	5 v
run;

proc print data=sasuser.two;
run;

proc sql;
	select *
		from sasuser.one,sasuser.two ;
run;

data one;
	set sasuser.one;
run;

data two;
	set sasuser.two;
run;

proc sql;
	create table three as 
	select *
		from sasuser.one,sasuser.two
		where one.x=two.x;
quit;

proc sql;
	create table three as 
	select one.x as ID,two.x,a,b
		from sasuser.one, sasuser.two
		where one.x=two.x;
quit;

data sasuser.three;
	input x A $;
	datalines;
	1 a1
	1 a2
	2 b1
	2 b2
	4 d
run;

proc print data=sasuser.three;
run;

data sasuser.four;
	input x B $;
	datalines;
	2 x1
	2 x2
	3 y
	5 v
run;

proc print data=sasuser.four;
run;

proc sql;
	select *
		from sasuser.three,sasuser.four
		where three.x=four.x;
quit;

data set1;
	merge sasuser.three sasuser.four;
	if three.x=four.x;
run;

proc print data=set1;
run;











proc print data=sasuser.three;
run;

proc sql;
	select *
		from sasuser.three,sasuser.four
		where three.x=four.x;
quit;

proc sql;
	select t.x,t.A,f.B
		from sasuser.three t, sasuser.four f
		where t.x=f.x;
quit;

proc sql;
	select *
		from sasuser.three t1,sasuser.three t2
		where t1.x=t2.x;
quit;

proc sql;
	select *
		from sasuser.one full join sasuser.two
		on one.x=two.x;
quit;

proc sql;
	select two.x,A,B
		from sasuser.one right join sasuser.two
		on one.x=two.x;
quit;

proc sql;
	select two.x,A,B
		from sasuser.one right join sasuser.two
		on one.x=two.x
		where two.x>2;
quit;

proc sql;
	select *
		from sasuser.one inner join sasuser.two
		on one.x=two.x;
quit;

proc print data=sasuser.two;
run;

proc print data=sasuser.six;
run;

data sasuser.five;
	input x A $;
	datalines;
	1 a
	2 b
	3 c
run;

data sasuser.six;
	input x B $;
	datalines;
	1 x
	2 y
	3 z
run;

proc sql;
	select five.x,a,b
	from sasuser.five,sasuser.six
		where five.x=six.x;
run;

data merged;
	merge sasuser.one sasuser.two;
	by x;
run;

proc print data=merged;
run;

proc print data=sasuser.three;
run;

proc sql;
	select coalesce(one.x,two.x) as x,a,b
		from sasuser.one full join sasuser.two
		on one.x=two.x;
quit;

proc print data=sasuser.eight;
run;

data sasuser.seven;
	input x A $;
	datalines;
	1 a
	1 a
	1 b 
	2 c
	3 v
	4 e
	6 g
run;

data sasuser.eight;
	input x B $;
	datalines;
	1 x
	2 y
	3 z
	3 v 
    5 w
run;

proc sql;
	select count(*) as NO
		from
	(select seven.x,a
		from sasuser.seven
	except corr	all
	select eight.x,b
		from sasuser.eight);
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect all
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect corr
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	intersect corr all
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	union
	select eight.x,b
		from sasuser.eight;
quit;

proc sql;
	select seven.x,a
		from sasuser.seven
	union corr all
	select eight.x,b
		from sasuser.eight;
quit;


proc sql;
	select seven.x,a
		from sasuser.seven
	outer union corr
	select eight.x,b
		from sasuser.eight;
quit;

data set1;
	set sasuser.seven sasuser.eight;
run;

proc print data=set1;
run;








proc sql;
	create table discount
		(Destination char(3),
		 BeginDate num Format=date9.,
		 EndDate num format=date9.,
		 Discount num);
	
quit;

proc sql;
describe table discount;
quit;

proc print data=discount;
run;

proc sql;
	create table nine
		like sasuser.bottle;
quit;

proc sql;
	describe table nine;
quit;

data ten;
	input x A $ length=3;
	datalines;
	1 adu
	2 Benjamin
run;

proc print data=ten;
run;



proc sql;
	create table set1 (keep=model machine day1)
		like sasuser.bottle;
quit;

proc sql;
	describe table set1;
quit;

proc sql;
	describe table sasuser.bottle;
quit;

proc sql;
	create table set2 as 
		select model,machine,day1
			from sasuser.bottle;
quit;

proc print data=set2;
run;

proc sql;
	create table set3 as 
		select * 
			from sasuser.bottle;
quit;

proc print data=set1;
run;

proc sql;
	describe table set1;
quit;

proc sql;
	insert into set1
		set model=4,
			machine=1
		set model=4;
quit;

proc sql;
	insert into set1 (model,machine)
	values (5,.)
	values (5,3)	;
quit;

proc print data=set1;
run;

proc sql;
	insert into set1 (model,machine)
		select model,machine
			from sasuser.bottle
			where model=2;
quit;


proc sql;
	create table set1
		(drop=day2 day3 day4)
		like sasuser.bottle;
quit;

proc sql;
	describe table set1;
quit;

proc sql;
	create table set2
		(keep=model machine day1)
		like sasuser.bottle;
quit;

proc sql;
	describe table set2;
quit;

proc sql;
	create table set3
		(ID char(5) primary key,
		name char(10) ,
		Gender char (1) not null check(gender in ("M","F")),
		HDate date label="Hire Date");
quit;

proc sql;
	create table set4
		(ID char(5) primary key,
		day1 num,
		day2 num check (day2>day1));
quit;

proc sql;
	describe table set4;
quit;

proc sql;
	insert into set4
		set ID="ID00",
			day1=2.3,
			day2=32;
quit;

proc print data=set4;
run;

proc sql undo_policy=none;
	create table set5
		(Destination char(3),
		BeginDate num format=date9.,
		EndDate num format=date9.,
		Discount num,
		constraint ok_discount check (discount le 0.5),
		constraint notnull_dest not null (destination));
quit;

proc sql;
	describe table set5;
quit;

proc sql; 
	insert into set5
		values("CDG","03mar2000"d,"10mar2000"d,.15)
		values("LHR","10mar2000"d,"12mar2000"d,.55);
quit;

proc print data=set5;
run;

proc sql;
	describe table set5;
quit;

proc sql;
	describe table constraints set5;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day1=20
		where model=1;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day2=0;
	select *
		from sasuser.bottle;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day3=0
			where model=1;
quit;

/*SAS won't write value into the data set until the end of the sql procedure*/

proc sql;
	update sasuser.bottle
		set day3=1
			where model=1;
	update sasuser.bottle
		set day3=2
			where model=2;
	update sasuser.bottle
		set day3=3
			where model=3;
	update set1
		set  a="good";
quit;

data set1;
	set sasuser.one;
run;

proc print data=set1;
run;

proc sql;
	update sasuser.bottle
		set day4=0,day5=1
			where model=2;
quit;

proc sql;
	update sasuser.bottle
		set day5=
		case
			when model=1 then 1
			when model=2 thne 2
			else 3
		end;
quit;

proc print data=sasuser.bottle;
run;

proc sql;
	update sasuser.bottle
		set day5=4-day5;
quit;
	
proc sql;
	update sasuser.bottle
		set day5=model*2;
quit;	

proc sql;
	update sasuser.bottle
		set day3=
		case 
			when day3=1 then 2
			when day3=2 then 3
			when day3=3 then 0
		end;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day3=3 
			where day3=2;
	update sasuser.bottle
		set day3=0 
			where day3=3;
	update sasuser.bottle
		set day3=1
			where day3=0;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day2=
		case
			when day2=0 then 1
			when day3=1 then 2
		    else 3
		end;
quit;

proc sql;
	update sasuser.bottle
		set day2=
		case
			when day5=2 then 0
			when day3=1 then 2
		    else 3
		end;
quit;

proc sql;
	update sasuser.bottle
		set day3=
		case
			when day5=4 then 0
			when day2=2 then 2
		    else 3
		end;
	select *
		from sasuser.bottle;
quit;

proc sql;
	update sasuser.bottle
		set day3=
		case day2
			when 0 then 1
			when 2 then -1
			else 0
		end;
	select *
		from sasuser.bottle;
quit;


proc print data=sasuser.bottle;
run;


proc sql;
	select model,
		case model
			when 1 then "First"
			when 2 then "Second"
			when 3 then "Third"
		end
		from sasuser.bottle;
quit;

proc print data=one;
run;

data one;
	set sasuser.one;
run;

proc sql;
	delete from one;
	select * 
		from one;
quit;

proc sql;
	describe table one;
quit;

data two;
	set sasuser.two;
run;

proc print data=two;
run;

proc sql;
	alter table two
		add Bonus num format=comma10.2,
			level char(3);
	select *
		from two;
quit;

proc sql;
	alter table two
		add New num;
	select *
		from two;
quit;

proc sql;
	alter table two
		drop new,bonus,level;
	select *
		from two;
quit;

proc sql;
	alter table two
		modify x format=dollar11.2 label="new";
quit;

proc print data=two;
run;

proc sql;
	select *
		from two;
quit;

proc sql;
	alter table two
		add bonus num format=dollar11.2
		modify x format=date9.
		drop b;
	select *
		from two;
quit;

proc sql;
	drop table two;
quit;




















