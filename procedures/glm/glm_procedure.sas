
proc glm data=set1;
	class block factorA factorB factorC;
  	model y = block factorA|factorB|factorC/ solution;
  	random block;
  	output out=setr r=resid p=yhat;
  	means factorA / tukey;
  	means factorB / tukey;
 	means factorC / tukey;
  	format factorA number.;
  	format factorB length.;
  	format factorC current.;
run;

proc glm data=set1;
	class location bait;
 	model y = location bait / p clm;
  	output out=set3 residual=r predicted=yhat;
  	lsmeans bait / stderr pdiff;
  	means bait / tukey snk;
run;

proc glm data=set1;
	class variety nitrogen;
  	model yield = variety|nitrogen / p clm alpha=.05;
  	output out=setr r=resid p=yhat;
  	lsmeans variety*nitrogen / stderr pdiff;
  	means variety nitrogen / tukey;
  	contrast 'n-linear' nitrogen -1 0 1;
  	contrast 'n-quad' nitrogen -1 2 -1;
  	contrast 'v1-v2' variety 1 -1 0;
  	contrast '(v1+v2)-v3' variety .5 .5 -1;
  	contrast '(v1-v2)*(n-lin)' variety*nitrogen
                      -1 0 1 1 0 -1 0 0 0;
  	contrast '(v1-v2)*(n-quad)' variety*nitrogen
                      -1 2 -1 1 -2 1 0 0 0;
  	contrast '(.5(v1+v2)-v3)*(n-lin)' variety*nitrogen
                      -.5 0 .5 -.5 0 .5 1 0 -1;
  	contrast '(.5(v1+v2)-v3)*(n-quad)' variety*nitrogen
                      -.5 1 -.5 -.5 1 -.5 1 -2 1;
  	estimate 'n-linear' nitrogen -1 0 1;
  	estimate 'n-quad' nitrogen -1 2 -1;
  	estimate 'v1-v2' variety 1 -1 0;
  	estimate '(v1+v2)-v3' variety .5 .5 -1;
  	estimate '(v1-v2)*(n-lin)' variety*nitrogen
                      -1 0 1 1 0 -1 0 0 0;
  	estimate '(v1-v2)*(n-quad)' variety*nitrogen
                      -1 2 -1 1 -2 1 0 0 0;
  	estimate '(.5(v1+v2)-v3)*(n-lin)' variety*nitrogen
                      -.5 0 .5 -.5 0 .5 1 0 -1;
  	estimate '(.5(v1+v2)-v3)*(n-quad)' variety*nitrogen
                      -.5 1 -.5 -.5 1 -.5 1 -2 1;
run;

proc glm data=set1;
	class oil;
  	model y = oil / p;
  	estimate 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 / divisor=3;
  	estimate 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  	estimate 'o1-o3' oil 1 0 -1 0 ;
  	contrast 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 ;
  	contrast 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  	contrast 'o1-o3' oil 1 0 -1 0 ;
  	means oil /alpha=.05 bon lsd scheffe tukey snk;
  	output out=set2 residual=r predicted=yhat;
run;

proc glm;
   	model logbrain = logbody loglitter loggest;
   	contrast 'test1' loglitter 1;
   	contrast 'test2' loglitter 1, loggest 1;
   	estimate 'test3' loglitter 1 loggest 1;
	title 'Regression of log brain size on log body, log litter and log gest';
   	output out = resids r = resid p = yhat; 
run;

proc glm data=set1;
	class oil;
  	model y = oil / p;
  	estimate 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 / divisor=3;
  	estimate 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  	estimate 'o1-o3' oil 1 0 -1 0 ;
  	contrast 'o4-(o1+o2+o3)/3' oil -1 -1 -1 3 ;
  	contrast 'o2-(o1+o3)/2' oil -0.5 1 -0.5 0;
  	contrast 'o1-o3' oil 1 0 -1 0 ;
  	means oil /alpha=.05 bon lsd scheffe tukey snk;
  	output out=set2 residual=r predicted=yhat;
run;

proc glm data =set1;
  	class car driver additive;
  	model mpg = driver car additive / p;
  	random  car driver / q test;
run;

proc glm data=set1;
  	class batch sample;
  	model y = batch sample(batch);
  	random batch sample(batch) / q test;
run;


proc glm data=set2;
     class subject group task cue;
     model x = group subject(group) task 
            task*group task*subject(group)
            cue cue*group cue*subject(group) 
            task*cue group*task*cue;
    test h=group  e=subject(group);
    test h=task task*group  e=task*subject(group);
    test h=cue cue*group  e=cue*subject(group);
run;

proc glm data=set1;
     class group;
     model x1-x6 = group;
     manova h=group;                              /* part A */
     manova h=group m=x1+x2+x3-x4-x5-x6;          /* part E */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;   /* part F */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;   /* part G */
run;

proc glm data=set1;
     class group;
     model x1-x6 = group / noint;
     manova h=group;
     manova h=group m=x1+x2+x3-x4-x5-x6;           /* part B */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;    /* part C */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;    /* part D */
run;

PROC GLM DATA=set1;
     CLASS location variety;
     MODEL x1-x3 = location variety location*variety / P SOLUTION;
     MANOVA H=location*variety /PRINTH PRINTE;
     MANOVA H=variety / printH printE;
     MANOVA H=location / printH printE;
     Repeated traits 3 profile / printm;
run;

proc glm data=set1;
     model x1-x6 = z1 z2 / noint;                /* part H */
     manova h=z1 m=x1-x3-x4+x6, x2-x3-x5+x6;
run;

proc glm data=owls;
  	class type;
  	model km91 km118 km140 km160 km177 km241 km338 = type;
  	manova h=type;
run;

PROC GLM DATA=SET1;
     CLASS TEMP;
     MODEL X1 X2 = TEMP / P SOLUTION;
     MANOVA H=TEMP /PRINTH PRINTE;
     LSMEANS TEMP /PDIFF STDERR;
run;

PROC GLM DATA=set1;
     MODEL d1 d2 d3 = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
Run;


proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-East, North-South, 
            North-West  prefix=c /printe ;
run;

proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-South, East-West, 
            North+South-East-West  prefix=c /printe ;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

PROC GLM DATA=set1;
     MODEL dBOD dSS = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
	 random tree;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

proc glm data=set1;
     model North East South West = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

proc glm data=set1;
     model x1-x4 = z /noint nouni;
     manova H=z M=x1-x2+x3-x4, -x1-x2+x3+x4, 
          -x1+x2+x3-x4 prefix=c /printe ;
run;

PROC GLM DATA=set1;
     CLASS group;
     MODEL x1-x4 = group / P SOLUTION;
     MANOVA H=group /PRINTH PRINTE;
     Manova H=group M= (1 0 0 -1, 0 1 0 -1, 0 0 1 -1) prefix=diff / printh printe;
     Repeated test 4 profile / printm printh printe;
run;

proc glm data=set3;
     class treat dog;
     model x = dog treat / solution;
     random dog;
     means treat / bon tukey ;
run;

proc glm data=set1;
     model x1-x4 = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

proc glm;
  	where code ne 1;
    class code;
    model percent = code;
    title 'Without judge 1';
run;
proc glm;
  	class amount type;
  	model gain = amount type amount*type;
     /* could also write amount | type */
    lsmeans amount amount*type /stderr;
  	estimate 'beef-pork' type 1 0 -1;
  	estimate 'high-low' amount 1 -1;
  	estimate 'high-low in beef' amount 1 -1 amount*type 1 0 0 -1 0 0;
  	estimate 'beef-pork in low' type 1 0 -1 amount*type 0 0 0 1 0 -1;
  	estimate 'high-low in beef vs high-low in pork' amount*type 1 0 -1 -1 0 1;
  	contrast 'av of beef,pork - cereal' type 1 -2 1;
  	contrast 'beef-pork' type 1 0 -1;
  	title '2 way factorial ANOVA';
run;
proc glm;
  	class both;
  	model gain = both;
  	lsmeans both /stderr;
  	contrast 'amount' both 1 1 1 -1 -1 -1;
  	contrast 'type'  both 1 -1  0 1 -1  0,
                     both 1  0 -1 1  0 -1;
  	contrast 'amount*type' both 1 -1  0 -1 1 0,
                           both 1  0 -1 -1 0 1;
 	estimate 'high-low in beef' both 1 0 0 -1 0 0;
    title '1-way anova approach to factorial ANOVA';
run;
proc glm;
  	class amount type;
  	model gain = amount type amount*type;
  	lsmeans amount*type / slice = amount;
  	title 'Slicing by amount';
run;
proc glm;
  	by amount;
  	class type;
  	model gain = type;
  	title 'Separate analyses by amount'; 
run;
proc glm;
  	class code;
  	model percent = code;
/* to get confidence intervals for estimates and model param */
/*   add /clparm to model statement, e.g.: */
/*  model percent =  code /clparm;   */


/*  The following request more information: */
/*   lsmeans command provides  means, s.e., and 95% ci for each group */

  	lsmeans code /stderr cl;
 
/*  output residuals and plot diagnostics to do a residual plot */

  	output out=resids p = yhat r = resid;

/* estimate and contrast statements for a-priori questions: */

  	estimate 'spock - rest ' code 6 -1 -1 -1 -1 -1 -1 /divisor = 6;
  	estimate 'BAD: spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  
  	contrast 'spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  	contrast 'among rest'   code 0  1 -1  0  0  0  0,
                            code 0  0  1 -1  0  0  0,
                            code 0  0  0  1 -1  0  0,
                            code 0  0  0  0  1 -1  0,
                            code 0  0  0  0  0  1 -1;

  	contrast 'among rest, 2' code 0 5 -1 -1 -1 -1 -1,
                             code 0 0  4 -1 -1 -1 -1,
                             code 0 0  0  3 -1 -1 -1,
                             code 0 0  0  0  2 -1 -1,
                             code 0 0  0  0  0  1 -1;
                           
/* multiple comparisons adjustments for post-hoc comparisons */
/*   adding cl to this (or any subsequent gives you simul. conf. int */
/*   for differences.  conf. int. for individ. means are NOT adjusted */

   	lsmeans code /stderr pdiff adjust = tukey cl;      
     /* tukey's mcp */
  
/* the following multiple comparisons adjustments are commented out */
/*   each produces output similar to the above command, */
/*   however, the indicated multiple comparisons procedure will be used */

/*   lsmeans code /stderr pdiff adjust = t;      
     /* No m.c.p., i.e. Fisher's lsd */
 
/*   lsmeans code /stderr pdiff adjust = bonferroni;      
     /* bonferroni mcp */

/*   lsmeans code /stderr pdiff adjust = scheffe;       
     /* scheffe mcp */
run;

proc glm data = meat;
  	model ph = logtime;
  	estimate 'ph at 5 hr' intercept 1 logtime 1.6094;
  	estimate 'ph at 2 hr' intercept 1 logtime 0.6931;
  	title 'regression on observed data';
run;

proc glm data = all;
  	model ph=logtime;
  	output out=set1 
    residual=r               /* residuals */
    predicted=yhat           /* predicted values */
    stdp = sepred            /* s.e. of predicted mean (the line) */
    stdi = stobs             /* s.e. on predicted observation */
    lclm = lcipred           /* lower 95% c.i. limit for pred. mean */
    uclm = ucipred           /* upper 95% c.i. limit for pred. mean */
    lcl = lciobs             /* lower 95% c.i. limit for pred. obs */
    ucl = uciobs             /* upper 95% c.i. limit for pred. obs */
    ;                        /* and here, finally, is the end of the output */
  	title 'regression on all data';
run;
