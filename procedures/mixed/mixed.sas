
proc mixed data=set1;
  	class batch process;
  	model yield = batch process / solution residual outp=set2 outpm=set3;
  	random batch ;
  	lsmeans process / pdiff;
  	estimate 'A-B' process 1 -1 0 0 ;
  	estimate 'C-(A+B)/2' process -0.5 -0.5 1 0;
  	estimate 'D-(A+B+C)/3' process -1 -1 -1 3 / divisor=3;
  	contrast 'A-B' process 1 -1 0 0 ;
  	contrast 'C-(A+B)/2' process -0.5 -0.5 1 0;
  	contrast 'D-(A+B+C)/3' process -1 -1 -1 3;
run;


proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=cs subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=un subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=cs subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set2;
     class group test subject;
     model y = group test group*test / ddfm=kr;
     random subject / subject=subject(group);
     lsmeans group*test / diff ;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=un subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=vc subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed method = type3;
  	class method class tutor;
  	model perform = method tutor method*tutor /ddfm = satterth;
  	random class(method);
  	lsmeans method tutor method*tutor /diff;
  	title 'Split plot: teaching methods';
run;

proc mixed;
  	model y =  x /solution ddfm = kr;
  	repeated /subject = intercept type = ar(1);
  	title 'regression with ar(1) error ';
run;

proc mixed method = type3;
  	class farm bale;
  	model clean =  ;
  	random farm bale(farm);
  	title 'Tests of variance components';
run;

proc mixed method = type3;
  	class pair trt;
  	model y =  trt;   /* fixed effects in model statement */
  	random pair;      /* random effects in random statement */

  	lsmeans trt;      /* mixed gives the s.e. by default */
  	estimate 'Aff - Unaff' trt 1 -1 /cl;

  	title 'Block analysis, random blocks';
  
/* can add statements to get residuals, contrasts, or m.c.p. as needed */
  	title 'Blocked analysis of Schizophrenia data, random blocks';
run;

proc mixed data = wool;
  	where clean < 62; 
  	class bale;
  	model clean = ;
  	random bale;
  	title 'REML estimates from proc mixed, without outlier';
run;  
 
