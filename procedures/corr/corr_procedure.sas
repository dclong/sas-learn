
PROC CORR DATA=bnotes;
     VAR X1-X6; 
run;

PROC CORR DATA=set1;
     VAR X1-X4;
RUN;

proc corr data=scores;
     var prin1 prin2 prin3 x1-x10 age;
run;

proc corr data=set1;
     var dBOD dSS; 
run;

proc corr data=scores;
     var prin1 prin2 prin3 length width height;
run;

proc corr data=set1 Fisher(biasadj=no); 
	var x y; 
run;

proc corr data=set1;
	var biomass x1-x5; 
run;

proc corr fisher (biasadj=no);
  	var ph time logtime;
  	title 'CI for correlation coefficients';
run;

proc corresp data=france out=results cp rp dimens=3 profile=both short  ;
     var      varw free huma sche sala secu comp inte near atmo soci auto like
           othe none outd noa node grad smal medi larg;
     supplementary node grad smal medi larg;
     id  work;
run;
proc corr fisher (rho0 = 0.6 biasadj=no); 
  	var ph time logtime;
  	title 'Fisher test of rho = 0.6';
run;

proc corr spearman;
  	var ph time logtime;
  	title 'estimate and test spearman correlation';
run;

proc corr data=sasuser.bottle fisher (rho0 = 0.6 biasadj=no); 
  	var day1-day4;
  	title 'Fisher test of rho = 0.6';
run;

