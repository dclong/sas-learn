
data one ;
  	infile "C:\microwave.dat"; 
  	input radiation ;
run;

/* We sort the observations to create the sample quantiles*/

proc sort data = one ;
  	by radiation ;
run ;

/* We now compute the 42 probability levels p and then evaluate the inverse normal cdf 
for each of the 42 p values. I create a new SAS dataset which then will be merged with the
dataset containing the sorted observations. I could have continued working within dataset one,
but this is just to show you how to merge datasets. */

data two;
  	n = 42 ;
  	do i = 1 to n ;
    	p = (i - 0.5) / n ;
		q = probit(p) ;
		output ;
  	end ;
run;

/* We now merge datasets one and two. Since the order of the x and the q coincide, we just do a 
default merge, meaning that the first line in one is paired with the first line in two and so on. */

data all ;  	
	merge one two ;
run ;


/* Before we create the Q-Q plot, we define characteristics of the axes and of the symbols that will
be used to represent the points on the graph. Axes are numbered (in this case, just 1 and 2) and the
vertical and horizontal axes are assigned axis1 definition or axis2 definition within the plot statement.*/

axis1 order = (0 to 0.5 by 0.05) minor = none length = 70 pct
   		label = (f=swiss a=90 h=1.5 'Sample quantiles') ;

axis2 order = (-2.4 to 2.4 by 0.4) minor = none length = 70 pct
   		label = (f=swiss h=1.5 'Normal quantiles');

symbol1 v = dot c = green i = none h = 1 ;

proc gplot data = all ;
  plot radiation * q / vaxis = axis1 haxis = axis2 frame ;
  title 'Q-Q plot for microwave example' h=2 f=swiss ;
run ;
