
  /*  This program uses PROC IML in SAS to
      compute sample sizes for one-sample and
      paired t-tests.  For the latter, n is the
      number of pairs.

      It is posted as size1.sas */

proc iml;
	start size1;
	alpha = .05;   /* specify the type I error level */
	                /* specify possible power values */
	power = {.80 .85 .90 .95};       
	                    
	 /* Specify standard deviation (s),
	    difference to be detected (diff),
        and difference divided by thestandard deviation (d) */
     
     s = 37;
	 diff = 23;
	 d=diff/s;
	 print s diff d;
                        
     np=ncol(power);    /* Compute number of power values */
                        /* Compute the type II error level */
     beta = j(1,np,1) - power; 

	 do i=1 to np;

     n = ((probit(alpha/2) + probit(beta[1,i]))**2)/(d*d);

     n = int(n) + 1;   /* round up to next largest integer */
     v = n-1;          /* Compute degrees of freedom  */

     n = ((tinv(alpha/2,v) + tinv(beta[1,i],v))**2)/(d*d);

     n = int(n) + 1;   /* round up to next largest integer */
     pow=power[1,i];

     print,,,,,,, "One-sample (or paired) ttest:" ;
     print   "           Significance level:  "  alpha;
     print   "                        power:  "  pow;
     print   "            Scaled difference:  "  d;
     print   " Sample size (two-sided test):  "  n;


    /* Compute sample size for a one-sided t-test */

     n = ((probit(alpha) + probit(beta[1,i]))**2)/(d*d);
     n = int(n) + 1;

     v = n-1;

     n = ((tinv(alpha,v) + tinv(beta[1,i],v))**2)/(d*d);
     n = int(n) + 1;

     print   "  Sample size (one-sided test):  " n;

    end;

    finish;


run size1;





