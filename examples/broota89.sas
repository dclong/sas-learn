
/* This program does the computations 
   for problems 6 and 7 on assignment 3. 
   It is posted as  broota.sas */

/*  Enter the data  */
filename broota89 url "http://streaming.stat.iastate.edu/~stat501/data/broota89.dat";

data set1;
     infile broota89;
     input subject group x1-x6;
     z1=1;
     z2=1; 
     if(group=1) then z2=-1;
run;

proc print data=set1;
run;

/*  Computations for problem 3  */

data set2; 
     set set1;
     array a(i) x1-x6;
     do over a;
        x=a; 
        task=int((i-1)/3)+1; 
        cue=i; 
        if(i>3) then cue=i-3; 
        output;
     end;
     keep subject group task cue x ;  
run;

proc print data=set2;
run;

data _NULL_; 
      set set2;
      file "I:\Study\Spring 2009\STAT 501\Homework\HW3\broota892.dat";
      put (subject group task cue x) (4.0 4.0 4.0 4.0 6.0);
run;

proc glm data=set2;
     class subject group task cue;
     model x = group subject(group) task 
            task*group task*subject(group)
            cue cue*group cue*subject(group) 
            task*cue group*task*cue;
    test h=group  e=subject(group);
    test h=task task*group  e=task*subject(group);
    test h=cue cue*group  e=cue*subject(group);
run;

/*  Test hypotheses in problem 4 on assignment 4 */

proc glm data=set1;
     class group;
     model x1-x6 = group;
     manova h=group;                              /* part A */
     manova h=group m=x1+x2+x3-x4-x5-x6;          /* part E */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;   /* part F */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;   /* part G */
run;

proc glm data=set1;
     class group;
     model x1-x6 = group / noint;
     manova h=group;
     manova h=group m=x1+x2+x3-x4-x5-x6;           /* part B */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;    /* part C */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;    /* part D */
run;

proc glm data=set1;
     model x1-x6 = z1 z2 / noint;                /* part H */
     manova h=z1 m=x1-x3-x4+x6, x2-x3-x5+x6;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=cs subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=un subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=vc subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;


