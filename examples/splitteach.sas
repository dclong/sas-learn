
data teach;
  	infile 'class.txt';
  	input method $ class tutor $ perform;
run;
 
proc mixed method = type3;
  	class method class tutor;
  	model perform = method tutor method*tutor /ddfm = satterth;
  	random class(method);
  	lsmeans method tutor method*tutor /diff;
  	title 'Split plot: teaching methods';
run;
