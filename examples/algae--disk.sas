
/*  This program is posted in the SAS folder of the
    course web page as algae.sas, and the data are
    posted as algae.dat in the data folder of the
    course web page */

/*  Enter the data and created variables that are
    Squares and the cross product of x1 and x2  */
filename data url 'http://www.stat.iastate.edu/resources/statisticsData/algae.txt';

data set1;
     infile data;
     input y x1 x2;
     x11=x1*x1;
     x22=x2*x2;
     x12=x1*x2;
	 x111=x11*x1;
	 x222=x22*x2;
	 x112=x11*x2;
	 x221=x22*x1;
     label y = units of algae
           x1 = copper level (mg)
           x2 = time (days)
		   x12="adfa";
run;

/*  print the data  */
proc print data=set1;
run;

/*  Fit the model in part A of problem 1 */
proc reg data=set1;
     model y = x1 x2 x12 / p ss1 ss2 clm cli partial
                        vif collin influence;
     output out=set2 r=residual p=yhat;
run;

/*  Compute the pure error and lack of fit sums of squares
    for the previous model */
data set1; 
     set set1;
     ctime = 100*x1+x2; 
run;

proc glm data=set1;
     class ctime;
     model y = x1 x2 x12 ctime/ ss1 ss2 p;
run;

proc glm data=set1;
     model y = x1 x2 x12 ctime;
run;
/*  Construct residual plots for the model in part A */

proc rank data=set2 normal=blom out=set2;
      var residual; 
      ranks q;
run;

proc univariate data=set2 normal;
     var residual;
run;

proc univariate data=set2;
     var residual;
	 qqplot;
run;

proc univariate data=set2;
     probplot residual/normal(mu=0 sigma=1 color=red);
run;

data set2; 
     set set2;
     zero=0; 
run;

goptions rotate=landscape targetdevice=ps;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to .4 by .1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2  by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

      SYMBOL1 V=CIRCLE H=3.0 ;
      SYMBOL2 V=none H=3.0 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT (residual)*yhat /  vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL yhat='Predicted values';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT residual*q / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Normal probability plot";
      LABEL q='q';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to 5 by 1
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by .1
      value=(f=swiss h=3.0)
      length = 60 pct;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

PROC GPLOT DATA=SET2;
      PLOT residual*x1 / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL x1='Amount of Copper (mg)';
      LABEL residual = 'Residuals';
RUN;

axis1 label=(f=swiss h=3.5)
      ORDER = 0 to 25 by 5
      value=(f=swiss h=3.0)
      length= 70 pct;

axis2 label=(f=swiss h=3.5 a=90)
      order = -.2 to .2 by  .1
      value=(f=swiss h=3.0)
      length = 60 pct;

      SYMBOL1 V=CIRCLE H=2.5 ;
      SYMBOL2 V=none H=2.5 l=1 i=spline ;

PROC GPLOT DATA=set2;
      PLOT residual*x2 / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Algae Growth Analysis";
      title2  h=3.5 f=swiss "Residual Plot";
      LABEL x2='Time (days)';
      LABEL residual = 'Residuals';
RUN;

/*  Fit a bunch of models  */
proc reg data=set1;
     model y = x1 x2 x12 x11 x22 x112 x221  x111 x222 / 
         selection=rsquare cp aic sbc mse b adjrsq;
run;

