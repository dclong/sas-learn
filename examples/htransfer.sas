
/*  Enter the heat transfer data for
    problem 2 on assignment 10  */

data set1;
  	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data/htransfer.dat';
  	input y1 y2 x1 x2 x3 x4;
    x11=x1*x1; 
	x33=x3*x3;
	x12=x1*x2;
	x13=x1*x3;
	x14=x1*x4;
    x22=x2*x2;
	x44=x4*x4;
	x23=x2*x3;
	x24=x2*x4;
	x34=x3*x4;
  	label y1 = heat transfer coefficient
          y2 = thermal efficiency
          x1 = fluidizing gas flow rate (lb/hr)
          x2 = supernatant gas flow rate (lb/hr)
          x3 = supernatant gas inlet nozzle (mm)
          x4 = supernatant gas inlet temperature (F);
run;


/*  print the data  */

proc print data=set1;
run;

/*

proc insight data=set1;
 scatter y1 y2 x1 x2 x3 x4* y1 y2 x1 x2 x3 x4;
run;

*/

/*  Fit a bunch of models  */

proc reg data=set1;
  	model y1 y2 = x1 x2 x3 x4 x11 x22 x33 x44 x12 x13 x14 x23 x24 x34 /
      		selection=rsquare  adjrsq mse cp aic sbc best=5;
run;


/* fit heat transfer model identified in part c */

proc reg data=set1;
  	model y1 = x3 x12 x22 x33 / partial influence all vif  collin;
  	output out=set2 r=residual p=yhat;
run;



/*  Construct residual plots for the model identified
    in part c */

proc rank data=set2 normal=blom out=set2;
  	var residual; 
	ranks q;
run;

proc univariate data=set2 normal plot;
  	var residual;
run;

data set2; 
	set set2;
  	zero=0; 
run;

 goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 250 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -50 to 50 by 10
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Heat Transfer Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;



/* fit best 4 variable thermal efficiency model identified in part c */

proc reg data=set1;
  	model y2 = x11 x22 x14 x24 / partial influence all vif  collin;
  				output out=set3 r=residual p=yhat;
run;



/*  Construct residual plots for the model identified
    in part c */

proc rank data=set3 normal=blom out=set3;
  	var residual; 
	ranks q;
run;

proc univariate data=set3 normal plot;
  	var residual;
run;

data set3; 
	set set3;
  	zero=0;
run;

 goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER =35 to 75 by 10
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency  Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;






/* fit best 5 variable thermal efficiency model identified in part c */

proc reg data=set1;
  	model y2 = x4 x11 x44 x14 x22 / partial influence all vif  collin;
  	output out=set3 r=residual p=yhat;
run;



/*  Construct residual plots for the model identified
    in part c */

proc rank data=set3 normal=blom out=set3;
  	var residual; 
	ranks q;
run;

proc univariate data=set3 normal plot;
  	var residual;
run;

data set3; 
	set set3;
  	zero=0; 
run;

 goptions  targetdevice=ps300 rotate=landscape;

axis1 label=(f=swiss h=3.0)
      ORDER =35 to 75 by 10
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL yhat='Predicted values';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = -3 to 3 by 1
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Residuals";
title2  h=4.0 f=swiss "Normal probability plot";
       LABEL q='q';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x1 zero*x1 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x1='Fluidizing Gas Flow Rate (lb/hour)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 60 to 240 by 30
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x2 zero*x2 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency  Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas flow rate (lb/hr)';
       LABEL residual = 'Residuals';
   RUN;


axis1 label=(f=swiss h=3.0)
      ORDER = 0 to 80 by 20
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3.0  a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=3.0 ;
       SYMBOL2 V=none H=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SET3;
      PLOT residual*x3 zero*x3 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x3='Supernatant Gas Inlet Nozzle Opening (mm)';
       LABEL residual = 'Residuals';
   RUN;

axis1 label=(f=swiss h=3.0)
      ORDER = 150 to 300 by 50
      value=(f=swiss h=3.0)
      length= 6.5 in;

axis2 label=(f=swiss h=3 a=90)
      order = -8 to 12 by 4
      value=(f=swiss h=3.0)
      length = 4.5 in;

       SYMBOL1 V=CIRCLE H=2.5 ;
       SYMBOL2 V=none H=2.5 l=1 i=spline ;

   PROC GPLOT DATA=SET2;
      PLOT residual*x4 zero*x4 / overlay vaxis=axis2 haxis=axis1;
TITLE1  H=4.0 F=swiss "Thermal Efficiency Model";
title2  h=4.0 f=swiss "Residual Plot";
       LABEL x2='Supernatant gas inlet temperature (F)';
       LABEL residual = 'Residuals';
   RUN;


