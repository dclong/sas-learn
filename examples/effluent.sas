

/* This program is posted as  
           effluent.sas
  on the course web page. It computes a
  one-sample Hotelling T-squared test. */

DATA set1;
     INPUT sample BOD1 SS1 BOD2 SS2;
     dBOD=BOD1-BOD2;
     dSS = SS1-SS2;
     Z=1;
  /* LABEL BOD1 = BOD (private lab)
        SS1 = SS (private lab)
        BOD2 = BOD (state lab)
        SS2 = SS (state lab);  */
	datalines;
 1  6  27 25 15
 2  6  23 28 13
 3 18  64 36 22
 4  8  44 35 29
 5 11  30 15 31
 6 34  75 44 64 
 7 28  26 42 30
 8 71 124 54 64
 9 43  54 34 56
10 33  30 29 20
11 20  14 39 21
run;

PROC PRINT DATA=set1;
run;

proc corr data=set1;
     var dBOD dSS; 
run;


/* Create high quality graphs with SASGRAPH */

goptions targetdevice=ps300 rotate=portrait;

axis1 label=(f=swiss h=2.5  
      "Differences in BOD Measurements")
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Differences in SS Measurements ")
      value=(f=triplex h=2.0)
      length = 5.0in;

SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT dBOD*dSS / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "Differences in Effluent Measurements";
     footnote ls=1.0in;
RUN;

Proc Univariate data=set1 normal;
     var dBOD dSS; 
run;

PROC GLM DATA=set1;
     MODEL dBOD dSS = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
run;
