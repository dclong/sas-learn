*****************Chuanlong Du***********************************;
/*
Functions:
This program calculate the generalize variance and total variance of a matrix
Parameters:
Parameter X is the Covariance Matrix
*/
proc iml;
*****************Body of Function*******************************;
     start MatInf(X);
	 GV=det(X);*Generalized Variance;
	 TV=trace(X);*Total Variance;
	 finish;
*****************Initial Parameters*****************************;

*****************Run the Function*******************************;

quit;
