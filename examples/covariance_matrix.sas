******************Chuanlong Du**********************************;
/*
Functions:
This program is used to calculate the covariance matrix
Parameters:
Parameter X is the observation matrix
*/
proc iml;
****************Body of Function*********************************;
     start Cov(X);
     SumVector=X[+,];
	 N=nrow(X);
	 P=ncol(X);
	 MeanVector=SumVector/N;
	 Xcent=X-shape(MeanVector,N,P);
	 A=Xcent`*Xcent;
	 CovMat=A/(N-1);
	 title f=arial c=red h=6 "Covariance Matirx";
	 print CovMat;
	 finish;
*************Initial Parameters**********************************;

*************Run Functions***************************************;
	 
quit;
