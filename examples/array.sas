
data dx;
	array a{3} x y z (1:3);
run;

proc print data=dx;
run;

data dx2;
	array a{3}  (1-3);
run;

proc print data=dx2;
run;
