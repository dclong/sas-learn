
/*  This program performs a one-sample 
    Hotelling T-squared test for equality 
    of means for repeated measures.  The
    program is posted as  dogs.sas

    This program analyzes the dog data
    posted in the file    dogs.dat    */
filename dog url "http://streaming.stat.iastate.edu/~stat501/data/dogs.dat";

data set1;
   	infile dog;
    input  dog x1-x4;
    Z = 1;
  /*
  LABEL X1 = High CO2 without Halothane
        X2 = Low CO2 without Halothane
        X3 = High CO2 with Halothane
        X4 = Low CO2 with Halothane;
  */
run;

proc print data=set1; 
run;

title 'Time between heartbeats (msec)';


/* Test the null hypothesis that 
   all treatments have the same mean 
   time between heartbeats  */

proc glm data=set1;
     model x1-x4 = z /noint nouni;
     manova H=z M=x1-x2+x3-x4, -x1-x2+x3+x4, 
          -x1+x2+x3-x4 prefix=c /printe ;
run;

/* Do a mixed model analysis of variance.
   Restructure the data file to put each
   observation on a different line  */

data set3; 
     set set1;
     array A(I) x1-x4;
     do over A;
        x = A;
        treat = I;
        output;
     end;
     keep dog treat x;
run;

proc print data=set3;
run;

proc glm data=set3;
     class treat dog;
     model x = dog treat / solution;
     random dog;
     means treat / bon tukey ;
run;

/* Use the REPEATED option in PROC GLM 
   to do both analyses */

proc glm data=set1;
     model x1-x4 = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

/* Use the MIXED procedure in SAS */

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=cs subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=un subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;
