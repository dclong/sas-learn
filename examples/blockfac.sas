

/*  Analysis of a 4x4x3 factorial performed
    in two replicates (blocks) 
    (Solant, Delury and Hunter, Arch. Neur. 
     and Psych., pp 802-807, 1943).

     Examine the effects of electical stimulation
     on preventing wasting away in denervated
     gastrocnemius-soleos muscles (removal of
     a small part of the sciatic nerve) in rats.

     Factor A: Number of daily treatment periods
          (j=1)  1
          (j=2)  3
          (j=3)  6

     Factor B: Length of each treatment (minutes)
          (k=1)  1
          (k=2)  2
          (k=3)  3 
          (k=4)  5

     Factor C: Type of current
          (r=1)  Galvanic
          (r=2)  Faradic
          (r=3)  60 cycle alternating
          (r=4)  25 cycle alternating

     Blocks:  Two replicates
          (i=1)  Replicate 1
          (i=2)  Replicate 2 

     Y = muscle weight at end of 11 days
     X = weight of normal muscle
*/


data set1;
  input block factorA  factorB factorC  Y X;
  datalines;
  1 1 1 1 72 152
  1 1 1 2 61 130
  1 1 1 3 62 141
  1 1 1 4 85 147
  1 1 2 1 67 136
  1 1 2 2 60 111
  1 1 2 3 64 126
  1 1 2 4 67 123
  1 1 3 1 57 120
  1 1 3 2 72 165
  1 1 3 3 63 112
  1 1 3 4 56 125
  1 1 4 1 57 121
  1 1 4 2 60  87
  1 1 4 3 61  93
  1 1 4 4 73 108
  1 2 1 1 74 131
  1 2 1 2 61 129
  1 2 1 3 65 112
  1 2 1 4 76 125
  1 2 2 1 52 110
  1 2 2 2 55 180
  1 2 2 3 65 190
  1 2 2 4 72 117
  1 2 3 1 66 132
  1 2 3 2 43  95
  1 2 3 3 66 130
  1 2 3 4 75 130
  1 2 4 1 56 160
  1 2 4 2 63 115
  1 2 4 3 79 125
  1 2 4 4 86 140
  1 3 1 1 69 131
  1 3 1 2 65 126
  1 3 1 3 70 111
  1 3 1 4 61 130
  1 3 2 1 62 122
  1 3 2 2 59 122
  1 3 2 3 64  98
  1 3 2 4 60  92
  1 3 3 1 72 129
  1 3 3 2 43  97
  1 3 3 3 72 180
  1 3 3 4 92 162
  1 3 4 1 78 135
  1 3 4 2 58 118
  1 3 4 3 68 160
  1 3 4 4 71 120
  2 1 1 1 46  97
  2 1 1 2 60 126
  2 1 1 3 71 129
  2 1 1 4 53 108
  2 1 2 1 44  83
  2 1 2 2 57 104
  2 1 2 3 62 114
  2 1 2 4 60 105
  2 1 3 1 53 101
  2 1 3 2 56 120
  2 1 3 3 56 101
  2 1 3 4 56  97
  2 1 4 1 46 107
  2 1 4 2 56 109
  2 1 4 3 64 114
  2 1 4 4 59 102
  2 2 1 1 74 131
  2 2 1 2 64 124
  2 2 1 3 64 117
  2 2 1 4 65 108
  2 2 2 1 58 117
  2 2 2 2 55 112
  2 2 2 3 61 100
  2 2 2 4 78 112
  2 2 3 1 50 103
  2 2 3 2 57 110
  2 2 3 3 56 109
  2 2 3 4 58  87
  2 2 4 1 55 108
  2 2 4 2 55 104
  2 2 4 3 66 101
  2 2 4 4 58  98
  2 3 1 1 58  81
  2 3 1 2 52 102
  2 3 1 3 71 108
  2 3 1 4 66 108
  2 3 2 1 54  97
  2 3 2 2 51 100
  2 3 2 3 79 115
  2 3 2 4 82 102
  2 3 3 1 61 115
  2 3 3 2 56 105
  2 3 3 3 71 105
  2 3 3 4 69 107
  2 3 4 1 64 115
  2 3 4 2 57 103
  2 3 4 3 62  99
  2 3 4 4 88 135
run;


proc format; 
	value number 1='1'
                 2='3'
			     3='6';
run;

proc format; 
	value current  1='galvanic'
                   2='faradic'
			       3='60 cycle AC'
				   4='25 cycle Ac';
run;

proc format; 
	value length  1='1 min'
                  2='2 min'
			      3='3 min'
			      4='5 min';
run;


proc glm data=set1;
	class block factorA factorB factorC;
  	model y = block factorA|factorB|factorC/ solution;
  	random block;
  	output out=setr r=resid p=yhat;
  	means factorA / tukey;
  	means factorB / tukey;
 	means factorC / tukey;
  	format factorA number.;
  	format factorB length.;
  	format factorC current.;
run;

proc rank data=setr normal=blom out=setr;
	var resid; 
	ranks q;
run;

goptions targetdevice=ps rotate=portrait;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Mean Muscle Weight')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=setr;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=setr;
  plot resid*yhat / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;
