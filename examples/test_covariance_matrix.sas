*************************Chuanlong Du************************************;
/*
Functions:
This function is used to test the structure of the covariance matrix
The null hypothesis is H0: Sigma=sigma^2*I
Parameters:
Parameter S is the covariance matrix
*/
proc iml;
****************Parameters Part*******************************************;
     
****************Body of Function******************************************;
     p=ncol(S);
     GSV=det(S);*generalize sample variance;
	 TraceS=trace(S);*trace of the sample variance;
     Chistat=-n*log(GSV)-n*p*log(p)+n*p*log(TraceS);
	 df=p*(p+1)/2-1;*the degrees of freedom;
	 Pvalue=1-probchi(Chistat,df);
	 title f=arial c=red h=6 "Test of the structure of the sample covariance";
	 print ChiStat df Pvalue;
***************Chisquare test using Bartlett Correction*******************;
	 print "Bartlett Correction";
	 ChiStatCorrected=(1-(2*p**2+p+2)/(6*p*n))*Chistat;
	 Pvalue=1-probchi(ChiStatCorrected,p*(p+1)/2-1);
	 print ChiStatCorrected,df,Pvalue;
quit;
