

/*  This is a program for analyzing the 
    penicillin data from Box, Hunter, 
    and Hunter.  It is posteded in the 
    file
              penicillin.sas          */

/*  First enter the data  */


data set1;
  	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data\penicillin.dat';
  	input batch process $ yield;
run;

/* Construct a dot plot of the process yields  */

goptions colors=(black)targetdevice=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Yield')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Production Process')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=dot i=none h=3 w=3 c=black;

proc gplot data=set2;
  plot yield*process / vaxis=axis1 haxis=axis2;
  title h=4.0 ls=0.5in f=swiss 
        c=black 'Dot Plot of Penicillin Process Yields';
  footnote ls=0.6in '  ';
  run;



/* Compute the ANOVA table, formulas for
   expectations of mean squares, process
   means and their standard errors */

  
proc mixed data=set1;
  	class batch process;
  	model yield = batch process / solution residual outp=set2 outpm=set3;
  	random batch ;
  	lsmeans process / pdiff;
  	estimate 'A-B' process 1 -1 0 0 ;
  	estimate 'C-(A+B)/2' process -0.5 -0.5 1 0;
  	estimate 'D-(A+B+C)/3' process -1 -1 -1 3 / divisor=3;
  	contrast 'A-B' process 1 -1 0 0 ;
  	contrast 'C-(A+B)/2' process -0.5 -0.5 1 0;
  	contrast 'D-(A+B+C)/3' process -1 -1 -1 3;
run;

proc print data=set2; run;

proc print data=set3; run;


/* Compute a normal probability plot for 
   the studentized residuals and the 
   Shapiro-Wilk test for normality  */

proc rank data=set2 normal=blom out=set2;
  var StudentResid;  ranks q;
  run;

proc univariate data=set2 normal plot;
  var StudentResid;
  run;

goptions colors=(black)targetdevice=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss 'Residuals')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis2 label=(h=2.3 f=swiss 'Standard Normal Quantiles')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  axis3 label=(h=2.3 f=swiss 'Production Process')
        value=(f=swiss h=2.0) w=3.0 length=5.0 in;

  symbol1 v=circle i=none h=2 w=3 c=black;

proc gplot data=set2;
  plot resid*q / vaxis=axis1 haxis=axis2;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Normal Probability Plot';
  footnote ls=0.6in '  ';
  run;

proc gplot data=set2;
  plot resid*process / vaxis=axis1 haxis=axis3;
  title h=3.0 ls=1.0in f=swiss 
        c=black 'Residual Plot';
  footnote ls=0.6in '   ';
  run;

/* Compute the Friedman Statistic */



proc freq data=set1;
  	tables batch*process*yield / cmh2 scores=rank noprint;
run;

