
/*  This program performs k-means cluster analysis of the Stanford
    diabetes data.  This code is posted as kmeans.sas */

options nosource nodate linesize=72;

DATA SET1;
  	INFILE 'c:/st501/data/diabetes.dat';
  	INPUT patient x1-x5;
/*  LABEL  x1 = 'relative weight'
           x2 = 'fasting plasma glucose'
           x3 = 'area under the plasma glucose curve (OGTT)'
           x4 = 'area under plasma insulin curve (OGTT)'
           x5 = 'steady state plasma glucose (SSPG)';           */
run;


/* First standardize each variable to have mean zero and
   unit standard deviation                               */

proc standard data=set1 out=set2 mean=0 std=1 ;
   	var x1-x5;
run;


/*  Use  FASTCLUS  to do K-means cluster analysis  */

PROC FASTCLUS DATA=SET2 OUT=SET3 MAXC=9 MAXITER=30 CONVERGE=.02 LIST;
   	VAR X1-X5;
   	ID patient;
   	TITLE "K-means Cluster ANALYSIS: Stanford Diabetes Data";
   	TITLE2;
RUN;

PROC CANDISC DATA=SET3 SIMPLE OUT=CAN NCAN=5 PREFIX=CAN;
   	CLASS CLUSTER;
   	VAR x1-x5;
RUN;

PROC PLOT;
   	PLOT CAN2*CAN1=CLUSTER/HPOS=56;
RUN;
