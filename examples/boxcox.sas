/* data handling needed for box-cox transformation */
/*  assumes 1-way layout */

options formdlim = '-';

data judges;
  	infile 'judges.txt';
  	input percent code;
run;

proc sort;
  	by code;
run;
  
/* compute mean and sd for each group */
/*  and store in a new data set */
proc means noprint;
  	by code;
  	var percent;
  	output out = means mean = mean std = sd;
run;
     
data boxcox;
  	set means;
  	logmean = log(mean);
  	logsd =  log(sd);
run;
       
proc plot;
  	plot logsd*logmean;
  	title 'Box-Cox regression plot';
run;
          

     

