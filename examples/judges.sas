options formdlim = '-';

data judges;
  	infile 'judges.txt';
  	input percent code;
run;
/* calculate ANOVA table  */

proc glm;
  	class code;
  	model percent = code;
/* to get confidence intervals for estimates and model param */
/*   add /clparm to model statement, e.g.: */
/*  model percent =  code /clparm;   */


/*  The following request more information: */
/*   lsmeans command provides  means, s.e., and 95% ci for each group */

  	lsmeans code /stderr cl;
 
/*  output residuals and plot diagnostics to do a residual plot */

  	output out=resids p = yhat r = resid;

/* estimate and contrast statements for a-priori questions: */

  	estimate 'spock - rest ' code 6 -1 -1 -1 -1 -1 -1 /divisor = 6;
  	estimate 'BAD: spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  
  	contrast 'spock - rest' code 6 -1 -1 -1 -1 -1 -1;
  	contrast 'among rest'   code 0  1 -1  0  0  0  0,
                            code 0  0  1 -1  0  0  0,
                            code 0  0  0  1 -1  0  0,
                            code 0  0  0  0  1 -1  0,
                            code 0  0  0  0  0  1 -1;

  	contrast 'among rest, 2' code 0 5 -1 -1 -1 -1 -1,
                             code 0 0  4 -1 -1 -1 -1,
                             code 0 0  0  3 -1 -1 -1,
                             code 0 0  0  0  2 -1 -1,
                             code 0 0  0  0  0  1 -1;
                           
/* multiple comparisons adjustments for post-hoc comparisons */
/*   adding cl to this (or any subsequent gives you simul. conf. int */
/*   for differences.  conf. int. for individ. means are NOT adjusted */

   	lsmeans code /stderr pdiff adjust = tukey cl;      
     /* tukey's mcp */
  
/* the following multiple comparisons adjustments are commented out */
/*   each produces output similar to the above command, */
/*   however, the indicated multiple comparisons procedure will be used */

/*   lsmeans code /stderr pdiff adjust = t;      
     /* No m.c.p., i.e. Fisher's lsd */
 
/*   lsmeans code /stderr pdiff adjust = bonferroni;      
     /* bonferroni mcp */

/*   lsmeans code /stderr pdiff adjust = scheffe;       
     /* scheffe mcp */
run;

proc plot;
  	plot resid*yhat;
  	title 'Predicted vs residual plot';
run;

proc glm;
  	where code ne 1;
    class code;
    model percent = code;
    title 'Without judge 1';
run;
       
