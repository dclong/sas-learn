
data set1;
  	infile 'c:\st501\data\roos2.dat';
  	input id $ sex species x1-x18;
  	type = 10*sex+species;
run;

PROC FORMAT;
  	VALUE TROO 10='Male Mg'
               11='Male Mm'
			   12='Male Mf'
			   20='Female Mg'
			   21='Female Mm'
               22='Female Mf'; 
RUN;

data oldroos; 
	set set1;
   if(id = .);
run;

data roos; 
	set set1;
    if(id= '.') then delete;
run;

proc print data=oldroos;  
run;

proc print data=roos;
run;

/*  Fit a linear discriminant model and classify historical
    cases that are not in the training samples.
    First save information about the classification rule to
    a file in a specified directory specified by a libname
    statement */

libname outdat "c:\stat501\sas\";

PROC DISCRIM DATA=roos POOL =Yes OUTSTAT=outdat.roos1;
  	CLASS type;
  	VAR x1 x3 x7 x9 x14 x15;
  	format type troo.;
run;

/* Access and print the saved file */

PROC PRINT data=outdat.roos1;
run;


/* Use this stored rule to classify the historical cases. */


proc discrim data=outdat.roos1 testdata=oldroos testlist;
  	class type;
  	var x1 x3 x7 x9 x14 x15;
  	testclass type;
  	FORMAT TYPE troo.;
run;

