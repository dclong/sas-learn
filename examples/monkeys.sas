
/*  This program computes a paired t test for the
    rhesus monkey experiment considered in class */

/*  First enter the data and compute the differences */

data set1;
  	input monkey y1 y2;
  	d = y1-y2;
  	label  y1 = CP level for severed nerve
           y2 = CP level for intact nerve
           d = difference (y1-y2);
	cards;
 1 11.5 16.3
 2  3.6  4.8
 3 12.5 10.9
 4  6.3 14.2
 5 15.2 16.3
 6  8.1  9.9
 7 16.6 29.2
 8 13.2 22.4
run;


/* Print the data file */

proc print data=set1;
run;


/* Compute the t test and summary statistics for the differences */

proc univariate data=set1 normal plot;
  	var d;
run;


/* Make a better normal probability plot for the differences  */

proc rank data=set1 normal=blom out=set1;
  	var d;
  	ranks q;
run;

proc plot data=set1;
  	plot d*q='*' /hpos=60;
run;

proc corr data=set1;
  	var y1 y2;
run;

/* Plot y1 against y2 */

proc plot data=set1;
  	plot y1*y2 = '*' / hpos=60;
run;


 axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.1)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.1)
      length = 5 in;

  symbol v=dot h=2 i=none w=2;

proc gplot data=means;
      plot logs*logmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Log(s) versus Log(mean)';
      label  logs = 'Log(s)';
      label  logmean = 'Log(mean)';
   run;
