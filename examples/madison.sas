
/*  This program is posted as  madison.sas
    It illustrates how to apply proncipal component 
    analysis to a covariance or correlation matrix
    when the covariance matrix is entered instead of 
    the raw data.  The variables are:

        X1  =  total population(thousands)
        X2  =  professional degrees (percent)
        X3  =  employed age over 16 (percent)
        X4  =  government employment(percent)
        X5  =  median home value ($100,000) */

options nonumber nodate nocenter linesize=64;

/*  Enter the data as a TYPE=COV data set */

DATA madison(TYPE=COV);
   	INPUT _TYPE_ $ _NAME_ $ x1-x5;
    datalines;
  	cov X1  3.397 -1.102   4.306  -2.078  0.027
  	cov X2 -1.102  9.673  -1.513  10.953  1.203
  	cov X3  4.306 -1.513  55.626 -28.937 -0.044
  	cov X4 -2.078 10.953 -28.937  89.067  0.957
  	cov X5  0.027  1.203  -0.044   0.957  0.319
run;
 
PROC PRINT data=madison;
     title "Principal Component Analyis";
     title2 "Madison: Five Socioeconomic Variables";
run;


/*  Compute principal components from
    the covariance matrix*/

PROC factor DATA=madison(TYPE=CORR) SCREE SCORE METHOD=PRIN N=5 COV EV REORDER;
run;

/*  Compute principal components from
    the correlation matrix*/

PROC factor DATA=madison(TYPE=CORR) SCREE SCORE METHOD=PRIN N=5 EV REORDER;
run;

