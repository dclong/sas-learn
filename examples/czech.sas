/*  This program is posted as  czech.sas
    It performs factor analyses for the 
    data are posted as  czech.dat     */
filename czech url "http://streaming.stat.iastate.edu/~stat501/data/czech.dat";

data set1;
     infile czech;
     input hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
run;

proc print data=set1;
     title f="Arial" c=red h=4 "Data of husbands and wives";
run;

/* Compute principal components and 
   do a varimax rotation */

proc factor data=set1 method=ml scree  nfactors=2 mineigen=0 simple 
             ev  msa out=scorepc  outstat=facpc;
     var hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
     priors smc;
	 title f="Arial" c=red h=4 "Factor analysis using ML method";
run;

proc factor data=set1 method=prinit n=2 rotate=varimax;
     var hw1-hw9 wh1-wh9 hh1-hh9 ww1-ww9;
     priors smc;
	 title f="Arial" c=red h=4 "Factor analysis using prinit method";
run;

/* Use information on the principal 
   components computed by PROC FACTOR 
   to rotate the first 8 components */

proc factor data=facpc n=8 rotate=varimax round reorder;
run;

