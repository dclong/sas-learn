
data schiz;
  	infile 'schiz.txt';
  	input pair unaff aff;
  	diff =  unaff - aff;
run;
/* analysis as paired data is a test of mean difference =  0 */

proc means mean std stderr t prt clm;
  	var diff;
  	title 'Paired data: t-test and 95% confidence interval';
run;
                     
proc univariate;
  	var diff;
  	title 'Paired t-test and nonparametric tests using proc univariate';
run;
        
/* analysis treating pairs as blocks */
/* this requires a different format for the data */
/*    now, each treatment is a separate observation */
/*    the following data step gets the schiz data into that form */
/*    this is unneccessary if the data already that way */

data schiz2;
  	set schiz;
	trt =  'Unaff';
  	y = unaff;
  	output;
	trt = 'Aff';
  	y =  aff;
  	output;
  	keep pair trt y;
run;

/* print out the data set so you see the reorganization */
proc print;
  	title 'Reshaped Schizophrenia data, for paired analysis ';
run;
/* pairs =  blocks treated as fixed effects is done in GLM */              

proc ttest;
  	class trt;
  	var y;
  	title 'Unpaired t-test';
run;

proc glm;
  	class trt;
  	model y = trt;
  	lsmeans trt /stderr;
  	title 'Unpaired t-test using pooled Sp';
run;
 
proc glm;
  	class pair trt;
  	model y = pair trt /clparm;
	lsmeans trt /stderr;
  	estimate 'Aff - Unaff' trt 1 -1;
	title 'Block analysis, fixed blocks';  
/* can add statements to get residuals, contrasts, or m.c.p. as needed */
  	title 'Blocked analysis of Schizophrenia data, fixed blocks';
run;
     
/* pairs =  blocks treated as random effects in MIXED */

proc mixed method = type3;
  	class pair trt;
  	model y =  trt;   /* fixed effects in model statement */
  	random pair;      /* random effects in random statement */

  	lsmeans trt;      /* mixed gives the s.e. by default */
  	estimate 'Aff - Unaff' trt 1 -1 /cl;

  	title 'Block analysis, random blocks';
  
/* can add statements to get residuals, contrasts, or m.c.p. as needed */
  	title 'Blocked analysis of Schizophrenia data, random blocks';
run;
          
