

/* SAS code for analyzing the pea yield
   data on assignment 11   */                                         ;


data set1;
 	input treatment $ W Z y1 y2 y3 y4;
  	array y(i) y1-y4;
    do over y;
      	rep=i; 
		yield=y; 
		output;
    end;
  	keep treatment W Z rep yield;
  	datalines;
A 0.0 0.0 359 302 264 317
B 0.5 0.0 373 368 333 356
C 1.0 0.0 343 372 356 336
D 1.5 0.0 365 395 342 361
E 0.0 1.0 331 372 341 327
F 0.0 2.0 377 358 304 299
G 0.5 1.0 342 361 377 314
H 1.0 1.0 377 378 349 385
I 1.5 1.0 374 391 387 341
J 0.5 2.0 359 348 343 327
K 1.0 2.0 360 367 343 312
L 1.5 2.0 339 271 298 310
run;

proc print data=set1;
  	var treatment W Z rep yield;
run;


/*  Create profile plots  */


 goptions cback=white colors=(black)
    targetdevice=ps rotate=landscape;

proc sort data=set1; 
	by W Z; 
run;

proc means data=set1 noprint;
  	by W Z;
  	var yield;
  	output out=means mean=my;
run;

axis1 label=(f=swiss h=2.5 )  ORDER = 0.0 to 1.5 by 0.5
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

axis2 label=(f=swiss h=2.0 a=90)  order = 300 to 380 by 20
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;

axis3 label=(f=swiss h=2.5)  ORDER = 0.0 to 2.0 by 1.0
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=join ;
       SYMBOL2 V=DIAMOND H=2.0 w=3 l=3 i=join ;
       SYMBOL3 V=square H=2.0 w=3 l=9 i=join ;
       SYMBOL4 V=dot H=2.0 w=3 l=15 i=join ;

 PROC GPLOT DATA=means;
   PLOT my*Z=W / vaxis=axis2 haxis=axis3;
   TITLE1  H=3.0 F=swiss "Pea Yields";
       LABEL my='Sample Mean';
       LABEL Z = 'Level of Z (pints/acre)';
   RUN;

  PROC GPLOT DATA=means;
   PLOT my*W=Z / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Pea Yields";
       LABEL my='Sample Mean';
       LABEL W = 'Level of W (pints/acre)';
   RUN;



proc glm data=set1;
  	class W Z;
  	model yield = W Z W*Z / solution p clm ss1 ss2 ss3 ss4;
  	output out=setr r=resid p=yhat;
  	lsmeans W*Z / pdiff stderr;
run;

data setr; 
	set setr; 
	zero=0;
run;

  /* Display a residual plot  */


  axis1 label=(f=swiss h=2.5)
      offset=(2 cm  2 cm)
      value=(f=swiss h=2.0)  w=3.0  length= 6.5 in;

  axis2 label=(f=swiss h=2.0 a=90)
      value=(f=swiss h=2.0) w= 3.0  length = 4.5 in;


  SYMBOL1 V=CIRCLE H=2.0 w=3 l=1 i=none ;


 PROC GPLOT DATA=setr;
   PLOT resid*yhat / vaxis=axis2 haxis=axis1;
   TITLE1  H=3.0 F=swiss "Pea Yield Residuals";
       LABEL resid='Resdiuals';
       LABEL yhat = 'Estimated mean yield';
   RUN;


proc rank data=setr normal=blom out=setr;
	var resid;  
	ranks q;
run;



   axis1 label=(f=swiss h=2.5 )
         value=(f=swiss h=2.0)
         length= 6.5 in;

   axis2 label=(f=swiss h=2.5 a=90)
         value=(f=swiss h=2.0)
         length = 4.5 in;

   SYMBOL1 V=CIRCLE H=3.0 ;

   PROC GPLOT DATA=SETr;
      PLOT resid*q / overlay vaxis=axis2 haxis=axis1;
      TITLE1  H=4.0 F=swiss ;
      TITLE2  H=3.5 F=swiss "Pea Yield Data";
      title3  h=3.5 f=swiss "Normal Probability Plot";
       LABEL yhat='Standard Normal Percentiles';
       LABEL resid = 'Ordered Residuals';
   RUN;
