

/*  The IML procedure in SAS can perform 
    basic matrix computations as well as 
    a variety of other functions 

    We will use the "start" command to create
    a module named "new".  This module is 
    executed with the statement  "run new:" */ 


PROC IML;
	START new;
 
/* A matrix can be entered directly using
   the { } brackets.  A comma is used to 
   end each row in the matrix.  The following
   enters the 5x2 matrix on page 21 of the 
   notes.  Note that any command must be 
   ended with a semicolon. */

	X={42 4, 52 5, 88 7, 58 4, 60 5};

/* This matrix is printed with the 
   following command  */

	print X;

/* Compute the number of rows in X with the
   nrow function and compute the numbers of 
   columns with the ncol function.  */

	n=nrow(X);
	p=ncol(X);	
	print ,, "number of rows: " n;
	print ,, "number of columns: " p;

/* Compute column sums.  Square brackets
   are used to select rows or columns of
   a matrix.  The plus sign below is used
   to sum across rows. */

	csum=X[+, ];    

/* Compute a row vector of sample means */

	means=csum/n;

	print ,,, n,,, csum,,, means;

/*  Compute the sample covariance matrix
    on page 22 of the notes.  This requires
    matrix multiplication denoted by the *
    operator and the transpose of a matrix
    obtained with the t( ) function  */

	S=(X`*X-csum`*csum/n)/(n-1);    

/* Compute the correlation matrix on page 22 
   The DIAG( ) function creates a diagonal matrix.
   The SQRT( ) function computes the square root
               of each element in a matrix 
   The INV( ) function computes the inverse of a 
               matrix */

	scale=INV(SQRT(DIAG(S)));
	R=scale*S*scale;     
	print ,,, S,,, scale,,, R;

/* Define the vectors on page 49 and
   compute transpose, multiply by a 
   scalar, and add two vectors  */

	x={2, 1, -4};
	y={5, -2, 0};
	xt = t(x);
	w=6*x;
	z=x+y;
	d=x-y;
	print ,, x y xt w z d;

/* sum of squares and length (page 52)  */

	x={2, 1, -4, -2};
	ssx=ssq(x); 
	lengthx = sqrt(ssq(x));
	print x ssx lengthx;

/* inner product and cos of angle */

	x={2, 1, -4};
	y={5, -2, 0};
	Lx=sqrt(t(x)*x);
	Ly=sqrt(sum(y#y));
	xy=sum(x#y);
	cos=xy/(Lx*Ly);
	print x y Lx Ly xy cos;

/* examples on page 62 */

	X={2 1 -4, 5 7 0};
	XT = t(x);
	Z=6*X;
	W={2 -1, 0 3}+{2 1, 5 7};
	print X XT Z W;

/* matrix multiplication (page 64) */

	A={2 0 1, 5 1 3};
	B={1 4, -1 3, 0 2};
	AB = A*B;
	C={2 1 , 5 3};
	D={1 4, -1 3};
	CD=C*D;
	DC=D*C;

/* Elementwise multiplication uses the
   "#" operator  */

	W=C#D;

	print ,, A B AB  ,, C D CD DC W;

/* Create identity matrices (page 65) */

	I3 = I(3);
	D = I(2);

	print D I3;

/* create matrices in which all
   entries are the same  */

	A=J(2,3);
	B=J(2,3,0);
	C=J(4,2,-3);

	print ,,A B C;

/* Inverse matrix */

	A ={9 2, 2 4};
	AINV = inv(A);
	W=A*AINV;

	B= { 6 3 1, 3 9 2, 1 2 5};
	BINV = inv(B);

	print ,, A AINV W ,, B BINV ;

/* determinant of a matrix */

	DA = det(A);
	DB = det(B);

	print ,,"determinants: " DA DB;

/* Compute eigenvalues and eigenvectors.  In
   the following call to the eigen function to
   find the eigenvalues and eigenvectors of A,
   M names a diagonal matrix of eigenvalues and
   E names a matrix with eigenvectors as columns */

	A= { 6 3 1, 3 9 2, 1 2 5};
	call eigen(M,E,A);

	print ,,A M E;

	/* Show that E is an orthogonal matrix */

	ETE=t(E)*E;
	EET=E*t(E);

	print ,, ETE EET;

	/* Compute the inverse of A, a square root matrix,
	and an inverse square root matrix */

	Ainv= E*INV(diag(M))*t(E);
	Asqrt= E*sqrt(diag(M))*t(E);
	Asqrtinv = E*inv(sqrt(diag(M)))*t(E);

	print ,, Ainv Asqrt Asqrtinv;

/* Compute trace and determinant of A */

	TraceA=trace(A);
	DetA=det(A);

	print ,, TraceA DetA;

/* Compute covariance matrix for conditional
   normal distribution (pages 126-127)  */

	S={4 0 1 3, 0 4 1 1, 1 1 3 1, 3 1 1 9};
	r1 = {1 3};
	r2 = {2 4};
	S11=S[r1,r1];
	S22=S[r2, r2];
	S12=S[r1, r2];
	S21=S[r2, r1];
	V = S11-S12*inv(S22)*S21;

	print ,,, S ,,, S11 S22 S12 S21 ,,, V;

/* The "FINISH" statement ends the module */

	finish;

/* Now execute the module */
	run new;
quit;
