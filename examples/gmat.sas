
/*  This program is posted as gmat.sas  */
filename gmat url "http://streaming.stat.iastate.edu/~stat501/data/gmat.dat";

DATA set1;
     INFILE gmat;
     INPUT Applicant Admit GPA GMAT;
run;

PROC FORMAT;
     VALUE ADMITFMT 1 = 'Admit'
                    2 = 'Do Not Admit'
                    3 = 'Borderline';
run;

PROC SORT DATA=set1; 
      by Admit; 
run;

PROC PRINT DATA=set1; 
run;

PROC UNIVARIATE DATA=set1 PLOT NORMAL; 
     BY Admit;
     VAR GPA GMAT;
run;

/*  Create a data set with new cases to classify */

data set2;
     input ID Admit GPA GMAT;
     datalines;
     101 .  3.21 497
     102 .  3.78 625
run;

PROC DISCRIM DATA=set1 POOL=TEST SLPOOL=.001 WCOV PCOV CROSSLIST
                WCORR PCORR Manova testdata=set2 testlist;
     CLASS Admit;
     FORMAT TYPE ADMITFMT.;
     VAR GPA GMAT;
run;

/*  Plot the data  */

goptions cback=white colors=(black) targetdevice=ps rotate=landscape;

axis1 label=(f=swiss h=3.5 )
      ORDER = 350 to 700 by 50
      value=(f=triplex h=3.0)
      length= 8.0 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 2 to 4 by 0.2
      value=(f=swiss h=3.0)
      length = 4.5 in;

      SYMBOL1 V=CIRCLE H=2.5 ;
      SYMBOL2 V=square H=2.2 ;
	  Symbol3 V=dot H=2.2;

 PROC GPLOT DATA=SET1;
      PLOT GPA*GMAT=admit / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Business Graduate School Admissions Data";
 RUN;

