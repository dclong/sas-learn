
 /*  This program computes a two-sample Hotelling
     T-squared test to compare mean yeild point (X1)
     and mean ultimate strength (X2) of steel rolled
     at two different temperatures. It also tests 
     for homogeneity of covariance matrices.  
     It is posted on the course web page as 
                  steel.sas     */
filename steel url "http://streaming.stat.iastate.edu/~stat501/data/steel.dat";

DATA SET1;
     INFILE steel;
     INPUT TEMP X1 X2;
     LABEL TEMP = TEMPERATURE
           X1 = YIELD POINT
           X2 = ULTIMATE STRENGTH;
run;

proc print data=set1;
run;
/*  Check normality assumption for each variable
    within each of the two treatment groups and
    compute box plots                           */

PROC BOXPLOT DATA=set1;
     PLOT (X1 X2)*temp / BOXSTYLE=SCHEMATICID;
run;

PROC SORT DATA=SET1; 
     BY TEMP; 
run;

PROC UNIVARIATE DATA=SET1 NORMAL;
     BY TEMP;
     VAR X1 X2;
run;


/* Check homogeneity of covariance matrices */

PROC DISCRIM DATA=SET1 POOL=TEST SLPOOL=.01 
     WCOV PCOV;
     CLASS TEMP;
     VAR X1-X2;
run;

/*  Compute 2-sample Hotelling T-squared test */

PROC GLM DATA=SET1;
     CLASS TEMP;
     MODEL X1 X2 = TEMP / P SOLUTION;
     MANOVA H=TEMP /PRINTH PRINTE;
     LSMEANS TEMP /PDIFF STDERR;
run;


/*  Plot the data  */

goptions cback=white colors=(black) 
    targetdevice=ps rotate=portrait;

axis1 label=(f=swiss h=3.5 )
      ORDER = 32 to 44 by 2
      value=(f=triplex h=3.0)
      length= 6 in;

axis2 label=(f=swiss h=3.5 r=0 a=90)
      order = 56 to 66 by 2
      value=(f=swiss h=3.0)
      length = 6 in;

SYMBOL1 V=CIRCLE H=2.5 ;
SYMBOL2 V=square H=2.2 ;

PROC GPLOT DATA=SET1;
      PLOT x2*x1=temp / vaxis=axis2 haxis=axis1;
      TITLE1  H=3.5 F=swiss "Strength of Steel Samples";
      TITLE2  H=3.0 F=swiss "Effect of Rolling temperature";
      LABEL x1='Yield Point ';
      LABEL x2 = 'Ultimate Strength';
RUN;











