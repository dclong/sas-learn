options noovp;      
/* enables overlay plots on vincent.  not necessary on pc's */
     
data brain;
  	infile 'brain.txt' firstobs = 2;   
  	input gest brain body litter species $;
  	loggest = log(gest);
  	logbrain = log(brain);
  	logbody = log(body);
  	loglitter = log(litter);
  	i =  _n_;    
run;
 /* _n_ is a special variable. it is the obs number */
 /*   assigning it to a variable name adds the obs number to the data */
              
proc reg;
  	model logbrain = loggest logbody loglitter /vif influence ;
/* the vif option gives you the vif for each obs */
/* the influence option gives you h, dffits and dfbeta for each obs */    
  	output out = resids r = resid rstudent = rs 
                      p = yhat cookd = cookd dffits = dffit h = h;
                      
/* various diagnostics can be stored in the output dataset */
/*   the syntax for all is keyword =  variable name */
/*   rstudent =  gives you externally studentized residual */
/*   cookd =  gives you cooks distance */
/*   dffits = gives you dffits */
/*   h =  gives you Hii */
     
/* there are many other pieces of info that can be stored.  See the proc reg */
/*   documentation for all the details */                      
run;
  

proc plot;
   	plot rs*yhat = '*' $ i;
   	plot rs*resid;
   	plot rs*i =  '*';
   	plot cookd*i = '*' ;
   	plot dffit*i = '*' ;
   	plot h*i = '*' ;
run;

/* It common to plot the case diagnostic (on Y) against obs number (on X) */
/*   the last four commands do this for various diagnostics */
/* The = '*' changes the plot symbol to * */
/*   the $ i in the first plot command labels each point with the value of i */
/* Both options can be used for any plot command */
/* The $ i can not be used in gplot. */
