
/* program to generate plots of density
   functions for the family of F-distributions
   This code is posted as fdensity.sas  */

data set1;
	do  i= 1 to 1000;
    	x = i/200;
    	f110 = (1**(1/2))*(10**(10/2))*(x**(-1/2));
    	f110 = f110/(((10+1*x)**((1+10)/2))*gamma(0.5)*gamma(5)/gamma(5.5));
     	f1010 = (10**(10/2))*(10**(10/2))*(x**((10-2)/2));
    	f1010 = f1010/(((10+10*x)**((10+10)/2))*gamma(10/2)*gamma(10/2)/gamma(20/2));
     	f104 = (4**(4/2))*(10**(10/2))*(x**((10-2)/2));
    	f104 = f104/(((4+10*x)**((4+10)/2))*gamma(4/2)*gamma(10/2)/gamma(14/2));
      	f1050 = (50**(50/2))*(10**(10/2))*(x**((10-2)/2));
    	f1050 = f1050/(((50+10*x)**((50+10)/2))*gamma(50/2)*gamma(10/2)/gamma(60/2));
      	f2299 = (99**(99/2))*(22**(22/2))*(x**((22-2)/2));
    	f2299 = f2299/(((99+22*x)**((99+22)/2))*gamma(99/2)*gamma(22/2)/gamma(121/2));
    	output;
 end;

goptions  cback=white colors=black device=WIN target=WINPRTC;

  axis1 label = (h=2.5 r=0 a=90 f=swiss 'density')
        length = 6 in
        color = black width=4.0 style=1
        order = 0 to 1.4 by .2;

  axis2 label = (h=2.5 f=swiss 'quantiles')
        length = 6.5 in
        color=black width=4.0 style=1
        order = 0 to 3 by .5;

  symbol1 v=none i=spline l=1 h=2 c=black width=4;
  symbol2 v=none i=spline l=3 h=2 c=black width=4;
  symbol3 v=none i=spline l=9 h=2 c=black width=4;
  symbol4 v=none i=spline l=16 h=2 c=black width=4;

proc gplot data=set1;
  plot (f110 f1010 f104 f1050)*x / overlay vaxis=axis1 haxis=axis2;
  title1 h=4;
  title2 h=3.5 f=swiss c=black 'F-distribution Densities';
  run;
