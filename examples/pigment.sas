
/* This is a SAS program for computing an ANOVA table
   for data from a nested or heirarchical experiment and
   estimating components of variance.  This program is
   stored in the file

         /home/kkoehler/st500/pigment.prg

   The data are measurements of moisture content of a
   pigment taken from Box, Hunter and Hunter (page 574).  */


data set1;
  	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data/pigment.dat';
  	input batch sample test y;
run;


proc print data=set1;
run;


/*  The  "random"  statement in the following GLM procedure
    requests the printing of formulas for expectations of
    mean squares.  These results are used in the estimation
    of the variance components.                        */

proc glm data=set1;
  	class batch sample;
  	model y = batch sample(batch);
  	random batch sample(batch) / q test;
run;


/*  Alternatively, estimates of variance components are
    produced by the  "varcomp"  procedure in SAS.  Use
    of the  "reml" option will guarantee that no variance
    component estimate is negative.                  */

proc varcomp data=set1 method=reml;
  	class batch sample;
  	model y = batch sample(batch);
run;

