
/*  This program is posted as  chbones.sas
    It applies PROC FACTOR to the estimated 
    correlation matrix for the chicken bone data 
     The sample size is n=276. The variables are
       X1  =  skull length
       X2  =  skull breadth
       X3  =  femur length
       X4  =  tibia length
       X5  =  humerus length
       X6  =  ulna length    */

/*  Enter the data as a TYPE=CORR data set */

DATA chbones(TYPE=CORR);
     INPUT _TYPE_ $ _NAME_ $ X1 X2 X3 X4 X5 X6;
     datalines;
  CORR  X1  1.000 0.505 0.569 0.602 0.621 0.603
  CORR  X2  0.505 1.000 0.422 0.467 0.482 0.450
  CORR  X3  0.569 0.422 1.000 0.926 0.877 0.878
  CORR  X4  0.602 0.467 0.926 1.000 0.874 0.894
  CORR  X5  0.621 0.482 0.877 0.874 1.000 0.937
  CORR  X6  0.603 0.450 0.878 0.894 0.937 1.000
     N   N    276   276   276   276   276   276
run;
 
PROC PRINT data=chbones;
     title "Factor Analyis of Chicken Bone Data";
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals;
     var X1-X6;
     priors smc;
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals  heywood outstat=VarStat;
     var X1-X6;
     priors smc;
run;

proc print data=VarStat;
run;

data VLoading;
     set VarStat;
	 if _type_ eq "PATTERN";
	 if _name_ eq "Factor1" then _name_ = "VF1";
	 else _name_ ="VF2";
RUN;

proc print data=Vloading;
run;

proc transpose data=VLoading out=VLoading2;
run;

proc print data=VLoading2;
run;

proc factor data=chbones(type=corr) method=ml n=2 rotate=promax residuals heywood outstat=ProStat;
     var x1-x6;
	 priors smc;
run;

proc print data=ProStat;
run;

data PLoading;
     set ProStat;
	 where _type_ eq "STRUCTUR";
	 if _name_ eq "Factor1" then _name_ = "PF1";
	 else _name_="PF2";
run;

proc print data=PLoading;
run;

proc transpose data=PLoading out=PLoading2;
     var x1-x6;
run;

proc print data=PLoading2;
run;

data ALoadings;
     merge VLoading2 PLoading2;
run;

proc print data=ALoadings;
run;

proc gplot data=ALoadings;
     plot VF2*VF1 PF2*pf1/overlay legend=legend1 haxis=axis1 vaxis=axis2;
	 goptions reset=all;
	 symbol1 v=circle c=red h=1.5 w=2;
	 symbol2 v=triangle c=blue h=1.5 w=2;
	 axis1 label=(f="Arial" c=magenta h=5pct "Factor1") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct);
     axis2 label=(f="Arial" c=magenta h=5pct "Factor2") c=cyan width=3 minor=none
               value=(f="Arial" c=red h=3pct); 
	 legend1 down=2 position=(top left inside) cshadow=black frame
	           value=(f="Arial" h=12pt t=1 "Varimax" t=2 "Promax")
			   label=(f="Arial" h=2 "Method");
	 title f="Arial" c=black h=18pt "Factor plot of Varimax and Promax";
run;


/*  A SAS program to analyze the bank note data
    The data are stored in the file

                 bnotes.dat

    and the program is stored in the file

                 bnotes.sas
*/
filename bnotes url "http://streaming.stat.iastate.edu/~stat501/data/bnotes.dat";

data bnotes;
     infile bnotes;
     input id x1 x2 x3 x4 x5 x6;
run;

/*   FIRST PRINT THE DATA FILE */
PROC PRINT DATA=bnotes;
run;

proc gplot data=bnotes;
     symbol i=join ci=bule v=triangle cv=red h=1.5;
     plot x1*x2/cframe=pink ctext=blue caxis=cyan;
	 title font="Arial" c=green "Just try a plot";
run;

proc gplot data=bnotes;
     goptions reset=all;
	 plot x1*x2/haxis=axis1 vaxis=axis2;
	 symbol v=triangle h=1.5 i=join c=green w=2;
	 title c=blue font="Arial" "Try another plot";
	 axis1 lable=(f=complex c=blue h=3pct) c=magenta width=3 minor=none;
     axis2 lable=(a=-90 r=90 f=complex c=blue h=3pct "X1") c=magenta width=3;
run; 



/*   COMPUTE CORRELATIONS */

PROC CORR DATA=bnotes;
     VAR X1-X6; 
run;

/*  Compute the Shapiro-Wilk W tests 
   for normality */

PROC UNIVARIATE DATA=bnotes NORMAL;
     VAR X1-X6;  
run;

/*   COMPUTE NORMAL Q-Q PLOTS  */

PROC RANK DATA=bnotes NORMAL=BLOM OUT=bnotes;
     VAR X1-X6; 
     RANKS Q1-Q6;
run;

/*   Use SASGRAPH to display the normal 
     probability plots */

/*  WINDOWS users should use these options 
    to prepare the figure for printing 
    as a postscript file*/

goptions cback=white colors=(black) device=WIN target=ps;

  axis1 label=(h=2.5 r=0 a=90 f=swiss)
        length = 5 in
        value=(h=0.25in f=swiss)
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Standard Normal quantiles')
        value=(h=0.25in f=swiss)
        length = 5.5 in
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=bnotes; 
  PLOT X1*Q1 / vaxis=axis1 haxis=axis2;
  PLOT X2*Q2 / vaxis=axis1 haxis=axis2;
  PLOT X3*Q3 / vaxis=axis1 haxis=axis2;
  PLOT X4*Q4 / vaxis=axis1 haxis=axis2; 
  PLOT X6*Q6 / vaxis=axis1 haxis=axis2;  
title ls=2.0in h=4 f=swiss  'Normal Q-Q Plot';
  run;
run; 



/*   USE PROC IML    
      TO COMPUTE SUMMARY STATISTICS, PARTIAL 
      CORRELATIONS, CHI-SQUARE Q-Q PLOT FOR  
      MULTIVARIATE NORMALITY, AND A GOODNESS_
      OF-FIT TEST  */


DATA newnotes; SET bnotes;
  KEEP X1-X6;

PROC IML;
START NORMAL;
USE newnotes;          *ENTER THE DATA;
  READ ALL  INTO X;
N=NROW(X);             *NUMBER OF OBSERVATIONS IS N;
P=NCOL(X);             *COMPUTE NUMBER OF TRAITS = P;
SUM=X[+, ];            *COMPUTE TOTAL FOR EACH TRAIT;
A=X`*X-SUM`*SUM/N;     *COMPUTE CORRECTED CROSSPRODUCTS MATRIX;
S=A/(N-1);             *COMPUTE SAMPLE COVARIANCE MATRIX;
XBAR=SUM/N;            *COMPUTE TRANSPOSE OF SAMPLE MEAN VECTOR;
SCALE=INV(SQRT(DIAG(A)));
R=SCALE*A*SCALE;       *COMPUTE SAMPLE CORRELATION MATRIX;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;

                       * Compute a chi-square probability plot;

E=X-(J(N,1)*XBAR);     *MATRIX OF DEVIATIONS FROM MEAN VECTOR;
D=VECDIAG(E*INV(S)*E`);    *SQUARED MAHALANOBIS DISTANCES;
RD = RANK(D);            * COMPUTE RANKS FOR THE D VALUES;
RD =(RD-.5)/N;
PD2=P/2;
Q=2*GAMINV(RD,PD2);  

/* COMPUTE CHI-SQUARE PLOTTING POSITIONS */
DD=D||Q;
CREATE CHISQ FROM DD ;   

/* OPEN A FILE TO TEMPORARILY STORE RESULTS */
APPEND FROM DD;

/* Compute the p-value for a correlation test
   of multivariate normality, using the points on 
   chi-square probability plot */

rpn = t(D)*Q-(sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)*(ssq(Q)-(sum(Q)**2)/N));

/* Simultate the p-value for the 
   correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)*(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;
RUN NORMAL;



/*   Display THE CHI-SQUARE PROBABILITY PLOT FOR 
     ASSESSING MULTIVARIATE NORMALITY */



  axis1 label = (h=2.5 r=0 a=90 f=swiss 'Ordered distances')
        value =(h=0.25in f=swiss)
        length = 5 in
        color = black width=4.0;

  axis2 label = (h=2.5 f=swiss 'Chi-square quantiles')
        length = 5.5 in
		value =(h=0.25in f=swiss)
        color=black width=4.0;

  symbol1 v=circle h=2 c=black width=2;

proc gplot data=CHISQ;
     plot col1*col2 / vaxis=axis1 haxis=axis2;
     title ls=2.0in h=4 f=swiss  'Chi-square Plot';
run;


/* This program computes summary statistics and
   tests of univariate normality for the board 
   stiffness data in example 4.14. It also creates
   a chi-square probability plot to test for
   multivariate normality. PROC IML in SAS is used 
   to generate points for the chi-square probability
   plot, to compute a sample covariance matrix, 
   the sample mean vector, sample correlation 
   matrix, partial correlation matrix, and related 
   t-tests. This program is posted in the SAS folder
   of the course web page as board.stiffness.sas  */

filename board url "http://streaming.stat.iastate.edu/~stat501/data/board.stiffness.dat";

data set1;
     INFILE board;
     INPUT Board X1 X2 X3 X4;
run;

/*   First print the data file  */

PROC PRINT DATA=set1;
     Title "Board Stiffness Data";
RUN;

/* You can make a scatterplot matrix and other
  data dipslays using the interactive data 
  analysis option in SAS.  Click on "Solutions"
  at the top of the SAS editing window.  
  Select "Analysis".  Then click on 
  "Interactive Data Analysis". Select the
  "Work" library and select the set1 data set.
  A spreadsheet containing the data will 
  appear.  Click on the "analyze" button that 
  now appears at the top of the editing window 
  and select scatter plot.  Select the variables
  to appear on the plot. */ 

/*  Compute correlations  */

proc insight data=set1;
      scatter x1 x2 x3*x1 x2 x3;
run;

PROC CORR DATA=set1;
     VAR X1-X4;
RUN;

/* Display box plots */

DATA set1; 
     SET set1;
     Z=1; 
run;

proc print data=set1;
run;

PROC BOXPLOT DATA=set1;
     PLOT (X1-X4)*Z / BOXSTYLE=SCHEMATICID;
run;

/* Check the quality of the data. Compute 
   summary statistics and tests of normality
   for each variable */

PROC UNIVARIATE DATA=set1 NORMAL;
     VAR X1-X4;
RUN;

/*   Compute Q-Q plots  */

PROC RANK DATA=set1 NORMAL=BLOM OUT=set1;
     VAR X1-X4; 
     RANKS Q1-Q4;
RUN;

PROC PRINT DATA=set1; 
run;

goptions device=WIN target=ps rotate=portrait;

/* Specify features of the plot */

axis1 label=(f=swiss h=2.5  
      "Standard Normal Quantiles")
      ORDER = -2.5 to 2.5 by .5
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Data ")
      value=(f=triplex h=2.0)
      length = 5.0in;

  SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT X1*Q1 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X1";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X2*Q2 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X2";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X3*Q3 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X3";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X4*Q4 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X4";
     footnote ls=1.0in;
RUN;


/* Use PROC IML to compute summary statistics
   partial correlations, related t-tests,
   a chi-square Q-Q plot for assessing multivariate
   normality, and a goodness-of-fit test */

DATA set2; 
     SET set1;
     KEEP X1-X4;
RUN;

PROC IML;
     START NORMAL;
     USE set2;            /* Enter the data */
     READ ALL  INTO X;
     N=NROW(X);             /* Number of observations is N */
     P=NCOL(X);             /* Number of traits is p */
     SUM=X[+, ];            /* Total for each trait */
     A=X`*X-SUM`*SUM/N;     /* Corrected crossproducts matrix */
     S=A/(N-1);             /* Sample covariance matrix */
     XBAR=SUM/N;            /* Sample mean vector */
     SCALE=INV(SQRT(DIAG(A)));
     R=SCALE*A*SCALE;       /* Sample correlation matrix */
     PTR=J(P,P); /* 1 matrix with dimension P*P*/
     TR=J(P,P);
     DF=N-2;
     DO I1=1 TO P;          /* T-tests for correlations */
         IP1=I1+1;
         DO I2=IP1 TO P;
               TR[I1,I2]=SQRT(N-2)#R[I1,I2]/SQRT(1-R[I1,I2]##2);
               TR[I2,I1]=TR[I1,I2];
               PTR[I1,I2]=(1-PROBT(ABS(TR[I1,I2]),DF))*2;
               PTR[I2,I1]=PTR[I1,I2];
         END;
     END;
     RINV=INV(R);           /* Partial Correlations */
     SCALER=INV(SQRT(DIAG(RINV)));
     PCORR=-SCALER*RINV*SCALER;
     DO I = 1 TO P;
          PCORR[I,I]=1.0;
     END;
     PTPCORR=J(P,P);
TPCORR=J(P,P);
DF=N-P;
DO I1=1 TO P;  /* T-tests for partial correlations */
IP1=I1+1;
  DO I2=IP1 TO P;
    TPCORR[I1,I2]=SQRT(N-P)#PCORR[I1,I2]/
                  SQRT(1-PCORR[I1,I2]##2);
    TPCORR[I2,I1]=TPCORR[I1,I2];
    PTPCORR[I1,I2]=
        (1-PROBT(ABS(TPCORR[I1,I2]),DF))*2;
    PTPCORR[I2,I1]=PTPCORR[I1,I2];
  END;
END;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;
PRINT,,,,," VALUES OF T-TESTS FOR ZERO CORRELATIONS";
PRINT TR;
PRINT,,,,," P-VALUES FOR THE T-TESTS FOR ZERO CORRELATIONS";
PRINT PTR;
PRINT,,,,," THE MATRIX OF PARTIAL CORRELATIONS CONTROLLING FOR";
PRINT "       ALL VARIABLES NOT IN THE PAIR";
PRINT PCORR;
PRINT,,,,," VALUES OF T-TESTS FOR PARTIAL CORRELATIONS";
PRINT TPCORR;
PRINT,,,,," P-VALUES FOR T-TESTS FOR PARTIAL CORRELATIONS";
PRINT PTPCORR;

/* Compute plotting positions
   for a chi-square probability plot */

E=X-(J(N,1)*XBAR);       
D=VECDIAG(E*INV(S)*E`);  /* Squared Mah. distances */
RD = RANK(D);            /* Compute ranks  */
  RD=(RD-.5)/N;
PD2=P/2;
  Q=2*GAMINV(RD,PD2);    /* Plotting positions */
  DQ=D||Q;
  print DQ;
CREATE CHISQ FROM DQ ;   /* Open a file to store results */
APPEND FROM DQ;

                         /* Compute test statistic */
rpn = t(D)*Q - (sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)
         *(ssq(Q)-(sum(Q)**2)/N));

/* Simultate a p-value for the correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)
           *(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;

RUN NORMAL;


/* Display the chi-square probability plot 
   for assessing multivariate normality */

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Distances ")
      value=(f=triplex h=2.0)
      length = 5.0in;

axis1 label=(f=swiss h=2.5 
      "Chi-Square Quantiles")
      value=(f=triplex h=1.6)
      length= 5.5in;
 
 SYMBOL1 V=CIRCLE H=2.0 ;
 
PROC GPLOT DATA=CHISQ;
     PLOT Col1*COL2 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "CHI-SQUARE PLOT";
     Title2 H=2.3 F=swiss "Board Stiffness Data";
     footnote ls=1.0in;
RUN;


/* This program does the computations 
   for problems 6 and 7 on assignment 3. 
   It is posted as  broota.sas */

/*  Enter the data  */
filename broota89 url "http://streaming.stat.iastate.edu/~stat501/data/broota89.dat";

data set1;
     infile broota89;
     input subject group x1-x6;
     z1=1;
     z2=1; 
     if(group=1) then z2=-1;
run;

proc print data=set1;
run;

/*  Computations for problem 3  */

data set2; 
     set set1;
     array a(i) x1-x6;
     do over a;
        x=a; 
        task=int((i-1)/3)+1; 
        cue=i; 
        if(i>3) then cue=i-3; 
        output;
     end;
     keep subject group task cue x ;  
run;

proc print data=set2;
run;

data _NULL_; 
      set set2;
      file "I:\Study\Spring 2009\STAT 501\Homework\HW3\broota892.dat";
      put (subject group task cue x) (4.0 4.0 4.0 4.0 6.0);
run;

proc glm data=set2;
     class subject group task cue;
     model x = group subject(group) task 
            task*group task*subject(group)
            cue cue*group cue*subject(group) 
            task*cue group*task*cue;
    test h=group  e=subject(group);
    test h=task task*group  e=task*subject(group);
    test h=cue cue*group  e=cue*subject(group);
run;

/*  Test hypotheses in problem 4 on assignment 4 */

proc glm data=set1;
     class group;
     model x1-x6 = group;
     manova h=group;                              /* part A */
     manova h=group m=x1+x2+x3-x4-x5-x6;          /* part E */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;   /* part F */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;   /* part G */
run;

proc glm data=set1;
     class group;
     model x1-x6 = group / noint;
     manova h=group;
     manova h=group m=x1+x2+x3-x4-x5-x6;           /* part B */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;    /* part C */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;    /* part D */
run;

proc glm data=set1;
     model x1-x6 = z1 z2 / noint;                /* part H */
     manova h=z1 m=x1-x3-x4+x6, x2-x3-x5+x6;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=cs subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=un subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=vc subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;


/*  This program is posted as  chbones.sas
    It applies PROC FACTOR to the estimated 
    correlation matrix for the chicken bone data 
     The sample size is n=276. The variables are
       X1  =  skull length
       X2  =  skull breadth
       X3  =  femur length
       X4  =  tibia length
       X5  =  humerus length
       X6  =  ulna length    */

/*  Enter the data as a TYPE=CORR data set */

DATA chbones(TYPE=CORR);
     INPUT _TYPE_ $ _NAME_ $ X1 X2 X3 X4 X5 X6;
     datalines;
  CORR  X1  1.000 0.505 0.569 0.602 0.621 0.603
  CORR  X2  0.505 1.000 0.422 0.467 0.482 0.450
  CORR  X3  0.569 0.422 1.000 0.926 0.877 0.878
  CORR  X4  0.602 0.467 0.926 1.000 0.874 0.894
  CORR  X5  0.621 0.482 0.877 0.874 1.000 0.937
  CORR  X6  0.603 0.450 0.878 0.894 0.937 1.000
     N   N    276   276   276   276   276   276
run;
 
PROC PRINT data=chbones;
     title "Factor Analyis of Chicken Bone Data";
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals;
     var X1-X6;
     priors smc;
run;

/*  Compute factors and apply a varimax rotation */

PROC FACTOR DATA=chbones(TYPE=CORR) METHOD=ml N=2 rotate=varimax residuals  heywood;
     var X1-X6;
     priors smc;
run;

proc factor data=chbones(type=corr) method=ml n=2 rotate=promax residuals heywood outstat=ProStat;
     var x1-x6;
	 priors smc;
run;

proc print data=ProStat;
run;

data PLoading;
     set ProStat;
	 where _type_ eq "STRUCTUR";
run;

proc print data=PLoading;
run;

proc transpose data=PLoading out=PLoading2;
run;

proc print data=PLoading2;
run;


/*  This program analyzes the cork data
    posted in the file    cork.dat    */
filename cork url "http://streaming.stat.iastate.edu/~stat501/data/cork.dat";

data set1;
     infile cork;
     input  tree North East South West;
     Z = 1;
run;

proc print data=set1; 
run;

proc univariate data=set1 normal;
     var North East South West;
run;

data set2;
     set set1;
	 keep north east south west;
run;

proc print data=set2;
run;
/* Test the null hypothesis that the 
   cork deposities are the same mean 
   thickness on all side of the trees  */
proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-East, North-South, 
            North-West  prefix=c /printe ;
run;

proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-South, East-West, 
            North+South-East-West  prefix=c /printe ;
run;


/* Do a mixed model analysis of variance.
   Restructure the data file to put each
   observation on a different line  */

data set3; 
     set set1;
     array A(I) North East South West;
     do over A;
        Y = A;
        side = I;
        output;
     end;
     keep tree side Y;
run;

proc print data=set3;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
	 random tree;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;


/* Use the REPEATED option in PROC GLM 
   to do both analyses */

proc glm data=set1;
     model North East South West = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;




/*  This program is posted as  corresp.sas
    It performs a simple correspondence analysis for the work
    activity/benefits contingency table discussed in the lectures.
    The table of counts is posted in the file  corresp.dat  */

/*  Enter the observed counts */

data set1;
infile "c:/stat501/data/corresp.dat";
 input work $ varw free huma sche sala secu comp inte near atmo soci
           auto like othe none outd noa node grad smal medi larg;
run;


/*  Output the table as a matrix with one column and retain the
    column labels for the original table as the variable  'job'  */

data x2;
 set set1;
 job='varw';y=varw;output;
 job='free';y=free;output;
 job='huma';y=huma;output;
 job='sche';y=sche;output;
 job='sala';y=sala;output;
 job='secu';y=secu;output;
 job='comp';y=comp;output;
 job='inte';y=inte;output;
 job='near';y=near;output;
 job='atmo';y=atmo;output;
 job='soci';y=soci;output;
 job='auto';y=auto;output;
 job='like';y=like;output;
 job='othe';y=othe;output;
 job='none';y=none;output;
 job='outd';y=outd;output;
 job='noa ';y=noa ;output;

 keep work job y;
run;


/*  Display the original continency table  */

proc freq data=x2;
  tables work*job / chisq;
  weight y;
run;


/*  Do the correspondence analysis and output results to the
    temporary SAS file called 'results'                     */

data france;
  set set1;

proc corresp data=france out=results cp rp dimens=3 profile=both short  ;
     var      varw free huma sche sala secu comp inte near atmo soci auto like
           othe none outd noa node grad smal medi larg;
     supplementary node grad smal medi larg;
     id  work;
run;

proc print data=results;
run;

proc plot data=results vtoh=2;
   plot dim2*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim1='*' $ work  /box href=0 vref=0;
   plot dim3*dim2='*' $ work  /box href=0 vref=0;
run;


/*  This program performs cluster analysis on the Stanford
    diabetes data.  The code is posted as diabetes.sas
	and the data are posted as in the data folder as diabetes.dat

    OGTT refers to a three hour oral glucose tolerance test  */


DATA SET1;
  INFILE 'c:/stat501/data/diabetes.dat';
  INPUT patient x1-x5;
/*  LABEL  x1 = 'relative weight'
           x2 = 'fasting plasma glucose'
           x3 = 'area under the plasma glucose curve (OGTT)'
           x4 = 'area under plasma insulin curve (OGTT)'
           x5 = 'steady state plasma glucose (SSPG)';           */
  run;


/* First standardize each variable to have mean zero and
   unit standard deviation                               */

proc standard data=set1 out=set2 mean=0 std=1 ;
   var x1-x5;
   run;

/*  This is a SAS macro for hierarchical cluster analysis.
    It uses PROC CLUSTER followed by PROC TREE to produce
    a file, callled OUT, containing the original data and
    a variable called CLUSTER that defines the clusters.
    PROC CANDISC provides additional information about the
    clusters as well as 2-dimensional projections.  To apply
    this macro to other data you will need to change the
    data set name, the list of variables in the  VAR 
    statement, the name of the subject identification
    variable in the ID statement, the title in the TITLE
    statement. */


%MACRO ANALYZE(METHOD,NCL);
   PROC CLUSTER DATA=SET2 OUT=TREE METHOD=&METHOD P=35 ccc STANDARD;
   VAR X1-X5;
   ID patient;
   TITLE "Stanford Diabetes Data";
   TITLE2;
   %LET K=1;
   %LET N=%SCAN(&NCL,&K);
   %DO %WHILE(&N NE);

/* Compute the tree and create a file with cluster labels */

PROC TREE DATA=TREE NOPRINT OUT=OUT NCL=&N;
  COPY PATIENT x1-x5;
   TITLE2 "COMPUTE &N CLUSTERS USING METHOD=&METHOD";
PROC SORT DATA=OUT; BY CLUSTER;

/* Delete the following line to avoid printing the 
   data with a column of cluster labels            */

   proc print data=out; run;                                   

/*  Compute canonical discriminants and display clusters */

PROC CANDISC data=out simple OUT=CAN ncan=5 prefix=can;
     CLASS cluster;
     VAR x1-x5;
     run;

PROC PLOT;
     PLOT CAN2*CAN1=CLUSTER/HPOS=56;
     PLOT CAN3*CAN1=CLUSTER/HPOS=56;
     TITLE2 "PLOT OF &N CLUSTERS FROM METHOD=&METHOD";
     RUN;
%LET K=%EVAL(&K+1);
%LET N=%SCAN(&NCL,&K);
%END;
%MEND;


/*  Run the macro for several methods        */

   %ANALYZE(WARDS, 7 10)
   %ANALYZE(centroid, 7)
   %ANALYZE(complete, 7)
   %ANALYZE(average, 7)
   %ANALYZE(single, 7)

   
/*  This program performs a one-sample 
    Hotelling T-squared test for equality 
    of means for repeated measures.  The
    program is posted as  dogs.sas

    This program analyzes the dog data
    posted in the file    dogs.dat    */
filename dog url "http://streaming.stat.iastate.edu/~stat501/data/dogs.dat";

data set1;
       infile dog;
       input  dog x1-x4;
       Z = 1;
  /*
  LABEL X1 = High CO2 without Halothane
        X2 = Low CO2 without Halothane
        X3 = High CO2 with Halothane
        X4 = Low CO2 with Halothane;
  */
run;

proc print data=set1; 
run;

title 'Time between heartbeats (msec)';


/* Test the null hypothesis that 
   all treatments have the same mean 
   time between heartbeats  */

proc glm data=set1;
     model x1-x4 = z /noint nouni;
     manova H=z M=x1-x2+x3-x4, -x1-x2+x3+x4, 
          -x1+x2+x3-x4 prefix=c /printe ;
run;

/* Do a mixed model analysis of variance.
   Restructure the data file to put each
   observation on a different line  */

data set3; 
     set set1;
     array A(I) x1-x4;
     do over A;
        x = A;
        treat = I;
        output;
     end;
     keep dog treat x;
run;

proc print data=set3;
run;

proc glm data=set3;
     class treat dog;
     model x = dog treat / solution;
     random dog;
     means treat / bon tukey ;
run;

/* Use the REPEATED option in PROC GLM 
   to do both analyses */

proc glm data=set1;
     model x1-x4 = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

/* Use the MIXED procedure in SAS */

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=cs subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=un subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;



/* This program is posted as  
           effluent.sas
  on the course web page. It computes a
  one-sample Hotelling T-squared test. */

DATA set1;
     INPUT sample BOD1 SS1 BOD2 SS2;
     dBOD=BOD1-BOD2;
     dSS = SS1-SS2;
     Z=1;
  /* LABEL BOD1 = BOD (private lab)
        SS1 = SS (private lab)
        BOD2 = BOD (state lab)
        SS2 = SS (state lab);  */
datalines;
 1  6  27 25 15
 2  6  23 28 13
 3 18  64 36 22
 4  8  44 35 29
 5 11  30 15 31
 6 34  75 44 64 
 7 28  26 42 30
 8 71 124 54 64
 9 43  54 34 56
10 33  30 29 20
11 20  14 39 21
run;

PROC PRINT DATA=set1;
run;

proc corr data=set1;
     var dBOD dSS; 
run;


/* Create high quality graphs with SASGRAPH */

goptions targetdevice=ps300 rotate=portrait;

axis1 label=(f=swiss h=2.5  
      "Differences in BOD Measurements")
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Differences in SS Measurements ")
      value=(f=triplex h=2.0)
      length = 5.0in;

SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT dBOD*dSS / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "Differences in Effluent Measurements";
     footnote ls=1.0in;
RUN;

Proc Univariate data=set1 normal;
     var dBOD dSS; 
run;

PROC GLM DATA=set1;
     MODEL dBOD dSS = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
run;






