
  /*  This program uses PROC IML in SAS to
      compute sample sizes for 2-sample t-tests

      It is posted as  size2.sas  */


   proc iml;
     start size2;

     alpha = .05;   /* specify the type I error level */
                    /* specify the power of the test */

     power = {0.63 0.8}; 
    
     d = 1.48/2.85;   /* specify difference to be detected
                        divided by standard deviation for
                        one observation */

	 np=ncol(power);  /* Compute number of power values */
                      /* Compute the type II error level */

     beta = j(1,np,1) - power;

     do i=1 to np;   /* Cycle through power values */

     n = (2*(probit(alpha/2) + probit(beta[1,i]))**2)/(d*d);

     n = int(n) + 1;   /* round up to next largest integer */
     v = 2*(n-1);      /* Compute degrees of freedom  */

     n = (2*(tinv(alpha/2,v) + tinv(beta[1,i],v))**2)/(d*d);

     n = int(n) + 1;   /* round up to next largest integer */
     pow=power[1,i];

     print,,,, "Two-sample ttest:" ;
     print   "           Significance level:  "  alpha;
     print   "                        power:  "  pow;
     print   "            Scaled difference:  "  d;
     print   " Sample size (two-sided test):  "  n;


    /* Compute sample size for a one-sided two-sample t-test */

     n = (2*(probit(alpha) + probit(beta[1,i]))**2)/(d*d);
     n = int(n) + 1;

     v = 2*(n-1);

     n = (2*(tinv(alpha,v) + tinv(beta[1,i],v))**2)/(d*d);
     n = int(n) + 1;

     print   " Sample size (one-sided test):  " n;

     end;    /* end the loop */

    finish;

    run size2;
