**************************Chuanlong Du*******************************************;
/*
Functions:
This program is used to do the correlation test, i.e. it use simulation to test 
whether the observations are multivariate normal distributed.
Parameters:
Parameter X is the observation matrix;
*/ 
PROC IML;
     START NORMAL(X);
     N=NROW(X);*Number of observations is N;
     P=NCOL(X);*Number of traits is p;
     SUM=X[+, ];*Total for each trait;
     A=X`*X-SUM`*SUM/N;*Corrected crossproducts matrix;
     S=A/(N-1);*Sample covariance matrix;
     XBAR=SUM/N;*Sample mean vector;
     SCALE=INV(SQRT(DIAG(A)));
     R=SCALE*A*SCALE;*Sample correlation matrix;
	 E=X-(J(N,1)*XBAR);*centered design matrix;     
     D=VECDIAG(E*INV(S)*E`);* Squared Mah. distances;
     RD = RANK(D);*ranks of the distances;
     RD=(RD-.5)/N;*percentiles;
     PD2=P/2;*corresponding degrees of freedom of gamma distribution;
     Q=2*GAMINV(RD,PD2);*theoretical quantile;
     DQ=D||Q;*combine D and Q together;
     CREATE CHISQ FROM DQ ;*create a data set name chisq;
     APPEND FROM DQ;*write data into the data set;
	 rpn = t(D)*Q - (sum(D)*sum(Q))/N;*computer correlation between D and Q;
     rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)*(ssq(Q)-(sum(Q)**2)/N));
/*******The following code simulate a p-value for the correlation test******/
	 ns=10000;
     pvalue=0;
     do i=1 to ns;
          do j1=1 to n;
                do j2=1 to p;
                      x[j1,j2] = rannor(-100);
                end;
          end;
          SUMX=X[+, ];           
          A=t(X)*X-t(SUMX)*SUMX/N;     
          S=A/(N-1);             
          XBAR=SUMX/N;           
          E=X-(J(N,1)*XBAR);   
          DN=VECDIAG(E*INV(S)*t(E));   
          RN = RANK(DN);            
          RN =(RN-.5)/N;
          PD2=P/2;
          QN=2*GAMINV(RN,PD2);  
          rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
          rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)*(ssq(QN)-(sum(QN)**2)/N));
          if(rpn>rpnn) then pvalue=pvalue+1;   
     end;
     pvalue=pvalue/ns;
	 print "Correlation Test of Normality";
     print N P rpn pvalue;
     FINISH;
*********************Initial Parameters***************************************;

*********************Run the Function*****************************************;
     RUN NORMAL(X);
quit;
