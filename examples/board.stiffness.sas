
/* This program computes summary statistics and
   tests of univariate normality for the board 
   stiffness data in example 4.14. It also creates
   a chi-square probability plot to test for
   multivariate normality. PROC IML in SAS is used 
   to generate points for the chi-square probability
   plot, to compute a sample covariance matrix, 
   the sample mean vector, sample correlation 
   matrix, partial correlation matrix, and related 
   t-tests. This program is posted in the SAS folder
   of the course web page as board.stiffness.sas  */

filename board url "http://streaming.stat.iastate.edu/~stat501/data/board.stiffness.dat";

data set1;
     INFILE board;
     INPUT Board X1 X2 X3 X4;
run;

/*   First print the data file  */

PROC PRINT DATA=set1;
     Title "Board Stiffness Data";
RUN;

/* You can make a scatterplot matrix and other
  data dipslays using the interactive data 
  analysis option in SAS.  Click on "Solutions"
  at the top of the SAS editing window.  
  Select "Analysis".  Then click on 
  "Interactive Data Analysis". Select the
  "Work" library and select the set1 data set.
  A spreadsheet containing the data will 
  appear.  Click on the "analyze" button that 
  now appears at the top of the editing window 
  and select scatter plot.  Select the variables
  to appear on the plot. */ 

/*  Compute correlations  */

proc insight data=set1;
 	scatter x1 x2 x3*x1 x2 x3;
run;

PROC CORR DATA=set1;
     VAR X1-X4;
RUN;

/* Display box plots */

DATA set1; 
     SET set1;
     Z=1; 
run;

proc print data=set1;
run;

PROC BOXPLOT DATA=set1;
     PLOT (X1-X4)*Z / BOXSTYLE=SCHEMATICID;
run;

/* Check the quality of the data. Compute 
   summary statistics and tests of normality
   for each variable */

PROC UNIVARIATE DATA=set1 NORMAL;
     VAR X1-X4;
RUN;

/*   Compute Q-Q plots  */

PROC RANK DATA=set1 NORMAL=BLOM OUT=set1;
     VAR X1-X4; 
     RANKS Q1-Q4;
RUN;

PROC PRINT DATA=set1; 
run;

goptions device=WIN target=ps rotate=portrait;

/* Specify features of the plot */

axis1 label=(f=swiss h=2.5  
      "Standard Normal Quantiles")
      ORDER = -2.5 to 2.5 by .5
      value=(f=triplex h=1.6)
      length= 5.5in;

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Data ")
      value=(f=triplex h=2.0)
      length = 5.0in;

  SYMBOL1 V=CIRCLE H=2.0 ;

PROC GPLOT DATA=set1;
     PLOT X1*Q1 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X1";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X2*Q2 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X2";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X3*Q3 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X3";
     footnote ls=1.0in;
RUN;

PROC GPLOT DATA=set1;
     PLOT X4*Q4 / vaxis=axis2 haxis=axis1;
     TITLE1 ls=1.5in H=3.0 F=swiss "NORMAL PROBABILITY PLOT";
     TITLE2  H=2.3 F=swiss "X4";
     footnote ls=1.0in;
RUN;


/* Use PROC IML to compute summary statistics
   partial correlations, related t-tests,
   a chi-square Q-Q plot for assessing multivariate
   normality, and a goodness-of-fit test */

DATA set2; 
     SET set1;
     KEEP X1-X4;
RUN;

PROC IML;
     START NORMAL;
     USE set2;            /* Enter the data */
     READ ALL  INTO X;
     N=NROW(X);             /* Number of observations is N */
     P=NCOL(X);             /* Number of traits is p */
     SUM=X[+, ];            /* Total for each trait */
     A=X`*X-SUM`*SUM/N;     /* Corrected crossproducts matrix */
     S=A/(N-1);             /* Sample covariance matrix */
     XBAR=SUM/N;            /* Sample mean vector */
     SCALE=INV(SQRT(DIAG(A)));
     R=SCALE*A*SCALE;       /* Sample correlation matrix */
     PTR=J(P,P); /* 1 matrix with dimension P*P*/
     TR=J(P,P);
     DF=N-2;
     DO I1=1 TO P;          /* T-tests for correlations */
         IP1=I1+1;
         DO I2=IP1 TO P;
               TR[I1,I2]=SQRT(N-2)#R[I1,I2]/SQRT(1-R[I1,I2]##2);
               TR[I2,I1]=TR[I1,I2];
               PTR[I1,I2]=(1-PROBT(ABS(TR[I1,I2]),DF))*2;
               PTR[I2,I1]=PTR[I1,I2];
         END;
     END;
     RINV=INV(R);           /* Partial Correlations */
     SCALER=INV(SQRT(DIAG(RINV)));
     PCORR=-SCALER*RINV*SCALER;
     DO I = 1 TO P;
          PCORR[I,I]=1.0;
     END;
     PTPCORR=J(P,P);
TPCORR=J(P,P);
DF=N-P;
DO I1=1 TO P;  /* T-tests for partial correlations */
IP1=I1+1;
  DO I2=IP1 TO P;
    TPCORR[I1,I2]=SQRT(N-P)#PCORR[I1,I2]/
                  SQRT(1-PCORR[I1,I2]##2);
    TPCORR[I2,I1]=TPCORR[I1,I2];
    PTPCORR[I1,I2]=
        (1-PROBT(ABS(TPCORR[I1,I2]),DF))*2;
    PTPCORR[I2,I1]=PTPCORR[I1,I2];
  END;
END;

PRINT,,,,," THE SAMPLE MEAN VECTOR";
PRINT XBAR;
PRINT,,,,," THE SAMPLE CORRELATION MATRIX";
PRINT R;
PRINT,,,,," VALUES OF T-TESTS FOR ZERO CORRELATIONS";
PRINT TR;
PRINT,,,,," P-VALUES FOR THE T-TESTS FOR ZERO CORRELATIONS";
PRINT PTR;
PRINT,,,,," THE MATRIX OF PARTIAL CORRELATIONS CONTROLLING FOR";
PRINT "       ALL VARIABLES NOT IN THE PAIR";
PRINT PCORR;
PRINT,,,,," VALUES OF T-TESTS FOR PARTIAL CORRELATIONS";
PRINT TPCORR;
PRINT,,,,," P-VALUES FOR T-TESTS FOR PARTIAL CORRELATIONS";
PRINT PTPCORR;

/* Compute plotting positions
   for a chi-square probability plot */

E=X-(J(N,1)*XBAR);       
D=VECDIAG(E*INV(S)*E`);  /* Squared Mah. distances */
RD = RANK(D);            /* Compute ranks  */
  RD=(RD-.5)/N;
PD2=P/2;
  Q=2*GAMINV(RD,PD2);    /* Plotting positions */
  DQ=D||Q;
  print DQ;
CREATE CHISQ FROM DQ ;   /* Open a file to store results */
APPEND FROM DQ;

                         /* Compute test statistic */
rpn = t(D)*Q - (sum(D)*sum(Q))/N;
rpn = rpn/sqrt((ssq(D)-(sum(D)**2)/N)
         *(ssq(Q)-(sum(Q)**2)/N));

/* Simultate a p-value for the correlation test  */

ns=10000;
pvalue=0;
do i=1 to ns;
  do j1=1 to n;
  do j2=1 to p;
    x[j1,j2] = rannor(-100);
    end;
    end;

   SUMX=X[+, ];           
   A=t(X)*X-t(SUMX)*SUMX/N;     
   S=A/(N-1);             
   XBAR=SUMX/N;           
   E=X-(J(N,1)*XBAR);   
   DN=VECDIAG(E*INV(S)*t(E));   
   RN = RANK(DN);            
   RN =(RN-.5)/N;
   PD2=P/2;
   QN=2*GAMINV(RN,PD2);  
   rpnn = t(DN)*QN-(sum(DN)*sum(QN))/N;
   rpnn = rpnn/sqrt((ssq(DN)-(sum(DN)**2)/N)
           *(ssq(QN)-(sum(QN)**2)/N));
   if(rpn>rpnn) then pvalue=pvalue+1;   
   end;
   pvalue=pvalue/ns;

print,,,,,"Correlation Test of Normality";
print N P rpn pvalue;

FINISH;

RUN NORMAL;


/* Display the chi-square probability plot 
   for assessing multivariate normality */

axis2 label=(f=swiss h=2.5 a=90 r=0
       "Ordered Distances ")
      value=(f=triplex h=2.0)
      length = 5.0in;

axis1 label=(f=swiss h=2.5 
      "Chi-Square Quantiles")
      value=(f=triplex h=1.6)
      length= 5.5in;
 
 SYMBOL1 V=CIRCLE H=2.0 ;
 
PROC GPLOT DATA=CHISQ;
   	PLOT Col1*COL2 / vaxis=axis2 haxis=axis1;
	TITLE1 ls=1.5in H=3.0 F=swiss "CHI-SQUARE PLOT";
	Title2 H=2.3 F=swiss "Board Stiffness Data";
	footnote ls=1.0in;
RUN;


