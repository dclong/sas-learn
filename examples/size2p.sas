
/* This program computes sample sizes
   needed to obtain tests with a
   specified power values for detecting
   specified differences between two 
   proportions. This program is stored 
   in the file     size2p.sas      */

options nodate nonumber linesize=68;

proc iml;
  start samples;

/* Enter the probability of success
   for the first population  */

  p1 = .58;                      

/* Enter the probability of success
   for the second population */

  p2 = .50; 
 
* Enter a set of power values;

  power = {.8 .9 .95 .99};      

* Enter the type I error level;

  alpha = .05;                  


* Compute sample sizes;

  za = probit(1-alpha/2);
  za1 = probit(1-alpha);
  nb = ncol(power);
  np = ncol(power);
  size = j(1,np);
  size1 = j(1,np);

  p = (p1+p2)/2;
  rp = sqrt(2*p*(1-p)/
        (p1*(1-p1)+p2*(1-p2)));
  

*  Cycle across power levels;

  do i2 = 1 to np;              
   zb = probit(power[1,i2]);
   size[1,i2] = ((za*rp+zb)**2)*
    (p1*(1-p1)+p2*(1-p2))/((p1-p2)**2);
   size1[1,i2] = ((za1*rp+zb)**2)*
    (p1*(1-p1)+p2*(1-p2))/((p1-p2)**2);
  end;

  print,,,,,,, 'Sample sizes for testing equality',
       '  of two proportions';
  print, 'The number of observations needed',
         '  for each treatment group';
  print,,, p1 p2 alpha power;
  sizer = int(size) + j(1,np);
  print 'Sample sizes (2-sided test):' sizer;

  size1r = int(size1) + j(1,np);
  print 'Sample sizes (1-sided test):' size1r;

/*  Compute corrected sample sizes using
    the Fleiss (1981) correction  */

  sizec=(size/4)#(j(1,np)+sqrt(j(1,np)
           +4/(size*abs(p1-p2))))##2;

  sizerc = int(sizec) + j(1,np);
  print 'Corrected sizes (2-sided test):' sizerc;

  size1c=(size1/4)#(j(1,np)+sqrt(j(1,np)
           +4/(size1*abs(p1-p2))))##2;

  size1rc = int(size1c) + j(1,np);
  print 'Corrected sizes (1-sided test):' size1rc;


/*  Determine sample sizes for 
    constructing confidence intervals */

/*  Enter confidence level  */

  level=0.95;
  percent = level*100;

/*  Enter values for margin of error */

   me = {0.04, 0.03, 0.02, 0.01};

   alpha = 1-level;
   za = probit(1-alpha/2);
   np = ncol(me);
   size = j(1,np);

  /*  Compute sample sizes  */

   n = ((za/me)##2)#(p1*(1-p1)+p2*(1-p2));
   n = int(n) + j(np,1);

   percent = level*100;

   print,,,,,,'Sample sizes for' percent 'percent',
        '   confidence intervals';
   print,,'The number of observations for each',
          '  treatment group';
   print,,,'       Half length       Sample size';
   print me '     ' n;


finish;
run samples;
