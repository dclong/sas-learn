
options formdlim = '-';

data wool;
   	infile 'wool.txt' missover;
	input bale @;
  	if bale < 3 then 
		farm = 'SE';
    else 
		if bale < 6 then 
			farm = 'SC';
    	else 
			farm = 'C';
		*endif;
	*endif;
  	do i =  1 to 4;
    	input clean @;
    	if clean ne . then output;
    end;
run;
 
data wool2;
  	set wool;
  	if clean  < 62;
run;         
    
proc varcomp method = type1;
  	class bale farm;
  	model clean = farm bale(farm);
run;
 
proc mixed method = type3;
  	class farm bale;
  	model clean =  ;
  	random farm bale(farm);
  	title 'Tests of variance components';
run;
        
   
  
