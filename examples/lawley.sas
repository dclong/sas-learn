

/*  This program is posted as  Lawley.sas.  It performs
    factor analyses on the test score data from section
    9.4 in Johnson & Wichern.  The data are posted as
    Lawley.dat.  The variables are test socres for 
        x1 = Gaelic         x2 = English
        x3 = History        x4 = Aritmetic
        x5 = Algebra        x6 = Geometry     */

options nonumber nodate linesize=64;

data set1(TYPE=CORR);
  	infile "c:\stat501\data\Lawley.dat";
  	input x1-x6;
  	_TYPE_ ='CORR';
    IF (_N_ = 1) THEN _NAME_='X1';
    IF (_N_ = 2) THEN _NAME_='X2';
    IF (_N_ = 3) THEN _NAME_='X3';
    IF (_N_ = 4) THEN _NAME_='X4';
    IF (_N_ = 5) THEN _NAME_='X5';
    IF (_N_ = 6) THEN _NAME_='X6';
	IF (_N_ = 7) THEN DO; _NAME_='  '; _TYPE_='N   '; END;
run;

/* Print the data file  */

proc print data=set1;
run;
title 'FACTOR ANALYSIS OF TEST SCORES';


/*  Use maximum likelihood estimation */

proc factor data=set1(TYPE=CORR) method=ml nfactors=2 
   	res msa  maxiter=75 rotate=varimax outstat=fac2;
   	var x1-x6;
   	priors smc;
run;

/*  Try a PROMAX rotation */

proc factor data=fac2 nfactors=2 rotate=promax 
    reorder score ;
  	var x1-x6;
  	priors smc;
run;


/*  Try a three factor solution  */

proc factor data=set1(TYPE=CORR) method=ml nfactors=3 res msa
   	maxiter=75 rotate=varimax reorder;
  	var x1-x6;
  	priors smc;
run;




