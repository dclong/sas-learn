*****************************Copyright @ Chuanlonng Du********************************************;
/*
Functions:
This program is used to get the eigenvalues and eigenvectors of covariance & correlation matrix.
Comment:
It's just for fun and not very useful since we can use factor or princomp procedure to do it
*/
proc iml;
*******************Body Part of the Function*******************************************************;
*A covariance matrix is needed;
      x={3.397 -1.102   4.306  -2.078  0.027,
        -1.102  9.673  -1.513  10.953  1.203,
         4.306 -1.513  55.626 -28.937 -0.044,
        -2.078 10.953 -28.937  89.067  0.957,
         0.027  1.203  -0.044   0.957  0.319};
**************************************************************************************************;
	  title f=arial c=red h=6 "Eigen values and vectors of covariance & correlation matrix";
	  title2 f=arial c=blue h=5 "The covariance matrix is";
	  print x;
	  p=nrow(x);*the dimension of x;
	  values=j(1,p,0);
	  vectors=j(p,p,0);
	  call eigen(values,vectors,x);
	  print "Eigen values and vectors of the covariance matrix:";
	  print values;
	  print vectors;
	  variance=vecdiag(x);
	  std=sqrt(variance);
	  print std;
	  y=shape(std,p,p);
	  corr=x/y;
	  corr=corr/y`;
	  print "The correlation matrix is as follows:";
	  print(corr);
	  call eigen(values,vectors,corr);
	  print "Eigen values and vectors of the corrlation matrix:";
	  print values;
	  print vectors;  
quit;
