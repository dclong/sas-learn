
proc glm data=set2;
     class subject group task cue;
     model x = group subject(group) task 
            task*group task*subject(group)
            cue cue*group cue*subject(group) 
            task*cue group*task*cue;
    test h=group  e=subject(group);
    test h=task task*group  e=task*subject(group);
    test h=cue cue*group  e=cue*subject(group);
run;

proc glm data=set1;
     class group;
     model x1-x6 = group;
     manova h=group;                              /* part A */
     manova h=group m=x1+x2+x3-x4-x5-x6;          /* part E */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;   /* part F */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;   /* part G */
run;

proc glm data=set1;
     class group;
     model x1-x6 = group / noint;
     manova h=group;
     manova h=group m=x1+x2+x3-x4-x5-x6;           /* part B */
     manova h=group m=x1-x3+x4-x6, x2-x3+x5-x6;    /* part C */
     manova h=group m=x1-x3-x4+x6, x2-x3-x5+x6;    /* part D */
run;

PROC GLM DATA=set1;
     CLASS location variety;
     MODEL x1-x3 = location variety location*variety / P SOLUTION;
     MANOVA H=location*variety /PRINTH PRINTE;
     MANOVA H=variety / printH printE;
     MANOVA H=location / printH printE;
     Repeated traits 3 profile / printm;
run;

proc glm data=set1;
     model x1-x6 = z1 z2 / noint;                /* part H */
     manova h=z1 m=x1-x3-x4+x6, x2-x3-x5+x6;
run;

proc glm data=owls;
  	class type;
  	model km91 km118 km140 km160 km177 km241 km338 = type;
  	manova h=type;
run;

PROC GLM DATA=SET1;
     CLASS TEMP;
     MODEL X1 X2 = TEMP / P SOLUTION;
     MANOVA H=TEMP /PRINTH PRINTE;
     LSMEANS TEMP /PDIFF STDERR;
run;

PROC GLM DATA=set1;
     MODEL d1 d2 d3 = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
Run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=cs subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=un subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=cs subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set2;
     class group test subject;
     model y = group test group*test / ddfm=kr;
     random subject / subject=subject(group);
     lsmeans group*test / diff ;
run;

proc mixed data=set3;
     class treat dog;
     model x = treat / solution;
     repeated / type=un subject=dog r=1;
     lsmeans treat / adjust=bon cl;
run;

proc mixed data=set2;
     class task cue group subject;
	 model x=task cue group / solution;
	 repeated/type=vc subject=subject r=1;
	 lsmeans task cue group /adjust=bon cl;
run;

proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-East, North-South, 
            North-West  prefix=c /printe ;
run;

proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-South, East-West, 
            North+South-East-West  prefix=c /printe ;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

PROC GLM DATA=set1;
     MODEL dBOD dSS = Z / NOINT SOLUTION;
     MANOVA H=Z / PRINTE;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
	 random tree;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

proc glm data=set1;
     model North East South West = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

proc glm data=set1;
     model x1-x4 = z /noint nouni;
     manova H=z M=x1-x2+x3-x4, -x1-x2+x3+x4, 
          -x1+x2+x3-x4 prefix=c /printe ;
run;

PROC GLM DATA=set1;
     CLASS group;
     MODEL x1-x4 = group / P SOLUTION;
     MANOVA H=group /PRINTH PRINTE;
     Manova H=group M= (1 0 0 -1, 0 1 0 -1, 0 0 1 -1) prefix=diff / printh printe;
     Repeated test 4 profile / printm printh printe;
run;

proc glm data=set3;
     class treat dog;
     model x = dog treat / solution;
     random dog;
     means treat / bon tukey ;
run;

proc glm data=set1;
     model x1-x4 = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;

proc logistic data=set1;
  	model RESULT = AGE SEX E1-E3 EMOTION ETREAT L1-L4 ATREAT ALCADD
        	HEALTH FINANCE M1-M3 PDRINK SIBS WORK WAGES JOBS DAGE
        		DFREQ STOP DRY DRUGS/ sle=.25 sls=.25 selection=s details
                   				maxiter=50 converge=.0001 covb itprint;
run;


proc logistic data=set1 nosimple covout outest=setp1;
  	model result =  ETREAT ATREAT E3 WORK/ itprint covb corrb ctable pprob=.4
                   maxiter=50 selection=none converge=.0001;
run;

/*  Use backward elimination to select a good subset of variables */

proc logistic data=set1;
  	model RESULT = AGE EMOTION ETREAT L1-L4 ATREAT
        	HEALTH  M1-M3 PDRINK SIBS WORK WAGES DAGE
        		DFREQ STOP DRUGS/  sls=.05 selection=b details
                   	maxiter=50 converge=.0001 covb itprint;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE EMOTION ETREAT L2 ATREAT HEALTH PDRINK WAGES
                 DFREQ  /  covb corrb ctable pprob=.5
                         		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = E3 ETREAT ATREAT HEALTH WORK
                /  covb corrb ctable pprob=.5
                   		maxiter=50 selection=none converge=.0001;
run;

proc logistic data=set1 nosimple covout outest=setp1;
  	model RESULT = AGE ATREAT/  covb corrb ctable pprob=.5
                   maxiter=50 selection=none converge=.0001;
run;
