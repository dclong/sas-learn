/*  This program is posted as  grain.sas.  It reads the
    data posted in the file grain.dat, fits a linear
    disrciminant model, and classifies 12 cases that
    are not in the training samples. */
filename grain url "http://streaming.stat.iastate.edu/~stat501/data/grain.dat";

DATA grain;
     INFILE grain;
     INPUT ID  TYPE X1-X11;
RUN;

PROC FORMAT;
     VALUE T 0='Insolvent'
             1='Solvent'; 
RUN;

PROC SORT DATA=grain; 
     BY TYPE;
RUN;

proc univariate data=grain normal; 
     by type;
	 var x1-x11;
	 title f=arial c=red h=4 "Test for normality";
run;

PROC PRINT DATA=grain N UNIFORM;
     VAR TYPE X1-X11;
RUN;

proc discrim data=grain pool=test anova crosslist manova method=normal 
              posterr simple slpool=0.01 outstat=dis1;
	 class type;
	 var x1-x11;
	 format type t.;
run;

proc boxplot data=grain;
     plot (x1-x11)*type/boxstyle=schematic;
run;

/*  Force the program to do linear discriminant analysis  */

PROC DISCRIM DATA=grain POOL=YES CROSSVALIDATE METHOD=NORMAL outstat=model1;
     CLASS TYPE;
     VAR X1-X11;
     FORMAT TYPE T.;
run;

proc print data=model1;
     title f=arial c=red h=4 "Fisher's Linear Discrimination Rule:";
run;

proc stepdisc data=grain sw sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain backward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

proc stepdisc data=grain forward sle=0.25 sls=0.25;
     class type;
	 var x1-x11;
	 title f=arial c=red h=4 "Variable slection using stepwise method";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=test CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x6 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

PROC DISCRIM DATA=grain POOL=yes CROSSVALIDATE METHOD=NORMAL outstat=model2;
     CLASS TYPE;
     VAR X1 x3 x4 x7 x10;
     FORMAT TYPE T.;
	 title f=arial c=red h=4 "Discrimination analysis using slected variables";
run;

data grain1;
     set grain;
	 where type=0;
	 keep x1-x11;
run;

proc print data=grain1;
run;

data grain2;
     set grain;
	 where type=1;
	 keep x1-x11;
run;

proc print data=grain2;
run;
