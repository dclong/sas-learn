************************************Chuanlong Du****************************************;
/*
Functions:
*This program test the null hypothesis that the correlation matrix is compound symmetric;
Parameters:
Parameter corr is the correlation matrix (covariance) matrix
Parameter n is the number of observations
*/
proc iml;
*******************Parameters Part******************************************************;
     use corr;
	 read all into corr;
	 n=33;*numbers of observations;
******************Body Part of Functions************************************************;
	 p=nrow(corr);
	 SumVector=corr[+,];
	 MeanVector=(SumVector-1)/(p-1);
	 OverallMean=(sum(corr)-p)/p/(p-1);
	 GammaHat=(p-1)##2*(1-(1-OverallMean)##2)/(p-(p-2)*(1-OverallMean)##2);
	 ChisqStat=(sum((corr-OverallMean)##2)-p*(1-OverallMean)##2)/2;
	 ChisqStat=ChisqStat-GammaHat*sum((MeanVector-OverallMean)##2);
	 ChisqStat=(n-1)/(1-OverallMean)##2*ChisqStat;
	 df=(p+1)*(p-2)/2;
	 Pvalue=1-probchi(ChisqStat,df);
	 title f=arial c=red h=6 "Test Compound Symmetric Structure of Correlation (Covariance) Matrix";
	 print "The value of the approximate chisquare statistic and its p-value are";
	 print ChisqStat df Pvalue;
quit;
