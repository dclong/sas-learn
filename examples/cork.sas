
/*  This program analyzes the cork data
    posted in the file    cork.dat    */
filename cork url "http://streaming.stat.iastate.edu/~stat501/data/cork.dat";

data set1;
     infile cork;
     input  tree North East South West;
     Z = 1;
run;

proc print data=set1; 
run;

proc univariate data=set1 normal;
     var North East South West;
run;

data set2;
     set set1;
	 keep north east south west;
run;

proc print data=set2;
run;
/* Test the null hypothesis that the 
   cork deposities are the same mean 
   thickness on all side of the trees  */
proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-East, North-South, 
            North-West  prefix=c /printe ;
run;

proc glm data=set1;
     model North East South West = z /noint;
     manova H=z M=North-South, East-West, 
            North+South-East-West  prefix=c /printe ;
run;


/* Do a mixed model analysis of variance.
   Restructure the data file to put each
   observation on a different line  */

data set3; 
     set set1;
     array A(I) North East South West;
     do over A;
        Y = A;
        side = I;
        output;
     end;
     keep tree side Y;
run;

proc print data=set3;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;

proc glm data=set3;
     class side tree;
     model Y = tree side / solution;
	 random tree;
     means side  / bon tukey;
     lsmeans side / stderr pdiff tdiff;
run;


/* Use the REPEATED option in PROC GLM 
   to do both analyses */

proc glm data=set1;
     model North East South West = z / noint nouni;
     repeated  x 4 contrast / printm 
                printe short summary;
run;
