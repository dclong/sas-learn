

/*  This program analyzes the thread winding speed
    data from problem 17 at the end of Chapter 18
    in KNNL (page 805). The data are posted as 
    ch18pr17.dat in the data folder of the course web
    page.  The program is posted as ch18pr17.sas
    in the SAS folder of the course web page.   */


/*  First enter the data  */

data set1;
	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data/ch18pr17.dat';
  	input breaks speed order;
run;


/*  Print the data  */

proc print data=set1;
run;


/*  Compute summary statistics and box plots */

proc sort data=set1;
	by speed; 
run;

proc boxplot data=set1;
	plot breaks*speed / boxstyle=schematic;
  	title 'Breaks for Different Thread Winding Speeds';
run;


proc univariate data=set1 normal;
	by speed; 
	var breaks;
run;


/*  Compute the Kruskal-Wallis test   */

proc npar1way data=set1 wilcoxon;
	class speed; 
	var breaks;
run;


/* Plot the log(std) against the log(mean) */

proc means data=set1; 
	by speed;
    var breaks;
    output out=set4 mean=mean std=std;
run;

data set4; 
	set set4;
    lmean=log(mean);
    lstd = log(std);
run;


goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set4;
      plot lstd*lmean / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Plot to Determine Power Transformation';
      label  lstd = 'Log(Standard Deviation)';
      label  lmean = 'Log(Mean)';
   run;


/*  Compute the ANOVA table, apply the Tukey HSD
    procedure to determine which agents differ
    with respect to mean transaction time.  Also
    list the means and compute t-tests for all
    possible pairs of means.  Output residuals
    to a file.  After viewing the previous results
    you may want to transform the response variable
    before running this step  */

proc glm data=set1;
	class speed;
  	model breaks = speed / p clm;
  	lsmeans speed / stderr pdiff tdiff;
  	means speed / tukey scheffe snk;
  	output out=set2 residual=r predicted=mean;
run;



/*  Construct a normal probability plot for the residuals
    and compute the Shapiro-Wilk test for normaility */

proc rank data=set2 normal=blom out=set2;
	var r; 
	ranks q;
run;

proc univariate data=set2 normal;
	var r;
run;

goptions targetdevice=ps rotate=landscape;

  axis1 label=(f=swiss a=90 h=2.3)
      order = -10 to 10 by 5
      value=(f=swiss h=2.3)
      length= 5 in;

  axis2 label=(f=swiss h=2.3)
      value=(f=swiss h=2.3)
      order = -3.0 to 3.0 by 1
      length = 5 in;

  symbol v=circle h=2 I=none w=2;

proc gplot data=set2;
      plot r*q / vaxis=axis1 haxis=axis2;
      title1  h=3. F=swiss 'Normal Probability Plot';
      label  r = 'Ordered Residuals';
      label  q = 'Standard Normal Quantiles';

   run;


   
/* Compute the Brown-Forsythe Test */

proc means data=set1 mean stddev median; 
	by speed;
    var breaks; 
    output out=means mean=mean stddev=s median=median;
run;

proc print data=means; 
run;

data set5; 
	merge set1 means; 
 	by speed; 
    d=abs(breaks-median);
run;

proc glm data=set5;
	class speed;
  	model d = speed;
run;
