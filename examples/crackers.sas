
options linesize=72 nocenter ;

/* Program to perform analysis of covariance,
   applied to cracker sales data from KNNL,  
   Section 22.3.  This code is posted as
   crackers.sas      */

data set1;
	input store treatment y x;
  	datalines;
  1 1 38 21
  2 1 39 26
  3 1 36 22
  4 1 45 28
  5 1 33 19
  6 2 43 34
  7 2 38 26
  8 2 38 29
  9 2 27 18
  10 2 34 25
  11 3 24 23
  12 3 32 29
  13 3 31 30
  14 3 21 16
  15 3 28 29
run;

proc print data=set1;
run;

/*  Fit the usual covariance model with
    common slope (parallel lines)  */

proc glm data=set1;
	class treatment;
  	model y = x treatment / p clm solution;
  	output out=setr residual=resid predicted=yhat;
  	lsmeans treatment / stderr pdiff tdiff;
run;


/*  Plot the estimated regression lines  */

data setr1; 
	set setr; 
  	if(treatment=1);
    yhat1=yhat; 
	y1=y; 
	x1=x;  
run;

data setr2; 
	set setr; 
    if(treatment=2);
    yhat2=yhat; 
	y2=y;
	x2=x;
run;

data setr3; 
	set setr; 
    if(treatment=3);
    yhat3=yhat; 
	y3=y;
	x3=x;
run;

data setrall; 
	merge setr1 setr2 setr3;  
run;



goptions targetdevice=ps rotate=landscape colors=(black) ;

axis1 label=(f=swiss h=2.5)  ORDER = 15 to 35 by 5
      value=(f=swiss h=2.0)  w=3.0  length= 7.5 in;

axis2 label=(f=swiss h=2.0 r=0 a=90)  order = 15 to 50 by 5
      value=(f=swiss h=2.0) w= 3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 w=3 i=none ;
       SYMBOL2 V=diamond H=2.0 w=3 i=none ;
       SYMBOL3 V=square H=2.0 w=3 i=none ;
	   SYMBOL4 V=none H=2.0 w=3 l=1 i=spline ;
       SYMBOL5 V=none H=2.0 w=3 l=1 i=spline ;
       SYMBOL6 V=none H=2.0 w=3 l=1 i=spline ;

 PROC GPLOT DATA=setrall;
    PLOT y1*x1 y2*x2 y3*x3 yhat1*x1 yhat2*x2 yhat3*x3 / overlay vaxis=axis2 haxis=axis1;
    TITLE1  H=3.0 F=swiss "Cracker Sales (cases)";
       LABEL y1='Post-promotion Sales';
       LABEL x1 = 'Pre-promotion Sales';
   RUN;

       

/* Create residual plots  */

proc rank data=setr normal=blom out=setr;
	var resid;
	ranks q;
run;

proc univariate data=setr normal;
	var resid;
run;

data setr; 
	set setr;
  	zero = 0;
run;

 /*  Create some high quality residual plots  */

axis1 label=(f=swiss h=2.5 )  ORDER = 15 to 45 by 5
      value=(f=swiss h=2.0)  w=3  length= 7.5 in;

axis2 label=(f=swiss h=2.2 r=0 a=90)  order = -3 to 3 by 1
      value=(f=swiss h=2.0)  w=3  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;
       SYMBOL2 V=none H=3.0 w=2 l=1 i=spline;

PROC GPLOT DATA=SETR;
      PLOT resid*yhat zero*yhat / overlay vaxis=axis2 haxis=axis1;
  TITLE1  H=3.0 F=swiss "Residuals";
       LABEL yhat='Estimated Means';
       LABEL resid = 'Residuals';
   RUN;

axis1 label=(f=swiss h=2.5 ) ORDER = -3 to 3 by 1
      value=(f=swiss h=2.0) w=3.0  length= 7.5 in;

axis2 label=(f=swiss h=2.2 r=0 a=90)  order = -3 to 3 by 1
      value=(f=swiss h=2.0)  w=3.0  length = 5.5 in;

       SYMBOL1 V=CIRCLE H=2.0 i=none ;
       SYMBOL2 V=none H=2.0 w=3.0 l=1 i=spline ;

   PROC GPLOT DATA=SETR;
      PLOT resid*q  /  vaxis=axis2 haxis=axis1;
TITLE1  H=3.0 F=swiss "Normal probability plot";
       LABEL q='Normal Scores';
       LABEL resid = 'Residuals';
   RUN;










