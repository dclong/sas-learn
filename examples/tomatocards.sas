/* the following options line saves paper - */
/*     SAS puts information together to print fewer pages */

options formdlim = '-';

/* following data step reads the datafile and creates a SAS data set */
/* datalines; is an alternative to cards; */
/* N.B. the cards or datalines statements MUST be the last thing */
/*   in the data step */
/* put any programming statements */
/*   (e.g. a log transformation, see sparrow.sas)
/*   BEFORE the cards statement */
/* the data ends with the first line with a ; on it */
/*  this may be the start of a proc or new data step */
/*  e.g. proc print; */
/*  or (more frequently) it is a line with just a ; */
 
data tomato;
 	input group yield;
	cards;
group yield
1 29.9
1 11.4
2 26.6
2 23.7
1 25.3
2 28.5
2 14.2
2 17.9
1 16.5
1 21.1
2 24.3
run;

/* you can print out a data set using proc print */

proc print;
  	title 'Tomato yield data set ';
run;
          
/* There are three (at least) ways to calculate summary statistics */
/*   (or do any other analysis) for just observations in group 1 */

/* The first method, using a subsetting IF command to create a data set */
/*    that contains only observations from group 1. */
/* The data set called tomato2 contains only the observations from group 1 */
/* the tomato dataset is unchanged */

data tomato2;
  	set tomato;
  	if group =  1;
run;

proc univariate data = tomato2;
  	var yield;
  	title 'Tomato yield summary for observations in group 1';
run;
     
/* The second method, using a WHERE command to extract the subset */
/*   'on the fly' */

proc univariate data = tomato;
  	where group =  1;
  	var yield;
  	title 'Tomato yield summary for observations in group 1';
run;

/* The third method, using a BY command to analyze both groups separately */
/*   this method requires that the data by sorted first, so all group 1 */
/*    observations are together, followed by all group 2 observations.
/* You can sort the data using proc sort */

proc sort data = tomato;
  	by group;
run;
      
proc univariate;
  	by group;
  	var yield;
  	title 'Summary statistics for both groups';
run;
     
/* High resolution plots, which work best in windowing SAS */

/* High resolution histograms */
proc univariate;
  	by group;
  	histogram yield;
  	title 'High resolution histograms for each group';
run;
       
/* High resolution box plots */
/* the response variable comes first, 
   then the variable defining the groups  */
   
proc boxplot;
  	plot yield*group;
  	title 'High resolution boxplots for each group';
run;
     
/* High resolution scatter plots */
/* The first variable goes on the Y axis; the second goes on the X axis */
/*   So, plot yield*group puts yield on the Y axis and group on the X */
proc gplot;
  	plot yield*group;
  	title 'High resolution scatter plot';
run;
              
          
/* the following draws a low resolution histogram of all observations */
proc chart;
  	vbar yield;
  	title 'Histogram of tomato yields';
run;

/* PROC UNIVARIATE with the plot option draws low resolution box plots */
/*     and stem and leaf plots */
     
proc univariate plot;
  	var yield;
  	title 'Tomato yield summary for all observations';
run;

/* Low resolution scatter plots are produced by proc plot; */
/* The first variable goes on the Y axis; the second goes on the X axis */
/*   So, plot yield*group puts yield on the Y axis and group on the X */
/* You can have many plot commands in one proc plot. */
/*   Each plot command produces one plot */

proc plot data = tomato;
  	plot yield*group;
  	title 'Plot of yield vs. treatment group';
run;
     



