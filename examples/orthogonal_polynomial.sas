*This program is used check use of function orpol();
/*There are still something not very clear*/
proc iml;
    t1=1:4;
	t2={1 3 4 5};
	weight={2 2 3 3};
	print t1 t2 weight;
	result1=orpol(t1,3);
	result2=orpol(t1,3,weight);
	result3=orpol(t2,3);
	result4=orpol(t2,3,weight);
	print "The 4 contrasts are as follows:";
	print result1 result2 result3 result4;
	print "Check contrast 2:";
	x=weight*result2;
	print x;
	print "Check contrast 3";
	ones={1 1 1 1};
	x=ones*result3;
	print x;
	y=t2-13/4;
	print y;
	y=result2[,2]/y`;
	print y;
    

	a      = {1,4,6,10}; /* spacing */ 
   trials = {7,2,3,4};  /* sample sizes */ 
   maxDegree = 3; /* model with Intercept,a,a##2,a##3 */ 
 
   P = orpol(a,maxDegree,trials); 
   print p;
run;
