
/*  This program is posted as turtlef.prg
    It performs a principal component 
    analysis of carapace measurements for 
    female turtles.  The data are contained
    in the program.                                          */

options  nodate nonumber linesize=64 nocenter;

data females;
   	input id $ length width height;
    length=log(length);
    width=log(width);
    height=log(height);
	datalines;
A 98 81 38
B 103 84 38
C 103 86 42
D 105 86 42
E 109 88 44
F 123 92 50
G 123 95 46
H 133 99 51
I 133 102 51
J 133 102 51
K 134 100 48
L 136 102 49
M 138 98 51
N 138 99 51
O 141 105 53
P 147 108 57
Q 149 107 55
R 153 107 56
S 155 115 63
T 155 117 60
U 158 115 62
V 159 118 63
W 162 124 61
X 177 132 67
run;

proc print data=females;
run;


/* Compute estimates of principal 
   components from the sample covariance
   matrix and plot the scores    */

proc princomp data=females cov n=3 out=scores prefix=prin;
     var length width height;
run;

proc print data=scores; 
run;

proc plot data=scores ;
     plot prin1*prin2 / hpos=56;
     plot prin1*(length width height)=id / hpos=56;
run;


proc corr data=scores;
     var prin1 prin2 prin3 length width height;
run;


/*  Compute estimates of principal 
    components from the sample correlation 
    matrix and plot the scores      */
proc princomp data=females out=pcorr prefix=pcorr;
     var length width height;
run;

proc print data=pcorr; 
run;

proc plot data=pcorr;
     plot pcorr1*pcorr2=id / hpos=56;
run;
