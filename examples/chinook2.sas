

/*  This program is posted as
        chinook2.sas        */

options nodate nonumber center linesize=64;

data set1;
	infile 'C:\Documents and Settings\dclong\My Documents\SAS Study\Fall 2008\SAS Code\Data\hdata.dat';
  	input (year month day biweek run gear age sexa length)
        (4. 2. 2. 1. 1. 1. 2. $1. 4.);
    if(sexa = 'F') then 
		sex=1; 
	else 
		sex=2;
run;

/*  Attach labels to categories  */

proc format;
	value run 1 = 'Early'
           	  2 = 'Late';
  	value sex 1 = 'Female'
              2 = 'Male';
  	value gear 1 = 'Hook'
               2 = 'Net';
run;


/*  Examine partial association between
    sex and method of capture within
    each run.  */

proc sort data=set1; 
	by run;
run;

proc freq data=set1; 
	by run;
    table sex*gear / chisq cmh nopercent nocol expected;
    format sex sex. gear gear. run run.;
run;




