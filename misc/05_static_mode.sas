
/* 
Create a temp table containing the variables whose most common value are to be figured out.
*/
proc sql;
    drop table tempDir.var_st
    ;
    create table tempDir.var_st (
        num num, 
        var_st char(255), 
        data_type char(255)
    )
    ;
    insert into tempDir.var_st
    set 
        num=1, 
        var_st='Property', 
        data_type='char(255)'	
    set 
        num=2, 
        var_st='Occupancy', 
        data_type='char(255)'	
    set 
        num=3, 
        var_st='Purpose', 
        data_type='char(255)'	
    set 
        num=4, 
        var_st='RateType', 
        data_type='char(255)'	
    set 
        num=5, 
        var_st='FixedRatePeriod', 
        data_type='char(255)'	
    set 
        num=6, 
        var_st='Documentation', 
        data_type='char(255)'	
    set 
        num=7, 
        var_st='OriginalAppraisalAmount', 
        data_type='char(255)'	
    set 
        num=8, 
        var_st='LTV', 
        data_type='char(255)'	
    set 
        num=9, 
        var_st='JrLTV', 
        data_type='char(255)'	
    set 
        num=10, 
        var_st='CombinedLTV', 
        data_type='char(255)'	
    set 
        num=11, 
        var_st='OriginalAmount', 
        data_type='char(255)'	
    set 
        num=12, 
        var_st='Srbalance', 
        data_type='char(255)'	
    set 
        num=13, 
        var_st='OriginalTerm', 
        data_type='char(255)'	
    set 
        num=14, 
        var_st='AmorTerm', 
        data_type='char(255)'	
    set 
        num=15, 
        var_st='IOTerm', 
        data_type='char(255)'	
    set 
        num=16, 
        var_st='State', 
        data_type='char(255)'	
    set 
        num=17, 
        var_st='Zip', 
        data_type='char(255)' 	
    set 
        num=18, 
        var_st='FICO', 
        data_type='char(255)'	
    set 
        num=19, 
        var_st='ORIG_INT_RT', 
        data_type='char(255)' 
    set 
        num=20, 
        var_st='OriginationDate', 
        data_type='char(255)' 
    set 
        num=21, 
        var_st='PrepayPenaltyTerm', 
        data_type='char(255)' 
    set 
        num=22, 
        var_st='MaxDrawnAmount', 
        data_type='char(255)' 
    set 
        num=23, 
        var_st='DrawdownTerm', 
        data_type='char(255)' 
    set 
        num=24, 
        var_st='DTI', 
        data_type='char(255)' 
    set 
        num=25, 
        var_st='IsModified', 
        data_type='char(255)' 
    set 
        num=26, 
        var_st='IsBroker', 
        data_type='char(255)' 
    set 
        num=27, 
        var_st='IsHeloc', 
        data_type='char(255)' 
    set 
        num=28, 
        var_st='IsHarris', 
        data_type='char(255)' 
    set 
        num=29, 
        var_st='cntrct_cd', 
        data_type='char(255)' 
    ;
quit;

/* Assign the most common non-zero, non-missing or 1st observation value through loop */
%macro most_common_value;
    proc sql noPrint;
        select count(*) into :n from tempDir.var_st
        ;
        select var_st into :v_1 - :v_%left(&n) from tempDir.var_st
        ;
    quit;
    %do i = 1 %to &n. %by 1;
        proc sql noPrint;
            drop table tempDir.summary
            ;
			drop table tempDir.summary_2
            ;
            create table tempDir.summary as
            select  
                loanID,
                &&v_&i as var_st, 
                count(*) as cnt
            from 
                tempDir.raw_all_temp
            where 
                &&v_&i is not null
				and lowcase(strip(reportedLoanStatus)) not in ('defaulted', 'write off', 'closed')
				and isPrepaid ^= 1
            group by 
                loanID, var_st
            order by
                loanID, cnt, var_st
            ;
		quit;
		data tempDir.summary_2;
			set tempDir.summary;
			obs_id = _n_;
		run;
		proc sql;	
            drop table tempDir.summary
            ;
			drop table tempDir.t1
            ;
            create table tempDir.t1 as
            select 
                loanID, 
                var_st
            from 
                tempDir.summary_2
            group by 
                loanId
            having 
                obs_id = max(obs_id)
            ;
            drop table tempDir.st_master_2
            ;
            create table tempDir.st_master_2 as
            select 
                A.*,
                B.var_st as &&v_&i
            from 
                tempDir.st_master A
            left join 
                tempDir.t1 B 
            on 
                A.loanID = B.loanID
            ;
        quit;
        proc datasets library=tempDir noList;
            delete st_master;
            change st_master_2=st_master;
        run;
    %end;
%mend;

%most_common_value;

/* 
The tempDir.raw_all_temp table is no long used after this block of code.
*/
