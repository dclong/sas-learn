

/* 
The follosing code use stratified sampleing (w.r.t snapshot month)
to patition the data into training and testing (about 25%) datasets.
*/

/* use the 10000th prime as RNG seed */
%let seed = 104729; 

/*
A sample with 12236 observations is generated.
*/
proc SurveySelect data=lgd.lgd_all_variables_dfast out=lgd.lgd_testing_dfast
	method=srs
	rep=1 
    sampRate=0.25
	seed=&seed.
	;
    strata snapshot_month;
  	id _all_;
run;

proc sql;
	create table lgd.lgd_training_dfast as
	select
		A.*
	from 
		lgd.lgd_all_variables_dfast as A
	left join
		lgd.lgd_testing_dfast as T
	on
		A.obs_id = T.obs_id
	where	
		T.obs_id is null
	;
quit;
