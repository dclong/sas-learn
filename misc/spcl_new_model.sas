
libname spcl "~/projects/ia_lgd_loan_level/lib/spcl";

%macro calculate_spcl(pd_lgd=, formation_output=, spcl_output=);
    proc iml;
        /* include SAS IML modules */
        %include "~/sas/iml/spcl.sas";
        /*-----------------------------------------------------------------------------------------------------------------*/
        use &pd_lgd;
        read all var {"ead"} into ead0;
        read all var {"d1"} into d1;
        read all var {"d2"} into d2;
        read all var {"pd_1" "pd_2" "pd_3" "pd_4" "pd_5" "pd_6" "pd_7" "pd_8" "pd_9" "pd_10"} into pd;
        read all var {"lgd_1" "lgd_2" "lgd_3" "lgd_4" "lgd_5" "lgd_6" "lgd_7" "lgd_8" "lgd_9" "lgd_10"} into lgd;
        close &pd_lgd;
        /*-----------------------------------------------------------------------------------------------------------------*/
        /* ead0 is only for current month, create ead for forecasting quarters*/
        ead = repeat(ead0, 1, 10); 
        num_accounts = nrow(pd);
        workout_actual = j(num_accounts, 1, 0);
        workout_stress = j(num_accounts, 1, 0);
		lgd0 = lgd[, 1];
		lgd = insert(lgd0, lgd, 0, ncol(lgd0)+1);
        /*-----------------------------------------------------------------------------------------------------------------*/
        /*
        use spcl.product_levels;
        read all var {workout_actual__n_} into workout_actual;
        read all var {workout_stress__m_} into workout_stress;
        close spcl.product_levels;
        */
        /*-----------------------------------------------------------------------------------------------------------------*/
        formation_1 = formation_retail(pd, ead, d1, 0); /* @TODO: confirm with Tianyu that the PD is quarterly!!! */ 
        spcl_1 = spcl_retail(lgd, ead0, formation_1, d1, workout_actual, workout_stress);
		create &formation_output._1 from formation_1;
        append from formation_1;
        close &formation_output._1;
        create &spcl_output._1 from spcl_1;
        append from spcl_1;
        close &spcl_output._1;
        /*-----------------------------------------------------------------------------------------------------------------*/
        formation_2 = formation_retail(pd, ead, d2, 0); /* @TODO: confirm with Tianyu that the PD is quarterly!!! */ 
        spcl_2 = spcl_retail(lgd, ead0, formation_2, d2, workout_actual, workout_stress);
		create &formation_output._2 from formation_2;
        append from formation_2;
        close &formation_output._2;
		create &spcl_output._2 from spcl_2;
        append from spcl_2;
        close &spcl_output._2;
    quit;
%mend calculate_spcl;

%calculate_spcl(
    pd_lgd=spcl.pd_lgd_inflation_06, 
    formation_output=spcl.formation_inflation_06,
    spcl_output=spcl.spcl_inflation_06
);

%calculate_spcl(
    pd_lgd=spcl.pd_lgd_inflation_15, 
    formation_output=spcl.formation_inflation_15,
    spcl_output=spcl.spcl_inflation_15
);

%calculate_spcl(
    pd_lgd=spcl.pd_lgd_european_06, 
    formation_output=spcl.formation_european_06,
    spcl_output=spcl.spcl_european_06
);

%calculate_spcl(
    pd_lgd=spcl.pd_lgd_european_15, 
    formation_output=spcl.formation_european_15,
    spcl_output=spcl.spcl_european_15
);


%macro sum_formation();
    sum(col1) as tf_1,
    sum(col2) as tf_2,
    sum(col3) as tf_3,
    sum(col4) as tf_4,
    sum(col5) as tf_5,
    sum(col6) as tf_6,
    sum(col7) as tf_7,
    sum(col8) as tf_8,
    sum(col9) as tf_9,
    sum(col10) as tf_10
%mend sum_formation;

%macro sum_spcl_breakdown();
    sum(col1) as tspcl_1,
    sum(col2) as tspcl_2,
    sum(col3) as tspcl_3,
    sum(col4) as tspcl_4,
    sum(col5) as tspcl_5,
    sum(col6) as tspcl_6,
    sum(col7) as tspcl_7,
    sum(col8) as tspcl_8,
    sum(col9) as tspcl_9,
    sum(col10) as tspcl_10,
    sum(col11) as tspcl_11,
    sum(col12) as tspcl_12,
    sum(col13) as tspcl_13,
    sum(col14) as tspcl_14,
    sum(col15) as tspcl_15,
    sum(col16) as tspcl_16,
    sum(col17) as tspcl_17,
    sum(col18) as tspcl_18,
    sum(col19) as tspcl_19,
    sum(col20) as tspcl_20,
    sum(col21) as tspcl_21,
    sum(col22) as tspcl_22,
    sum(col23) as tspcl_23,
    sum(col24) as tspcl_24,
    sum(col25) as tspcl_25,
    sum(col26) as tspcl_26,
    sum(col27) as tspcl_27,
    sum(col28) as tspcl_28,
    sum(col29) as tspcl_29,
    sum(col30) as tspcl_30
%mend sum_spcl;

%macro sum_spcl_total();
    sum(col1) + sum(col11) + sum(col21) as ttspcl_1,
    sum(col2) + sum(col12) + sum(col22) as ttspcl_2,
    sum(col3) + sum(col13) + sum(col23) as ttspcl_3,
    sum(col4) + sum(col14) + sum(col24) as ttspcl_4,
    sum(col5) + sum(col15) + sum(col25) as ttspcl_5,
    sum(col6) + sum(col16) + sum(col26) as ttspcl_6,
    sum(col7) + sum(col17) + sum(col27) as ttspcl_7,
    sum(col8) + sum(col18) + sum(col28) as ttspcl_8,
    sum(col9) + sum(col19) + sum(col29) as ttspcl_9,
    sum(col10) + sum(col20) + sum(col30) as ttspcl_10
%mend sum_spcl;

/* formation summary 1 */
proc sql;
	create table spcl.formation_summary_1 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_formation
    from
        spcl.formation_inflation_06_1
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_formation
    from
        spcl.formation_european_06_1
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_formation
    from
        spcl.formation_inflation_15_1
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_formation
    from
        spcl.formation_european_15_1
    ;
quit;

/* formation summary 2 */
proc sql;
	create table spcl.formation_summary_2 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_formation
    from
        spcl.formation_inflation_06_2
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_formation
    from
        spcl.formation_european_06_2
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_formation
    from
        spcl.formation_inflation_15_2
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_formation
    from
        spcl.formation_european_15_2
    ;
quit;

/* Breakdown SPCl summary 1 */
proc sql noPrint;
    create table spcl.spcl_breakdown_summary_1 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_inflation_06_1
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_european_06_1
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_inflation_15_1
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_european_15_1
    ;
quit;

/* Breakdown SPCl summary 2 */
proc sql noPrint;
    create table spcl.spcl_breakdown_summary_2 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_inflation_06_2
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_european_06_2
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_inflation_15_2
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_spcl_breakdown
    from
        spcl.spcl_european_15_2
    ;
quit;

/* Total SPCl summary 1 */
proc sql noPrint;
    create table spcl.spcl_total_summary_1 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_inflation_06_1
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_european_06_1
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_inflation_15_1
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_european_15_1
    ;
quit;

/* Total SPCl summary 2 */
proc sql noPrint;
    create table spcl.spcl_total_summary_2 as
    select
        'Model 06 - Inflation' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_inflation_06_2
    union all
    select
        'Model 06 - European' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_european_06_2
    union all
    select
        'Model 15 - Inflation' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_inflation_15_2
    union all
    select
        'Model 15 - European' as model_scenario,
        %sum_spcl_total
    from
        spcl.spcl_european_15_2
    ;
quit;

proc print data=spcl.formation_summary_1;
run;

proc print data=spcl.formation_summary_2;
run;

proc print data=spcl.spcl_total_summary_1;
run;

proc print data=spcl.spcl_total_summary_2;
run;

proc print data=spcl.spcl_breakdown_summary_1;
run;

proc print data=spcl.spcl_breakdown_summary_2;
run;
