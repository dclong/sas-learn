


/************************************************************************
proc datasets library=tempDir;
	delete bmo_raw_all raw_all_temp;
	copy in=clean out=tempDir index=yes;
	select bmo_raw_all;
	change bmo_raw_all=raw_all_temp;
run;
************************************************************************/

*--------------------------------------------------------------------------------------------------------;
/* @question: The following block of code is copied here from other place with #net changed to #raw_all_temp.
Why do we keep it at both places?
*/

/*Truncate records after first DF/PRP/Closed/WO date/Expired observation*/
*--Maturity Date;

/* the last month for each loan */
proc sql noPrint;
    drop table tempDir.m_dt
    ;
    create table tempDir.m_dt as
    select 
        loanID, 
        max(yyyy_mm) as mat_yyyy_mm
    from 
        clean.bmo_raw_all
    group by 
        loanID
    ;
quit;

*--First DF/PRP/Closed/WO date/Expired;
proc sql noPrint;
    drop table tempDir.cutoff
    ;
    create table tempDir.cutoff as
    select 
        A.loanID, 
        min(A.yyyy_mm) as cutoff_yyyy_mm 
    from
        clean.bmo_raw_all A 
    left join 
        tempDir.m_dt B
    on 
        A.loanID = B.loanID 
    where 
        lowcase(strip(A.reportedLoanStatus)) in ('closed','defaulted','write off') or A.isPrepaid = 1
    or 
        /* if no closed, defaulted or write off, use the last available date */
        A.yyyy_mm = B.mat_yyyy_mm
    group by 
        A.loanID
    ;
quit;

proc sql noPrint;
	/* keep only records before the cutoff date */
    drop table tempDir.raw_all_temp
    ;
    create table tempDir.raw_all_temp as
    select 
        A.*
    from 
        clean.bmo_raw_all A 
    left join 
        tempDir.cutoff B
    on 
        A.loanID = B.loanID
    where 
        A.yyyy_mm <= B.cutoff_yyyy_mm
    ;
	/* create index */
    create index pIndex
    on tempDir.raw_all_temp (loanID, yyyy_mm)
    ;
quit;

*--------------------------------------------------------------------------------------------------------;

/* Generate Master Static Table */
proc sql;
    drop table tempDir.st_master
    ;
	create table tempDir.st_master as
    select distinct 
   		loanID 
    from 
        tempDir.raw_all_temp
	order by
		loanID
	;
quit;

/*
Define a temp table var_st_1st which contains all variables whose first records need to be figured out.
*/
proc sql;
    drop table tempDir.var_st_1st
    ;
    create table tempDir.var_st_1st (
        num num, 
        var_st_1st char(255), 
        data_type char(255)
    )
    ;
    insert into tempDir.var_st_1st
    set 
        num = 1, 
        var_st_1st = 'LienPosition', 
        data_type = 'char(255)'	
    set 
        num = 2, 
        var_st_1st = 'c_interest_only', 
        data_type = 'char(255)'
    ;
quit;

/* 
*--Assign the first observation through loop;
For a given column, find the first record (time) non empty/NULL record for each loan.
The table summary is then join back to the original table to get the (real) first record for the column.
It sounds to me that this can be better done using partition over.
*/
%macro first_observation;
    %local i;
    proc sql noPrint;
        select count(*) into :n from tempDir.var_st_1st
        ;
        select var_st_1st into :v_1 - :v_%left(&n.) from tempDir.var_st_1st
        ;
    quit;
    %do i = 1 %to &n. %by 1;
        proc sql noPrint;
            drop table tempDir.t1
            ;
            create table tempDir.t1 as
            select distinct 
                A.loanID,
                &&v_&i as var_st_1st
            from 
                tempDir.raw_all_temp as A 
            /* inner and left join are both OK here */
            inner join (
                    /*This subquery creates the table summary*/
                    select 
                        loanId, 
                        min(yyyy_mm) as yyyy_mm_1st 
                    from 
                        tempDir.raw_all_temp
                    where 
                        &&v_&i <> '' and &&v_&i is not null
                    group by 
						loanId
                ) as B
            on 
                A.loanID = B.loanID and A.yyyy_mm = B.yyyy_mm_1st
            ;
			drop table tempDir.st_master_2
            ;
            create table tempDir.st_master_2 as
            select 
                A.*,
                B.var_st_1st as &&v_&i
            from 
                tempDir.st_master A
            left join 
                tempDir.t1 B 
            on 
                A.loanID = B.loanID
            ;
        quit;
		proc datasets library=tempDir noList;
			delete st_master;
			change st_master_2=st_master;
		run;
    %end;
%mend;

%first_observation;
