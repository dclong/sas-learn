

libname heloc "~/projects/heloc/lib";

libname tt "~/projects/heloc/tingting";

proc print data=tt.moodys_hlc_har_fields_lgd (obs=5);
run;

proc sql;
	select distinct year, month, count(*) as freq from tt.moodys_hlc_har_fields_lgd group by year, month;
	select distinct reportedLoanStatus, count(*) as feq from tt.moodys_hlc_har_fields_lgd group by reportedLoanStatus;
quit;

proc sql;
	select
		sum(reportedLoanStatus="Defaulted" or reportedLoanStatus="Write Off")/count(*) as total_bal_amt
	from
		tt.moodys_hlc_har_fields_lgd
	where 
		strip(c_1st_lien) = "Y" 
	group by
		year, month
	;
quit;

proc sql;
	select
		year,
		month,
		count(*) as total_num_loan,
		sum(cur_bal_amt) as total_bal_amt
	from
		tt.moodys_hlc_har_fields_lgd
	where 
		strip(c_1st_lien) = "Y" 
	group by	
		year,
		month
	;
	select
		year,
		month,
		count(*) as total_num_loan,
		sum(cur_bal_amt) as total_bal_amt
	from
		tt.moodys_hlc_har_fields_lgd
	where 
		strip(c_1st_lien) ^= "Y" 
	group by	
		year,
		month
	;
	/*------------------------------------------------*/
	select
		year,
		month,
		count(*) as total_num_loan,
		sum(cur_bal_amt) as total_bal_amt
	from
		tt.moodys_hlc_mi_fields_lgd_usp
	where 
		strip(c_1st_lien) = "Y" 
	group by	
		year,
		month
	;
	select
		year,
		month,
		count(*) as total_num_loan,
		sum(cur_bal_amt) as total_bal_amt
	from
		tt.moodys_hlc_mi_fields_lgd_usp
	where 
		strip(c_1st_lien) ^= "Y" 
	group by	
		year,
		month
	;
quit;






proc sql;
	create table heloc.dymstat2 as
	select
		*
	from 
		heloc.dymstat
	;
quit;

proc print data=heloc.dymstat (obs=50);
run;


ODS NOPROCTITLE;

proc iml;
	%include "~/sas/util.sas";
    cns = {"TIME_DIM_KEY"	"SK_ACCT_ID"	"MODEL_VERSION_NO"	"RISK_SEG_ID"	"DELINQUENCY_DS"	"CUR_BAL_AMT"	"CUR_INT_RT"	"TOT_PRNCPL_CHG_OFF_AMT"	"PERIOD_START_DT"	"PERIOD_END_DT"	"MONTH_NAME"	"FISCAL_YEAR"	"CALENDAR_YEAR"	"ACCT_PRFL_END_DT"	"ACCT_PRFL_START_DT"	"LN_REG_PYMT_AMT"	"ORIG_CRB_SCR"	"CUR_CRB_SCR"	"LN_ORIG_DT"	"PRNCPL_CHG_OFF_AMT"	"PRNCPL_CHG_OFF_DT"	"COLL_CD"	"PROP_OCC_CD"	"PROP_TYP_CD"	"COLL_AMT"	"ORIG_INT_RT"	"TOT_DTI_RTO"	"ORIG_LTV_RTO"	"LN_AMT"	"AUTH_LIMIT_AMT"	"TERM"	"ADJ_TYP_CD"	"SRC_PRIM_PROV_STATE_CD"	"SRC_PRIM_ZIP_CD"	"PROP_ADDR_STATE_CD"	"PROP_ADDR_ZIP_CD"	"CMTMNT_END_DT"	"LAST_MODIF_DT"	"MODIF_TYP_CD"	"CUST_RT_INDEX_CD"	"c_1st_Lien"	"init_draw_amt"	"CUST_RT_INDEX_DS"	"IOterm"	"CUR_CRB_SCR_FINAL"	"Init_unused_line_amt"	"unused_line_amt"	"modif_dt"	"PIchange"	"PIchange_monthly"	"MATURITY_DT"	"PGM_TYP_CD"	"documentation"	"CSI_RTO"	"Updatedappraisalamount"	"amorterm"	"PRINCPL_CHG_OFF_BAL_AMT"};
	do i = 1 to ncol(cns);
		vname = cns[i];
		use heloc.dymstat;
		read all var vname into v;
		close heloc.dymstat;
		run check_variable(v, vname);
	end;
quit;



/* check dataset moodys_hlc_hbrccar_har_fields.sas7bdat */

proc print data=tt.moodys_hlc_hbrccar_har_fields (obs=50);
run;


proc sql;
	select
		count(*)
	from 
		tt.moodys_hlc_hbrccar_har_fields
	;
quit;


proc sql;
	select
		count(*)
	from 
		tt.dymstat
	;
quit;

proc iml;
	%include "~/sas/util.sas";
	cns = {"TIME_DIM_KEY" "SK_ACCT_ID" "MODEL_VERSION_NO" "RISK_SEG_ID" "DELINQUENCY_DS" "CUR_BAL_AMT" "CUR_INT_RT" "TOT_PRNCPL_CHG_OFF_AMT" "PERIOD_START_DT" "PERIOD_END_DT" "MONTH_NAME" "FISCAL_YEAR" "CALENDAR_YEAR" "ACCT_PRFL_END_DT" "ACCT_PRFL_START_DT" "LN_REG_PYMT_AMT" "ORIG_CRB_SCR" "CUR_CRB_SCR" "LN_ORIG_DT" "PRNCPL_CHG_OFF_AMT" "PRNCPL_CHG_OFF_DT" "COLL_CD" "PROP_OCC_CD" "PROP_TYP_CD" "COLL_AMT" "ORIG_INT_RT" "TOT_DTI_RTO" "ORIG_LTV_RTO" "LN_AMT" "AUTH_LIMIT_AMT" "TERM" "ADJ_TYP_CD" "SRC_PRIM_ZIP_CD" "PROP_ADDR_STATE_CD" "PROP_ADDR_ZIP_CD" "CMTMNT_END_DT" "LAST_MODIF_DT" "MODIF_TYP_CD" "c_1st_Lien" "init_draw_amt" "IOterm" "CUR_CRB_SCR_FINAL" "Init_unused_line_amt" "unused_line_amt" "modif_dt" "PIchange" "PIchange_monthly" "MATURITY_DT" "PGM_TYP_CD" "documentation" "CSI_RTO" "Updatedappraisalamount" "amorterm" "PRINCPL_CHG_OFF_BAL_AMT" "ACCT_NBR" "year" "month" "FI_INSTRUMENT_ID" "START_DATE" "CAP" "FLOOR" "PERIODIC_CAP" "LOAN_MARGIN" "LOAN_INDEX" "RATE_RESET_TYPE" "RT_RESET_FREQ" "OE_FIRST_REPR_DT" "fixedrateperiod"};
	do i = 1 to ncol(cns);
		vname = cns[i];
		use tt.moodys_hlc_hbrccar_har_fields;
		read all var vname into v;
		close heloc.dymstat;
		run check_variable(v, vname);
	end;
quit;