
				
/******************************************************************************************/
%let hpi_transformation = qqc; /* does not matter as hpi is not used */
%let ur_transformation = yyc;
%let llvs = orig_ltv_rto_grp joint_applcnt_ind term_grp;
%let mevs = ur_hybrid_&ur_transformation. x73_l1 x1_yyc;
/******************************************************************************************/
%let predictors = &llvs. &mevs.;

%let hpi_hybrid_extra = x54_&hpi_transformation. x56_&hpi_transformation. x53_&hpi_transformation.;
%let ur_hybrid_extra = x61_&ur_transformation. x60_&ur_transformation. x3_&ur_transformation.;
 

%let extra = sk_acct_id year quarter snapshot_month src_prim_prov_state_cd &hpi_hybrid_extra. &ur_hybrid_extra. vehicle_year_nbr;

%let categorical_variables = acct_status_cd acct_typ_cd amort_typ_cd coll_cd prod_typ_cd
				ln_amt_grp ln_orig_year_grp orig_crb_scr_grp orig_int_rt_grp orig_ltv_rto_grp 
				region_grp term_grp vehicle_age_grp vehicle_make_grp vehicle_year_grp
                vtype prod_id joint_applcnt_ind cur_bal_amt_grp;


data temp.dec_2014_clean_subset;
	set temp.dec_2014_clean_3;
	keep &predictors. &extra.;
run;

data temp.dfast_base_tran_subset;
	set dfast.dfast_base_tran;
	keep &predictors. &extra.;
run;

data temp.dfast_inflation_tran_subset;
	set dfast.dfast_inflation_tran;
	keep &predictors. &extra.;
run;

data temp.dfast_european_tran_subset;
	set dfast.dfast_european_tran;
	keep &predictors. &extra.;
run;


proc sql;
	create table temp.dec_2014_dbase_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_base_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;

proc sql;
	create table temp.dec_2014_inflation_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_inflation_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;

proc sql;
	create table temp.dec_2014_european_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_european_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;


data fc.dec_2014_dbase;
    set temp.dec_2014_dbase_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;

data fc.dec_2014_inflation;
    set temp.dec_2014_inflation_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;


data fc.dec_2014_european;
    set temp.dec_2014_european_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;


/* In-sample fitting and scenario forecast */
proc logistic data=lgd.lgd_all_variables_dfast outest=betas outmodel=lmodel;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = &predictors. / rsquare;
	contrast 'Joint Significance of LTV' orig_ltv_rto_grp 1 0 0 0,
										 orig_ltv_rto_grp 0 1 0 0,
										 orig_ltv_rto_grp 0 0 1 0,
										 orig_ltv_rto_grp 0 0 0 1;
	contrast 'Joint Significance of Term' term_grp 1 0,
										  term_grp 0 1;
run;

/* In-sample fitting and scenario forecast */
proc logistic data=lgd.lgd_all_variables_dfast outest=betas outmodel=lmodel;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = &predictors. / rsquare;
	score data=lgd.lgd_all_variables_dfast out=testing.dfast_all_fit;
	score data=fc.dec_2014_dbase out=fc.dbase_forecast;
	score data=fc.dec_2014_inflation out=fc.inflation_forecast;
	score data=fc.dec_2014_european out=fc.european_forecast;
run;

/* Out-of-sample testing */
proc logistic data=lgd.lgd_training_dfast outest=betas outmodel=lmodel;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = &predictors. / rsquare;
	score data=lgd.lgd_testing_dfast out=testing.dfast_testing_fit;
run;


/* 
In-sample and out-of-sample backtesting
*/
proc sql;
	create table testing.dfast_backtesting as
	select
		I.year,
		I.quarter,
		cat(substr(putn(I.year, "4."), 3, 2), "Q", I.quarter) as yyqq,
		I.lgd_qtr_avg as observed_lgd_qtr_avg_in,
		I.lgd_hat_qtr_avg as fitted_lgd_qtr_avg,
		O.lgd_qtr_avg as observed_lgd_qtr_avg_out,
		O.lgd_hat_qtr_avg as predicted_lgd_qtr_avg
	from /* in-sample */
		(select year, quarter, mean(final_lgd) as lgd_qtr_avg, mean(p_event) as lgd_hat_qtr_avg from testing.dfast_all_fit group by year, quarter) I
	inner join /* out-of-sample */
		(select year, quarter, mean(final_lgd) as lgd_qtr_avg, mean(p_event) as lgd_hat_qtr_avg from testing.dfast_testing_fit group by year, quarter) O
	on
		I.year = O.year and I.quarter = O.quarter
	;
quit;

/*
Scenario forecasting
*/
proc sql;
	create table fc.dfast_scenario_forecast as
	select 
		year, 
		quarter, 
		yyqq, 
		observed_lgd_qtr_avg_in as observed_lgd_qtr_avg,
		fitted_lgd_qtr_avg as base_fit_forecast,
		fitted_lgd_qtr_avg as adverse_fit_forecast,
		fitted_lgd_qtr_avg as severely_fit_forecast
	from
		testing.dfast_backtesting
	union all
	select 
		B.year,
		B.quarter,
		cat(substr(putn(B.year, "4."), 3, 2), "Q", B.quarter) as yyqq,
		. as Observed,
		B.lgd_hat_qtr_avg as base_fit_forecast,
		A.lgd_hat_qtr_avg as adverse_fit_forecast,
		S.lgd_hat_qtr_avg as severely_fit_forecast
	from /* quarterly average of base */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from fc.dbase_forecast group by year, quarter) as B
	inner join /* quarterly average of adverse */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from fc.inflation_forecast group by year, quarter) as A
	on
		B.year = A.year and B.quarter = A.quarter
	inner join /* quarterly average of severely */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from fc.european_forecast group by year, quarter) as S
	on
		B.year = S.year and B.quarter = S.quarter
	;
quit;

/*------------------------------ R-squared ------------------------------------------------------*/
/*The usual MSE and R-Square */
%r_squared(input_data=testing.dfast_all_fit, y=final_lgd, yhat=p_event, output_data=temp.r_squared_all);
/* The Quarterly (notice that snapshot_month is selected 1 in 3) MSE and R-Square */
%r_squared(input_data=testing.dfast_all_fit, y=final_lgd, yhat=p_event, group_by=snapshot_month, output_data=temp.r_squared_all_quarterly);
/* The Yearly usual MSE and R-Square */
%r_squared(input_data=testing.dfast_all_fit, y=final_lgd, yhat=p_event, group_by=year, output_data=temp.r_squared_all_yearly);
/* The usual MSE and R-Square */
%r_squared(input_data=testing.dfast_testing_fit, y=final_lgd, yhat=p_event, output_data=temp.r_squared_testing);
/* The Quarterly (notice that snapshot_month is selected 1 in 3) MSE and R-Square */
%r_squared(input_data=testing.dfast_testing_fit, y=final_lgd, yhat=p_event, group_by=snapshot_month, output_data=temp.r_squared_testing_quarterly);
/* The Yearly usual MSE and R-Square */
%r_squared(input_data=testing.dfast_testing_fit, y=final_lgd, yhat=p_event, group_by=year, output_data=temp.r_squared_testing_yearly);

proc sql;
	ods select all;
	select "All Data - Usual" as type, * from temp.r_squared_all
	union all
	select "All Data - Quarterly" as type, * from temp.r_squared_all_quarterly
	union all
	select "All Data - Yearly" as type, * from temp.r_squared_all_yearly
	union all
	select "Testing Data - Usual" as type, * from temp.r_squared_testing
	union all
	select "Testing Data - Quarterly" as type, * from temp.r_squared_testing_quarterly
	union all
	select "Testing Data - Yearly" as type, * from temp.r_squared_testing_yearly
	;
quit;