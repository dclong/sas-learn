
libname spcl "~/projects/ia_lgd_loan_level/lib/spcl/";
libname llm "~/projects/ia_lgd_loan_level/lib/spcl/loan_level";
libname seg "~/projects/ia_lgd_loan_level/lib/spcl/segmentation";

proc iml;
    /* include SAS IML modules */
    %include "~/sas/iml/spcl.sas";
    %include "~/sas/iml/ia.sas";
    /*-----------------------------------------------------------------------------------------------------------------*/
    use llm.pd_lgd_european_06;
	read all var {"risk_seg_id"} into segment_id;
    read all var {"ead"} into ead0;
    read all var {"d1"} into d1;
    read all var {"d2"} into d2;
    use llm.pd_lgd_european_06;
	num_accounts = nrow(segment_id) * ncol(segment_id);
    /*-----------------------------------------------------------------------------------------------------------------*/
    segments_pd = {"IAN_01" "IAN_02" "IAN_03" "IAN_04" "IAN_05" "IAN_06" "IAU_01" "IAU_02" "IAU_03" "IAU_04" "IAU_05" "IAU_06"};
    realized_dr = {0.00127643 0.005173535 0.017377271 0.08410697 0.042622359 0.367857143
                        0.001794936 0.005477932 0.015805446 0.071339758 0.04165731 0.340776699};
    realized_dr = realized_dr_seg_ia(segment_id, segments_pd, realized_dr);
    /*-----------------------------------------------------------------------------------------------------------------*/
    pd_scalars_european = {1.276998048 1.36901491 1.470203738 1.883317509 2.215301097 2.56297393 2.751150055 2.830474654 2.689363396};
    pd_scalars = pd_scalar_seg_ia(segment_id, pd_scalars_european);
    /*-----------------------------------------------------------------------------------------------------------------*/
    /* calculating post PDs */
	post_crm_pd = j(num_accounts, 1, 0.8);
    pd = post_pd(segment_id, post_crm_pd, realized_dr, pd_scalars);
    /*-----------------------------------------------------------------------------------------------------------------*/
    segments_lgd = {"IAN_01", "IAN_02", "IAU_01", "IAU_02"};
    realized_lgd = {0.30, 0.39, 0.29, 0.39};
    realized_lgd = realized_lgd_seg_ia(segment_id, segments_lgd, realized_lgd);
    /*-----------------------------------------------------------------------------------------------------------------*/
    lgd_scalars_european = {0.922218785	1.119389632	1.162261001	1.058672093,
                            0.915672409	1.121984973	1.156606338	1.053312593,
                            0.903975274	1.113625251	1.161531921	1.057947114,
                            0.897879305	1.11527683	1.188457922	1.082460619,
                            0.907431441	1.14085554	1.248989386	1.13750382,
                            0.949699965	1.207679999	1.257117979	1.144808231,
                            0.982151364	1.235507433	1.255072541	1.143051885,
                            1.00780677	1.254047754	1.246792712	1.135637259,
                            1.026388525	1.263982094	1.231012702	1.121246567};
    lgd_scalars = lgd_scalar_seg_ia(segment_id, segments_lgd, lgd_scalars_european);
    /*-----------------------------------------------------------------------------------------------------------------*/
    /* calculating post LGD */
	post_crm_lgd = j(num_accounts, 1, 0);
	realized_lgd_port = .;
	lgd_scalar_port = .;
    lgd = post_lgd(post_crm_lgd, realized_lgd, lgd_scalars, realized_lgd_port, lgd_scalar_port);
    /*-----------------------------------------------------------------------------------------------------------------*/
    /* ead0 is only for current month, create ead for forecasting quarters*/
    ead = repeat(ead0, 1, 9); 
    workout_actual = j(num_accounts, 1, 2);
    workout_stress = j(num_accounts, 1, 4);
    /*-----------------------------------------------------------------------------------------------------------------*/
    formation_1 = formation_retail(pd, ead, d1, 1); /* @TODO: confirm with Tianyu that the PD is quarterly!!! */ 
    spcl_1 = spcl_retail(lgd, ead0, formation_1, d1, workout_actual, workout_stress);
    create seg.formation_european_1 from formation_1;
    append from formation_1;
    close seg.formation_european_1;
    create seg.spcl_european_1 from spcl_1;
    append from spcl_1;
    close seg.spcl_european_1;
    /*-----------------------------------------------------------------------------------------------------------------*/
    formation_2 = formation_retail(pd, ead, d2, 1); /* @TODO: confirm with Tianyu that the PD is quarterly!!! */ 
    spcl_2 = spcl_retail(lgd, ead0, formation_2, d2, workout_actual, workout_stress);
    create seg.formation_european_2 from formation_2;
    append from formation_2;
    close seg.formation_european_2;
    create seg.spcl_european_2 from spcl_2;
    append from spcl_2;
    close seg.spcl_european_2;
quit;
