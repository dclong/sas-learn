
/*GAP*/

/* year table */
proc sql noPrint;
    drop table tempDir.year
    ;
    create table tempDir.year (
        yr num
    )
    ;
    insert into tempDir.year values (2002);
    insert into tempDir.year values (2003);
    insert into tempDir.year values (2004);
    insert into tempDir.year values (2005);
    insert into tempDir.year values (2006);
    insert into tempDir.year values (2007);
    insert into tempDir.year values (2008);
    insert into tempDir.year values (2009);
    insert into tempDir.year values (2010);
    insert into tempDir.year values (2011);
    insert into tempDir.year values (2012);
    insert into tempDir.year values (2013);
    insert into tempDir.year values (2014);
    insert into tempDir.year values (2015);
quit;

/* month table */
proc sql noPrint;
    drop table tempDir.month
    ;
    create table tempDir.month (
        m num
    )
    ;
    insert into tempDir.month values (1);
    insert into tempDir.month values (2);
    insert into tempDir.month values (3);
    insert into tempDir.month values (4);
    insert into tempDir.month values (5);
    insert into tempDir.month values (6);
    insert into tempDir.month values (7);
    insert into tempDir.month values (8);
    insert into tempDir.month values (9);
    insert into tempDir.month values (10);
    insert into tempDir.month values (11);
    insert into tempDir.month values (12);
quit;


*--time Table, t starts from 200201;
proc sql noPrint;
    drop table tempDir.t1
    ;
    create table tempDir.t1 as
    select 
        yr, 
        m,
        yr*100 + m as yrM,
        (yr-2002) * 12 + (m-0) as t
    from 
        tempDir.year 
    cross join 
        tempDir.month
    order by 
        yrM
    ;
quit;

*--2.b.table contains static information and maxT, minT;
proc sql noPrint;
    /* find the max and min time period for each record */
    drop table tempDir.n0
    ;
    create table tempDir.n0 as
    select 
        A.loanId, 
        max(B.t) as maxT,
        min(B.t) as minT,
        /* @question: does it make sense to use max(orginationDate) here? We have mixed portfolios together 
		and why don't we use min(originationDate)? */
        year(max(A.originationDate)) as origination_year,
        month(max(A.originationDate)) as origination_month
    from 
        tempDir.net A 
    left join 
        tempDir.t1 B
    on 
        A.yyyy_mm = B.yrM
    group by 
        A.loanId
    ;
quit;

proc sql noPrint;
    drop table tempDir.n1
    ;
    *-- tempDir.n1 will have all the records for each loan from the first observation available to the last obs without any gap;
    create table tempDir.n1 as
    select 
        S.*,
        T.*,
        (T.yr-S.origination_year)*12 + (T.m-S.origination_month) as loanAge
    from 
        tempDir.n0 S    
    cross join 
        tempDir.t1 T
    where 
        S.minT <= T.t <= S.maxT
    order by 
        loanId, yrM
    ;
quit;

proc sql noPrint;
    /*table tempDir.w1 is the new dynamic table that won't have any gaps*/
    drop table tempDir.w1
    ;
    create table tempDir.w1 as
    select 
        N.t
        ,N.loanId
        ,N.yrm as yyyy_mm
        ,W.rate
        ,W.currentAmount
        ,W.updatedFICO
        ,W.reportedLoanStatus 
        ,W.isPrepaid
        ,W.isDefault
        ,case when W.yyyy_mm is NULL then 1 else 0 end as impute
    from 
        tempDir.n1 N            
    left join 
        tempDir.net W 
    on 
        N.loanId = W.loanId and N.yrM = W.yyyy_mm
    ;
quit;


/* tempDir.i1 has the closest t of realized non-missing observations to the imputed record */
proc sql noPrint;
    drop table tempDir.i1
    ;
    create table tempDir.i1 as
    select 
        A.loanId,
        A.t,
        A.yyyy_mm, 
        max(B.t) as maxNonMissingT 
    from 
        tempDir.w1 A
    left join 
        tempDir.w1 B
    on 
        A.loanId = B.loanId and A.t > B.t
    where 
        A.impute = 1 and B.impute = 0
    group by 
        A.loanId, A.t, A.yyyy_mm
    ;
quit;


proc sql noPrint;
    drop table tempDir.i2
    ;
    create table tempDir.i2 as
    select 
        I.*, 
        W.currentAmount,
        W.rate,
        W.reportedLoanStatus,
        W.isPrepaid
    from 
        tempDir.i1 I
    left join 
        tempDir.w1 W
    on 
        I.loanId = W.loanId and I.maxNonMissingT = W.t
    ;
quit;


proc sql noPrint;
    drop table tempDir.w1_2
    ;
    create table tempDir.w1_2 as
    select
        W.*,
        I.currentAmount as current_amount_i2,
        I.rate as rate_i2,
        I.reportedLoanStatus as reported_loan_status_i2,
        I.isPrepaid as is_prepaid_i2
    from  
        tempDir.w1 W
    left join 
        tempDir.i2 I
    on  
        W.loanId = I.loanId and W.t = I.t
    ;
    update tempDir.w1_2
    set  
        currentAmount = current_amount_i2,
        rate = rate_i2,
        reportedLoanStatus = reported_loan_status_i2,
        isPrepaid = is_prepaid_i2
    where 
        impute = 1
    ;
quit;


proc datasets library=tempDir;
	delete w1;
	change w1_2=w1;
run;


