



%let categorical_variables = acct_status_cd acct_typ_cd amort_typ_cd coll_cd prod_typ_cd
				ln_amt_grp ln_orig_year_grp orig_crb_scr_grp orig_int_rt_grp orig_ltv_rto_grp 
				region_grp term_grp vehicle_age_grp vehicle_make_grp vehicle_year_grp
                vtype prod_id joint_applcnt_ind cur_bal_amt_grp;
/*
data select.variable_combs_3;
	set variable_combs_3;
run;
*/

/*
orig_ltv_rto_grp cur_bal_amt_grp vehicle_age_grp
orig_ltv_rto_grp cur_bal_amt_grp term_grp
orig_ltv_rto_grp cur_bal_amt_grp joint_applcnt_ind
orig_ltv_rto_grp vehicle_age_grp term_grp
orig_ltv_rto_grp vehicle_age_grp joint_applcnt_ind
orig_ltv_rto_grp term_grp joint_applcnt_ind
cur_bal_amt_grp vehicle_age_grp term_grp
cur_bal_amt_grp vehicle_age_grp joint_applcnt_ind
cur_bal_amt_grp term_grp joint_applcnt_ind
vehicle_age_grp term_grp joint_applcnt_ind
*/			

proc logistic data=lgd.lgd_all_variables_dfast;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = orig_ltv_rto_grp cur_bal_amt_grp vehicle_age_grp hpi_hybrid_qqc	x1_yyc_l1	ur_hybrid_qqg;
	ods output ParameterEstimates=temp.pe;
run;
ods select all;
proc contents data=lgd.lgd_all_variables_dfast;
run;

data sss;
	set lgd.lgd_all_variables_dfast;
	keep orig_ltv_rto_grp cur_bal_amt_grp vehicle_age_grp hpi_hybrid_qqc	x1_yyc_l1	ur_hybrid_qqg;
run;


%macro fit_logistic_models(variable_list=, loan_level_variables=, data=, parameter_estimates=, temp_lib=Work, clean=True);
	ods select none;
	proc delete data=&parameter_estimates.;
	run;
	proc sql noPrint;
		select count(*) into :n from &variable_list.;
		select hpi into :hpi_1 - :hpi_%left(&n.) from &variable_list.;
		select manheim into :manheim_1 - :manheim_%left(&n.) from &variable_list.;
		select ur into :ur_1 - :ur_%left(&n.) from &variable_list.;
	quit;
	%local i pe;
	%let pe = _0618115778196013479142657742047;
	%do i = 1 %to &n.;
		proc logistic data=&data.;
			class &categorical_variables. / param=ref;
			model final_lgd/dummy_trial = &loan_level_variables. &&hpi_&i. &&manheim_&i. &&ur_&i.;
			ods output ParameterEstimates=&temp_lib..&pe.;
		run;
		proc append base=&parameter_estimates.
			data=&temp_lib..&pe.
			force
			; 
		run;
	%end;
	%if &clean. = True %then %do;
        proc datasets library=&temp_lib. noList;
            delete &pe.;
        run;
    %end;
	ods select all;
%mend;

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp cur_bal_amt_grp vehicle_age_grp, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_1
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp cur_bal_amt_grp term_grp, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_2
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp cur_bal_amt_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_3
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp vehicle_age_grp term_grp, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_4
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp vehicle_age_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_5
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=orig_ltv_rto_grp term_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_6
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=cur_bal_amt_grp vehicle_age_grp term_grp, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_7
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=cur_bal_amt_grp vehicle_age_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_8
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=cur_bal_amt_grp term_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_9
);

%fit_logistic_models(
	variable_list=select.variable_combs_3, 
	loan_level_variables=vehicle_age_grp term_grp joint_applcnt_ind, 
	data=lgd.lgd_all_variables_dfast,
	parameter_estimates=select.parameter_estimates_10
);

/* export parameter estimates to csv */
proc export data=select.parameter_estimates_01 
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_01.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_02 
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_02.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_03
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_03.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_04
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_04.csv"
	dbms=csv
	replace;
run;


proc export data=select.parameter_estimates_05
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_05.csv"
	dbms=csv
	replace;
run;


proc export data=select.parameter_estimates_06
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_06.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_07
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_07.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_08
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_08.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_09
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_09.csv"
	dbms=csv
	replace;
run;

proc export data=select.parameter_estimates_10
	outfile="~/projects/ia_lgd_loan_level/data/parameter_estimates_10.csv"
	dbms=csv
	replace;
run;
