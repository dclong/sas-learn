




%let categorical_variables = acct_status_cd acct_typ_cd amort_typ_cd coll_cd prod_typ_cd
				ln_amt_grp ln_orig_year_grp orig_crb_scr_grp orig_int_rt_grp orig_ltv_rto_grp 
				region_grp term_grp vehicle_age_grp vehicle_make_grp vehicle_year_grp
                vtype prod_id joint_applcnt_ind;

%let predictors = ur_hybrid ur_hybrid_l1 ur_hybrid_l2 ur_hybrid_l3 ur_hybrid_qqc ur_hybrid_qqc_l1 
		ur_hybrid_qqc_l2 ur_hybrid_qqc_l3 ur_hybrid_qqg ur_hybrid_qqg_l1 ur_hybrid_qqg_l2 
		ur_hybrid_qqg_l3 ur_hybrid_yyc ur_hybrid_yyc_l1 ur_hybrid_yyc_l2 ur_hybrid_yyc_l3 
		ur_hybrid_yyg ur_hybrid_yyg_l1 ur_hybrid_yyg_l2 ur_hybrid_yyg_l3 gdp_hybrid gdp_hybrid_l1 
		gdp_hybrid_l2 gdp_hybrid_l3 gdp_hybrid_qqc gdp_hybrid_qqc_l1 gdp_hybrid_qqc_l2 gdp_hybrid_qqc_l3 
		gdp_hybrid_qqg gdp_hybrid_qqg_l1 gdp_hybrid_qqg_l2 gdp_hybrid_qqg_l3 gdp_hybrid_yyc gdp_hybrid_yyc_l1 
		gdp_hybrid_yyc_l2 gdp_hybrid_yyc_l3 gdp_hybrid_yyg gdp_hybrid_yyg_l1 gdp_hybrid_yyg_l2 gdp_hybrid_yyg_l3 
		x32 x32_l1 x32_l2 x32_l3 x32_qqc x32_qqc_l1 x32_qqc_l2 x32_qqc_l3 x32_qqg x32_qqg_l1 x32_qqg_l2 
		x32_qqg_l3 x32_yyc x32_yyc_l1 x32_yyc_l2 x32_yyc_l3 x32_yyg x32_yyg_l1 x32_yyg_l2 x32_yyg_l3 
		x42 x42_l1 x42_l2 x42_l3 x42_qqc x42_qqc_l1 x42_qqc_l2 x42_qqc_l3 x42_qqg x42_qqg_l1 x42_qqg_l2 
		x42_qqg_l3 x42_yyc x42_yyc_l1 x42_yyc_l2 x42_yyc_l3 x42_yyg x42_yyg_l1 x42_yyg_l2 x42_yyg_l3 
		x17 x17_l1 x17_l2 x17_l3 x17_qqc x17_qqc_l1 x17_qqc_l2 x17_qqc_l3 x17_qqg x17_qqg_l1 
		x17_qqg_l2 x17_qqg_l3 x17_yyc x17_yyc_l1 x17_yyc_l2 x17_yyc_l3 x17_yyg x17_yyg_l1 x17_yyg_l2 
		x17_yyg_l3 x19 x19_l1 x19_l2 x19_l3 x19_qqc x19_qqc_l1 x19_qqc_l2 x19_qqc_l3 x19_qqg x19_qqg_l1
		x19_qqg_l2 x19_qqg_l3 x19_yyc x19_yyc_l1 x19_yyc_l2 x19_yyc_l3 x19_yyg x19_yyg_l1 x19_yyg_l2 
		x19_yyg_l3 x20 x20_l1 x20_l2 x20_l3 x20_qqc x20_qqc_l1 x20_qqc_l2 x20_qqc_l3 x20_qqg x20_qqg_l1 
		x20_qqg_l2 x20_qqg_l3 x20_yyc x20_yyc_l1 x20_yyc_l2 x20_yyc_l3 x20_yyg x20_yyg_l1 x20_yyg_l2 
		x20_yyg_l3 x1 x1_l1 x1_l2 x1_l3 x1_qqc x1_qqc_l1 x1_qqc_l2 x1_qqc_l3 x1_qqg x1_qqg_l1 x1_qqg_l2 
		x1_qqg_l3 x1_yyc x1_yyc_l1 x1_yyc_l2 x1_yyc_l3 x1_yyg x1_yyg_l1 x1_yyg_l2 x1_yyg_l3 hpi_hybrid 
		hpi_hybrid_l1 hpi_hybrid_l2 hpi_hybrid_l3 hpi_hybrid_qqc hpi_hybrid_qqc_l1 hpi_hybrid_qqc_l2 
		hpi_hybrid_qqc_l3 hpi_hybrid_qqg hpi_hybrid_qqg_l1 hpi_hybrid_qqg_l2 hpi_hybrid_qqg_l3 
		hpi_hybrid_yyc hpi_hybrid_yyc_l1 hpi_hybrid_yyc_l2 hpi_hybrid_yyc_l3 hpi_hybrid_yyg 
		hpi_hybrid_yyg_l1 hpi_hybrid_yyg_l2 hpi_hybrid_yyg_l3
		term term_grp orig_ltv_rto orig_ltv_rto_grp region_grp vtype
		orig_crb_scr orig_crb_scr_grp ln_amt ln_amt_grp joint_applcnt_ind orig_spread
		x69 x69_l1 x69_l2 x69_l3 x69_qqc x69_qqc_l1 x69_qqc_l2 x69_qqc_l3 x69_qqg x69_qqg_l1 x69_qqg_l2 
		x69_qqg_l3 x69_yyc x69_yyc_l1 x69_yyc_l2 x69_yyc_l3 x69_yyg x69_yyg_l1 x69_yyg_l2 x69_yyg_l3 x58 
		x58_l1 x58_l2 x58_l3 x58_qqc x58_qqc_l1 x58_qqc_l2 x58_qqc_l3 x58_qqg x58_qqg_l1 x58_qqg_l2 
		x58_qqg_l3 x58_yyc x58_yyc_l1 x58_yyc_l2 x58_yyc_l3 x58_yyg x58_yyg_l1 x58_yyg_l2 x58_yyg_l3 x65 
		x65_l1 x65_l2 x65_l3 x65_qqc x65_qqc_l1 x65_qqc_l2 x65_qqc_l3 x65_qqg x65_qqg_l1 x65_qqg_l2 
		x65_qqg_l3 x65_yyc x65_yyc_l1 x65_yyc_l2 x65_yyc_l3 x65_yyg x65_yyg_l1 x65_yyg_l2 x65_yyg_l3 x68 
		x68_l1 x68_l2 x68_l3 x68_qqc x68_qqc_l1 x68_qqc_l2 x68_qqc_l3 x68_qqg x68_qqg_l1 x68_qqg_l2 
		x68_qqg_l3 x68_yyc x68_yyc_l1 x68_yyc_l2 x68_yyc_l3 x68_yyg x68_yyg_l1 x68_yyg_l2 x68_yyg_l3 x60 
		x60_l1 x60_l2 x60_l3 x60_qqc x60_qqc_l1 x60_qqc_l2 x60_qqc_l3 x60_qqg x60_qqg_l1 x60_qqg_l2 
		x60_qqg_l3 x60_yyc x60_yyc_l1 x60_yyc_l2 x60_yyc_l3 x60_yyg x60_yyg_l1 x60_yyg_l2 x60_yyg_l3 x64 
		x64_l1 x64_l2 x64_l3 x64_qqc x64_qqc_l1 x64_qqc_l2 x64_qqc_l3 x64_qqg x64_qqg_l1 x64_qqg_l2 
		x64_qqg_l3 x64_yyc x64_yyc_l1 x64_yyc_l2 x64_yyc_l3 x64_yyg x64_yyg_l1 x64_yyg_l2 x64_yyg_l3 x2 
		x2_l1 x2_l2 x2_l3 x2_qqc x2_qqc_l1 x2_qqc_l2 x2_qqc_l3 x2_qqg x2_qqg_l1 x2_qqg_l2 x2_qqg_l3 x2_yyc 
		x2_yyc_l1 x2_yyc_l2 x2_yyc_l3 x2_yyg x2_yyg_l1 x2_yyg_l2 x2_yyg_l3 x57 x57_l1 x57_l2 x57_l3 x57_qqc 
		x57_qqc_l1 x57_qqc_l2 x57_qqc_l3 x57_qqg x57_qqg_l1 x57_qqg_l2 x57_qqg_l3 x57_yyc x57_yyc_l1 x57_yyc_l2 
		x57_yyc_l3 x57_yyg x57_yyg_l1 x57_yyg_l2 x57_yyg_l3 x3 x3_l1 x3_l2 x3_l3 x3_qqc x3_qqc_l1 x3_qqc_l2 
		x3_qqc_l3 x3_qqg x3_qqg_l1 x3_qqg_l2 x3_qqg_l3 x3_yyc x3_yyc_l1 x3_yyc_l2 x3_yyc_l3 x3_yyg x3_yyg_l1 x3_yyg_l2 x3_yyg_l3
;

/* based on all observations */
%let lasso = orig_spread orig_ltv_rto_grp ur_hybrid_l3 x1_yyc region_grp
		JOINT_APPLCNT_IND term_grp x42_l3 x32_qqc_l2; 


proc glmSelect data=lgd.harris_ia_lgd_all_variables plot=CriterionPanel;
     class &categorical_variables.; 
     model final_lgd = &predictors. / selection = lasso;
run; 

proc logistic data=lgd.harris_ia_lgd_all_variables outest=betas outmodel=lmodel;
	class &categorical_variables.;
	model final_lgd/dummy_trial = &lasso. / rsquare;
	score data=lgd.harris_ia_lgd_all_variables out=lgd.all_fit;
run;

proc logistic data=lgd.harris_ia_lgd_training outest=betas outmodel=lmodel;
	class &categorical_variables.;
	model final_lgd/dummy_trial = &lasso. / rsquare;
	score data=lgd.harris_ia_lgd_training out=lgd.training_fit;
	score data=lgd.harris_ia_lgd_testing out=lgd.testing_fit;
run;

