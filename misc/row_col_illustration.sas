/*
An illustration of using the row and col modules.
*/
proc iml;
	%include "~/sas/util.sas";
	x = {1 2 3 4, 1 2 3 4, 1 2 3 4, 1 2 3 4};
	print x;
	row_index = row(x);
	print row_index;
	col_index = col(x);
	print col_index;
	trunc_matrix = (row_index < col_index) & (col_index <= row_index+2);
	print trunc_matrix;
quit;
