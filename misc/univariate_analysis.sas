
%include '/home/nsun01/profile.txt';
%include '/home/nsun01/SAS_Macro/maclib/*.sas';

libname hbr db2 user=&userid using=&userpw database=HBR schema=HBR; /*HARRIS BASEL RETAIL*/
libname hbrusp db2 user=&userid using=&userpw database=HBR schema=HBRUSPACE;
libname sunny '/sasdata/rmg_output01/nsun01/data/IndirectAuto_LGD';
libname usdata '/sasdata/rmg_output01/US data/2015 US IA';

data model;
    set sunny.ia_lgd_final_macro_mdl_v2;
    /*lgd_nix_model = probit(lgd_nix);*/
run;

proc contents data=model; 
run;

proc univariate data=model; 
    var lgd_nix; 
run;


*******************************************************************************************;
**** segement numerical variables *********************************************************;
*******************************************************************************************;

proc arboretum data=lgd.lgd_all_variables_dfast;
    input cur_bal_amt  / level=interval; 
    target final_lgd / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;

proc sql;
	select count(*) from lgd.lgd_all_variables_dfast where cur_bal_amt < 0;
run;

proc sql;
	create table t as
	select *,
		case 
			when cur_bal_amt = . then
				5
			when cur_bal_amt < 10000 then 
				1
			when cur_bal_amt < 21000 then
				2
			when cur_bal_amt < 32000 then
				3
			else 
				4
		end as cur_bal_amt_grp
	from 
		lgd.lgd_all_variables_dfast
	;
quit;

proc sql;
	ods select all;
	select
		cur_bal_amt_grp,
		count(*) as n, 
		mean(final_lgd) as lgd_avg 
	from	
		t
	group by
		1
	;
quit;

proc arboretum data=model;
    input orig_vintage  / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;

proc arboretum data=model;
    input new_ln_amt / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;

proc arboretum data=model;
    input new_orig_ltv_rto  / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;

proc arboretum data=model;
    input new_orig_int_rt  / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;

proc arboretum data=model;
    input new_orig_crb_scr  / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;


proc arboretum data=model;
    input veh_year  / level=interval; 
    target lgd_nix / level=interval; 
    interact;
    train leafsize=200 maxbranch=5 maxdepth=1 intervalbins=15 ;/*missing=useinsearch*/
    assess measure=ase;
    subtree best;
    save model=lgdsegmodel path=segpath;
run;


*********************************************************************************;
**** univariate variable analysis ***********************************************;
*********************************************************************************;

**** macroeconomic variables;
proc corr data=model noprob out=corr_macro noprint;
    var lgd_nix gdp--prime_yyg_lag3;
run;

proc corr data=model noprob out=corr_macro_level noprint;
    var lgd_nix
    gdp
    unemply_rt
    tbill_1mth
    tbill_3mth
    libor_1mth
    libor_3mth
    spread_1mth
    spread_3mth
    libor_6mth
    libor_12m
    tnote_2yr
    tnote_5yr
    tnote_7yr
    tnote_10y
    tnote_30y
    us_swap_rt_2yr
    us_swap_rt_5yr
    us_swap_rt_7yr
    us_swap_rt_10yr
    us_swap_rt_30yr
    adj_mort_rt_1yr
    adj_mort_rt_5over1
    mort_rt_15yr
    mort_rt_30yr
    mort_rt_less_30y_tbond
    real_fed_funds_tgt_rt
    real_tnote_10yr
    w_texas_inter
    henry_hub
    consumer_credit
    hs_dt_t_disp_inc_rt
    case_shiller_hpi_nat
    case_shiller_hpi_il
    fhfa_hpi_milwaukee
    unemply_rt_wi
    unemply_rt_il
    gdp_wi
    gdp_il
    manheim;
run;

proc corr data=model noprob out=corr_macro_level noprint;
    var
    lgd_nix
    tbill_1mth
    tbill_3mth
    libor_1mth
    libor_3mth
    spread_1mth
    spread_3mth
    libor_6mth
    libor_12m
    tnote_2yr
    tnote_5yr
    tnote_7yr
    tnote_10y
    tnote_30y
    us_swap_rt_2yr
    us_swap_rt_5yr
    us_swap_rt_7yr
    us_swap_rt_10yr
    us_swap_rt_30yr
    adj_mort_rt_1yr
    adj_mort_rt_5over1
    mort_rt_15yr
    mort_rt_30yr
    mort_rt_less_30y_tbond
    real_fed_funds_tgt_rt
    real_tnote_10yr
    ;
run;


**** account level variables;
** numerical;
proc corr data=model noprob out=corr_acct;
    var lgd_nix orig_vintage new_ln_amt new_orig_ltv_rto new_orig_int_rt new_orig_crb_scr veh_year;
run;

proc means data= model; 
    var lgd_nix; 
    class orig_vintage_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_ln_amt_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_ltv_rto_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_int_rt_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_crb_scr_grp; 
run;

proc means data= model; 
    var lgd_nix; class veh_year_grp; 
run;  

** categorical; 
proc means data= model; 
    var lgd_nix; class joint_applcnt_ind; 
run;

proc means data= model; 
    var lgd_nix; class region; 
run;

proc means data= model; 
    var lgd_nix; class vtype; 
run;

proc means data= model; 
    var lgd_nix; class age_collateral_yr_grp; 
run;

proc means data= model; 
    var lgd_nix; class loan_term; 
run;

proc means data= model; 
    var lgd_nix; class veh_brand; 
run;

** modified the categorical; 
proc means data= model; 
    var lgd_nix; class region_grp; 
run;

proc means data= model; 
    var lgd_nix; class loan_term_grp; 
run;

proc means data= model; 
    var lgd_nix; class veh_brand_grp; 
run;

** group numerical variables;
proc means data= model; 
    var lgd_nix; class new_orig_vintage_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_ln_amt_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_ltv_rto_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_int_rt_grp; 
run;

proc means data= model; 
    var lgd_nix; class new_orig_crb_scr_grp; 
run;

proc means data= model; 
    var lgd_nix; class veh_year_grp; 
run;

** corr for categorical variables;
proc corr data=model noprob out=corr_acct;
    var lgd_nix orig_vintage new_ln_amt new_orig_ltv_rto new_orig_int_rt new_orig_crb_scr veh_year
        joint_app_ind_n joint_app_ind_y loan_term_le_60mth loan_term_61_72mth loan_term_gt_72mth
        region_grp_mw region_grp_oth veh_brand_grp_jp veh_brand_grp_oth vtype_new vtype_old
        age_collateral_yr_grp_0_2yr age_collateral_yr_grp_2_5yr age_collateral_yr_grp_5_7yr age_collateral_yr_grp_ge_7yr age_collateral_yr_grp_missing;
run;
