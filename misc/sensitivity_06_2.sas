
/******************************************************************************************/
%let magnifier = 3;
%let hpi_transformation = qqc; /* does not matter as hpi is not used */
%let ur_transformation = yyc;
%let llvs = orig_ltv_rto_grp joint_applcnt_ind term_grp;
%let mevs = ur_hybrid_&ur_transformation. x73_l1 x1_yyc;
/******************************************************************************************/
%let predictors = &llvs. &mevs.;

%let hpi_hybrid_extra = x54_&hpi_transformation. x56_&hpi_transformation. x53_&hpi_transformation.;
%let ur_hybrid_extra = x61_&ur_transformation. x60_&ur_transformation. x3_&ur_transformation.;
 

%let extra = sk_acct_id year quarter snapshot_month src_prim_prov_state_cd &hpi_hybrid_extra. &ur_hybrid_extra. vehicle_year_nbr;

%let categorical_variables = acct_status_cd acct_typ_cd amort_typ_cd coll_cd prod_typ_cd
				ln_amt_grp ln_orig_year_grp orig_crb_scr_grp orig_int_rt_grp orig_ltv_rto_grp 
				region_grp term_grp vehicle_age_grp vehicle_make_grp vehicle_year_grp
                vtype prod_id joint_applcnt_ind cur_bal_amt_grp;


data temp.dec_2014_clean_subset;
	set temp.dec_2014_clean_3;
	keep &predictors. &extra.;
run;

data temp.dfast_base_tran_subset;
	set dfast.dfast_base_tran;
	keep &predictors. &extra.;
run;

data temp.dfast_inflation_tran_subset;
	set dfast.dfast_inflation_tran;
	keep &predictors. &extra.;
run;

data temp.dfast_european_tran_subset;
	set dfast.dfast_european_tran;
	keep &predictors. &extra.;
run;


proc sql;
	create table temp.dec_2014_dbase_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_base_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;

proc sql;
	create table temp.dec_2014_inflation_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_inflation_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;

proc sql;
	create table temp.dec_2014_european_1 as
	select
		D.*,
		F.*,
        F.year - vehicle_year_nbr + (F.quarter*3 - 1)/12 as vehicle_age,
        case
            when (CALCULATED vehicle_age) = . then
                5
            when (CALCULATED vehicle_age) < 2 then
                1
            when (CALCULATED vehicle_age) < 5 then
                2
            when (CALCULATED vehicle_age) < 7 then
                3
            else 
                4
        end as vehicle_age_grp
	from 
		temp.dec_2014_clean_subset as D
	inner join
		temp.dfast_european_tran_subset as F
	on
		201212 <= F.snapshot_month <= 201703
	;
quit;



data fc.dec_2014_dbase;
    set temp.dec_2014_dbase_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;

data fc.dec_2014_inflation;
    set temp.dec_2014_inflation_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;

data fc.dec_2014_european;
    set temp.dec_2014_european_1;
    if strip(src_prim_prov_state_cd) = 'IL' then do;
        /* x61 is the unemployment rate of Illinois */
	    ur_hybrid_&ur_transformation. = x61_&ur_transformation.;
		/* x54 is the Case-Shiller HPI of Chicago */
		hpi_hybrid_&hpi_transformation. = x54_&hpi_transformation.;
    end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
        /* x60 is the unemployment rate of Wisconsin */
		ur_hybrid_&ur_transformation. = x60_&ur_transformation.;
		/* x56 is the FHFA HPI of Milwauke */
		hpi_hybrid_&hpi_transformation. = x56_&hpi_transformation.;
    end; else do;
        /* x3 is the national unemployment rate */
		ur_hybrid_&ur_transformation. = x3_&ur_transformation.;
		/* x53 is the national Case-Shiller HPI  */
		hpi_hybrid_&hpi_transformation. = x53_&hpi_transformation.;
    end;
run;

************************************************************************************************;

/*
x73_l1_std: 1.02231
x73 is the real 7-year T-note
*/
data sen.dec_2014_dbase_x73;
	set fc.dec_2014_dbase;
	if snapshot_month > 201412 then; do;
		x73_l1 = x73_l1 + 1.02231 * &magnifier.;
	end;
run; 

/*
x1_yyc_std: 5.971926 
x1 is the Manheim index
*/
data sen.dec_2014_dbase_x1;
	set fc.dec_2014_dbase;
	if snapshot_month > 201412 then; do;
		x1_yyc = x1_yyc - 5.971926 * &magnifier.;
	end;
run; 

/*
x61_yyc_std: 1.454055
x61 is the unemployment rate of Illinois
x60_yyc_std: 1.24964
x60 is the unemployment rate of Wisconsin
x3_yyc_std: 1.248215
x3 is the national unemployment rate
*/
data sen.dec_2014_dbase_ur;
	set fc.dec_2014_dbase;
	if snapshot_month > 201412 then; do;
		if strip(src_prim_prov_state_cd) = 'IL' then do;
            /* x61 is the unemployment rate of Illinois */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.454055 * &magnifier.;
        end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
            /* x60 is the unemployment rate of Wisconsin */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.24964 * &magnifier.;
        end; else do;
            /* x3 is the national unemployment rate */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.248215 * &magnifier.;
        end;
	end;
run;

************************************************************************************************;

/*
x73_l1_std: 1.02231
x73 is the real 7-year T-note
*/
data sen.dec_2014_inflation_x73;
	set fc.dec_2014_inflation;
	if snapshot_month > 201412 then; do;
		x73_l1 = x73_l1 + 1.02231 * &magnifier.;
	end;
run; 

/*
x1_yyc_std: 5.971926 
x1 is the Manheim index
*/
data sen.dec_2014_inflation_x1;
	set fc.dec_2014_inflation;
	if snapshot_month > 201412 then; do;
		x1_yyc = x1_yyc - 5.971926 * &magnifier.;
	end;
run; 


/*
x61_yyc_std: 1.454055
x61 is the unemployment rate of Illinois
x60_yyc_std: 1.24964
x60 is the unemployment rate of Wisconsin
x3_yyc_std: 1.248215
x3 is the national unemployment rate
*/
data sen.dec_2014_inflation_ur;
	set fc.dec_2014_inflation;
	if snapshot_month > 201412 then; do;
		if strip(src_prim_prov_state_cd) = 'IL' then do;
            /* x61 is the unemployment rate of Illinois */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.454055 * &magnifier.;
        end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
            /* x60 is the unemployment rate of Wisconsin */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.24964 * &magnifier.;
        end; else do;
            /* x3 is the national unemployment rate */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.248215 * &magnifier.;
        end;
	end;
run;

************************************************************************************************;

/*
x73_l1_std: 1.02231
x73 is the real 7-year T-note
*/
data sen.dec_2014_european_x73;
	set fc.dec_2014_european;
	if snapshot_month > 201412 then; do;
		x73_l1 = x73_l1 + 1.02231 * &magnifier.;
	end;
run; 

/*
x1_yyc_std: 5.971926 
x1 is the Manheim index
*/
data sen.dec_2014_european_x1;
	set fc.dec_2014_european;
	if snapshot_month > 201412 then; do;
		x1_yyc = x1_yyc - 5.971926 * &magnifier.;
	end;
run; 

/*
x61_yyc_std: 1.454055
x61 is the unemployment rate of Illinois
x60_yyc_std: 1.24964
x60 is the unemployment rate of Wisconsin
x3_yyc_std: 1.248215
x3 is the national unemployment rate
*/
data sen.dec_2014_european_ur;
	set fc.dec_2014_european;
	if snapshot_month > 201412 then; do;
		if strip(src_prim_prov_state_cd) = 'IL' then do;
            /* x61 is the unemployment rate of Illinois */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.454055 * &magnifier.;
        end; else if strip(src_prim_prov_state_cd) = 'WI' then do;
            /* x60 is the unemployment rate of Wisconsin */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.24964 * &magnifier.;
        end; else do;
            /* x3 is the national unemployment rate */
            ur_hybrid_&ur_transformation. = ur_hybrid_&ur_transformation. + 1.248215 * &magnifier.;
        end;
	end;
run;

************************************************************************************************;


/* In-sample fitting and scenario forecast */
proc logistic data=lgd.lgd_all_variables_dfast outest=betas outmodel=lmodel;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = &predictors. / rsquare;
	score data=lgd.lgd_all_variables_dfast out=testing.dfast_all_fit;
	score data=sen.dec_2014_dbase_x73 out=sen.dec_2014_dbase_x73_forecast;
	score data=sen.dec_2014_dbase_x1 out=sen.dec_2014_dbase_x1_forecast;
	score data=sen.dec_2014_dbase_ur out=sen.dec_2014_dbase_ur_forecast;
	score data=sen.dec_2014_inflation_x73 out=sen.dec_2014_inflation_x73_forecast;
	score data=sen.dec_2014_inflation_x1 out=sen.dec_2014_inflation_x1_forecast;
	score data=sen.dec_2014_inflation_ur out=sen.dec_2014_inflation_ur_forecast;
	score data=sen.dec_2014_european_x73 out=sen.dec_2014_european_x73_forecast;
	score data=sen.dec_2014_european_x1 out=sen.dec_2014_european_x1_forecast;
	score data=sen.dec_2014_european_ur out=sen.dec_2014_european_ur_forecast;
run;

/* Out-of-sample testing */
proc logistic data=lgd.lgd_training_dfast outest=betas outmodel=lmodel;
	class &categorical_variables. / param=ref;
	model final_lgd/dummy_trial = &predictors. / rsquare;
	score data=lgd.lgd_testing_dfast out=testing.dfast_testing_fit;
run;


/* 
In-sample and out-of-sample backtesting
*/
proc sql;
	create table testing.dfast_backtesting as
	select
		I.year,
		I.quarter,
		cat(substr(putn(I.year, "4."), 3, 2), "Q", I.quarter) as yyqq,
		I.lgd_qtr_avg as observed_lgd_qtr_avg_in,
		I.lgd_hat_qtr_avg as fitted_lgd_qtr_avg,
		O.lgd_qtr_avg as observed_lgd_qtr_avg_out,
		O.lgd_hat_qtr_avg as predicted_lgd_qtr_avg
	from /* in-sample */
		(select year, quarter, mean(final_lgd) as lgd_qtr_avg, mean(p_event) as lgd_hat_qtr_avg from testing.dfast_all_fit group by year, quarter) I
	inner join /* out-of-sample */
		(select year, quarter, mean(final_lgd) as lgd_qtr_avg, mean(p_event) as lgd_hat_qtr_avg from testing.dfast_testing_fit group by year, quarter) O
	on
		I.year = O.year and I.quarter = O.quarter
	;
quit;

/*
Shocked Scenario forecasting
*/
proc sql;
	create table sen.dfast_scenario_forecast as
	select 
		year, 
		quarter, 
		yyqq, 
		observed_lgd_qtr_avg_in as observed_lgd_qtr_avg,
		fitted_lgd_qtr_avg as b73_fit_forecast,
		fitted_lgd_qtr_avg as b1_fit_forecast,
		fitted_lgd_qtr_avg as bur_fit_forecast,
		fitted_lgd_qtr_avg as I73_fit_forecast,
		fitted_lgd_qtr_avg as I1_fit_forecast,
		fitted_lgd_qtr_avg as Iur_fit_forecast,
		fitted_lgd_qtr_avg as E73_fit_forecast,
		fitted_lgd_qtr_avg as E1_fit_forecast,
		fitted_lgd_qtr_avg as Eur_fit_forecast
	from
		testing.dfast_backtesting
	union all
	select 
		B73.year,
		B73.quarter,
		cat(substr(putn(B73.year, "4."), 3, 2), "Q", B73.quarter) as yyqq,
		. as Observed,
		B73.lgd_hat_qtr_avg as b73_fit_forecast,
		B1.lgd_hat_qtr_avg as b1_fit_forecast,
		Bur.lgd_hat_qtr_avg as bur_fit_forecast,
		I73.lgd_hat_qtr_avg as i73_fit_forecast,
		I1.lgd_hat_qtr_avg as i1_fit_forecast,
		Iur.lgd_hat_qtr_avg as iur_fit_forecast,
		E73.lgd_hat_qtr_avg as e73_fit_forecast,
		E1.lgd_hat_qtr_avg as e1_fit_forecast,
		Eur.lgd_hat_qtr_avg as eur_fit_forecast
	from /* quarterly average of base x73 */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_dbase_x73_forecast group by year, quarter) as B73
	inner join
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_dbase_x1_forecast group by year, quarter) as B1
	on
		B73.year = B1.year and B73.quarter = B1.quarter
	inner join
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_dbase_ur_forecast group by year, quarter) as Bur
    on
		B1.year = Bur.year and B1.quarter = Bur.quarter
	inner join /* quarterly average of inflation x73 */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_inflation_x73_forecast group by year, quarter) as I73
	on 
		B1.year = I73.year and B1.quarter = I73.quarter
	inner join /* quarterly average of inflation x1 */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_inflation_x1_forecast group by year, quarter) as I1
	on
		I73.year = I1.year and I73.quarter = I1.quarter
	inner join /* quarterly average of inflation unemployment rate */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_inflation_ur_forecast group by year, quarter) as Iur
	on
		I1.year = Iur.year and I1.quarter = Iur.quarter
	inner join /* quarterly average of european x73 */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_european_x73_forecast group by year, quarter) as E73
	on
		I1.year = E73.year and I1.quarter = E73.quarter
	inner join /* quarterly average of european x1 */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_european_x1_forecast group by year, quarter) as E1
	on
		E73.year = E1.year and E73.quarter = E1.quarter
	inner join /* quarterly average of european unemployment rate */
		(select year, quarter, mean(p_event) as lgd_hat_qtr_avg from sen.dec_2014_european_ur_forecast group by year, quarter) as Eur
	on
		E1.year = Eur.year and E1.quarter = Eur.quarter
	;
quit;
