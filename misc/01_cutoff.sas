
*-----------------------------------------------------Dynamic Table-------------------------------------------------------------------;
/*Truncate records after first DF/PRP/Closed/WO date/Expired observation*/
*--Maturity Date;

/* the last month for each loan */
proc sql noPrint;
    drop table tempDir.m_dt
    ;
    create table tempDir.m_dt as
    select 
        loanID, 
        max(yyyy_mm) as mat_yyyy_mm
    from 
        clean.bmo_raw_all
    group by 
        loanID
    ;
quit;

*--First DF/PRP/Closed/WO date/Expired;
proc sql noPrint;
    drop table tempDir.cutoff
    ;
    create table tempDir.cutoff as
    select 
        A.loanID, 
        min(A.yyyy_mm) as cutoff_yyyy_mm 
    from
        clean.bmo_raw_all A 
    left join 
        tempDir.m_dt B
    on 
        A.loanID = B.loanID 
    where 
        lowcase(strip(A.reportedLoanStatus)) in ('closed','defaulted','write off') or A.isPrepaid = 1
    or 
        /* if no closed, defaulted or write off, use the last available date */
        A.yyyy_mm = B.mat_yyyy_mm
    group by 
        A.loanID
    ;
quit;

proc sql noPrint;
	/* keep only records before the cutoff date */
    drop table tempDir.net
    ;
    create table tempDir.net as
    select 
        A.*
    from 
        clean.bmo_raw_all A 
    left join 
        tempDir.cutoff B
    on 
        A.loanID = B.loanID
    where 
        A.yyyy_mm <= B.cutoff_yyyy_mm
    ;
	/* create index */
    create index pIndex
    on tempDir.net (loanID, yyyy_mm)
    ;
quit;


