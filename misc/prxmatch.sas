
%include "~/.sas_profile";
libname sophia "~/projects/ia_lgd_loan_level/sophia";
libname sunny "~/projects/ia_lgd_loan_level/sunny";
libname lgd "~/projects/ia_lgd_loan_level/lib";
libname temp "~/projects/ia_lgd_loan_level/lib/temp";
libname tianyu "~/projects/ia_lgd_loan_level/tianyu/data/IA_PD/";


proc sql;
	select distinct 
		src_prim_prov_state_cd
	from	
		tianyu.dec2014
	;
run;



proc import	out=fed_base
	dataFile="~/projects/ia_lgd_loan_level/data/fed_base.csv"
	dbms=csv
	replace;
	getNames=Y;
	dataRow=2;
run;

proc sql;
	select distinct vehicle_year_nbr from tianyu.dec2014
	;
run;

proc sql;
	select distinct
		TIME_DIM_KEY
	from
		tianyu.dec2014
	;
run;

proc sql;	
	select distinct vehicle_year_nbr from temp.loan_level_variables;
run;


proc contents data=temp.loan_level_variables_clean out=names noPrint;
run;

proc sql;
	select name from names;
run;


proc print data=tianyu.dec2014 (obs=5);
run;


proc contents data=tianyu.dec2014 out=names;
run;

proc sql;
	select name from names;
run;

proc print data = tianyu.dec2014 (obs=5);
	title "d";
run;

proc sql;
	select
		name
	from 
		names
	;
quit;







proc print data = sophia.final_ia_datav2_for_mike (obs=5);
	title "Raw Data from Sophia";
run;


/*
Check characteristic variables (for possible issues).
*/
data loan_level_variables_char (keep=_character_);
	set lgd.loan_level_variables;
run;

proc contents data=loan_level_variables_char out=attr;
run;

proc iml;
	use attr;
	read all var "name" into cns;
	close attr;
	*-----------------------------;
	%include "~/sas/util.sas";
	do i = 1 to nrow(cns);
		vname = cns[i];
		use loan_level_variables_char;
		read all var vname into v;
		close loan_level_variables_char;
		run check_variable(v, vname);
	end;
quit;

proc iml;
	cns = {"acct_status_cd", "acct_typ_cd", "amort_typ_cd", "coll_cd", "prod_typ_cd", 
		"shdw_status_cd", "ln_amt_grp", "ln_orig_year_grp", "orig_crb_scr_grp", 
		"orig_int_rt_grp", "orig_ltv_rto_grp", "region_grp", "term_grp", "vehicle_age_grp", 
		"vehicle_make_grp", "vehicle_year_grp", "vtype", "prod_id", "joint_applcnt_ind"};
	*********************************************;
	%include "~/sas/util.sas";
	do i = 1 to numel(cns);
		vname = cns[i];
		use lgd.harris_ia_lgd_all_variables;
		read all var vname into v;
		close lgd.harris_ia_lgd_all_variables;
		run check_variable(v, vname);
	end;
quit;

/*********************************************************************/

/* 
Find columns containing specifed patterns.
*/
proc contents data=lgd.harris_ia_lgd_all_variables out=attr;
run;

proc sql;
	select
		name
	from 
		attr
	where	
		pRxMatch("/.*id.*/i", name) > 0
	;
run;

proc sql;
	select
		name
	from 
		attr
	where	
		pRxMatch("/.*grp|.*_cd/i", name) > 0
	;
run;
**********************************************************************;

/*
Check date range in the monthly snapshot data 
*/
proc sql;
	select distinct
		snapshot_month
	from 
		loan_level_variables2
	;
quit;

proc sql;
	select distinct
		yyyy_mm
	from 
		sunny.ia_lgd_final_macro_mdl_v3
	;
quit;

proc sql;
	select distinct
		year,
		quarter
	from 
		lgd.harris_ia_lgd_all_variables
	;
quit;


proc sql;
	select distinct
		snapshot_month
	from 
		lgd.harris_ia_lgd_all_variables
	;
quit;

proc sql;
	select 
		count(*)
	from 
		lgd.harris_ia_lgd_all_variables
	;
quit;


proc sql;
    create table loan_level_variables2 as
    select 
        B.*,
        case 
            when B.acct_typ_cd in ('184', '185', '186', '187') then 
                'RV/MARINE'
            else 
                'OTHER' 
        end as rv_marine_flag,
        A.bk_flag,
        A.lgd,
        A.lgd_downturn,
        A.me_risk_seg_id,
        A.prod_id,
        /* @question: why do we have use this one? */
        A.risk_seg_id as risk_seg_id_2, 
        A.seg_id,
        A.def_date,
        A.default_time_key,
        A.final_lgd,
        A.lgd_nix,
        A.product,
        A.rec_typ,
        /* @question: this is a little bit strange.  it sounds to me we should take a difference*/
        year(B.ln_orig_dt) as ln_orig_year 
        from 
            lgd.harris_lgd_with_nix_3m_cure as A
		/* 
		* harris_lgd_with_nix_3m_cure contains only the defaulted accounts while ia_montly_snapshot contains all acounts
		*/
        left join 
            lgd.ia_monthly_snapshot as B
        on 
            A.sk_acct_id = B.sk_acct_id
        and 
            A.time = B.snapshot_month
    ;
quit;
************************************************************************************;

proc sql outobs=5;
	select	
		bnkrpt_ind,
		bk_flag
	from	
		loan_level_variables2;
run;


proc sql;
	select distinct	
		bnkrpt_ind,
		bk_flag
	from	
		loan_level_variables2;
run;


proc sql;
	select
		min(bnkrpt_ind - bk_flag) as min_diff
	from 
		loan_level_variables2
	;
quit;


/*
Check where the column bnkrpt_ind come from
*/
proc contents data=lgd.harris_lgd_with_nix_3m_cure out=attr;
run;

proc print data=attr;
run;

proc sql;
	select
		name
	from 
		attr
	where	
		pRxMatch("/bnkrpt_ind/i", name) > 0
	;
quit;

proc contents data=lgd.ia_monthly_snapshot out=attr;
run;

proc print data=attr;
run;

proc sql;
	select
		name
	from 
		attr
	where	
		pRxMatch("/bnkrpt_ind|bk_flag/i", name) > 0
	;
quit;


proc contents data=lgd.harris_ia_lgd_all_variables out=attr;
run;


proc sql;
	select
		name
	from 
		attr
	where	
		pRxMatch("/perf_q/i", name) > 0
	;
quit;